package polymorph

import java.nio.ByteBuffer

import scala.concurrent.Future

object Resource {
  var _getResource: String => Future[ByteBuffer] = null
  def getResource(file: String): Future[ByteBuffer] =
    _getResource(file)
}
