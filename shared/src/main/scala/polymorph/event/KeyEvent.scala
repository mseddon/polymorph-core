package polymorph.event

/** An abstract keyboard event. */
trait KeyEvent extends Event {
  /** The modifiers for this key event. */
  val modifiers: Int
  /** Determines if the Alt key was held down during this event. */
  def isAltDown = (modifiers & Event.AltDown) != 0

  /** Determines if the Ctrl key was held down during this event. */
  def isCtrlDown = (modifiers & Event.CtrlDown) != 0

  /** Determines if the Meta key was held down during this event. */
  def isMetaDown = (modifiers & Event.MetaDown) != 0

  /** Determines if the Shift key was held down during this event. */
  def isShiftDown = (modifiers & Event.ShiftDown) != 0
}

/**
 * Indicates a key was pressed.
 * @param source the source of this event.
 * @param when the time, in milliseconds, that this event occurred.
 * @param modifiers the key modifiers for this event.
 * @param keyCode the key code, listed in `KeyEvent`, for this event.
 */
case class KeyDown(source: Object, when: Long, override val modifiers: Int, keyCode: Int, originalEvent: RawEvent = null) extends KeyEvent

/**
 * Indicates a key was released.
 * @param source the source of this event.
 * @param when the time, in milliseconds, that this event occurred.
 * @param modifiers the key modifiers for this event.
 * @param keyCode the key code, listed in `KeyEvent`, for this event.
 */
case class KeyUp(source: Object, when: Long, override val modifiers: Int, keyCode: Int, originalEvent: RawEvent = null) extends KeyEvent

case class KeyTyped(source: Object, when: Long, override val modifiers: Int, keyCode: Int, keyChar: Int, originalEvent: RawEvent = null) extends KeyEvent

object KeyEvent {
  val VK_BACKSPACE = 8
  val VK_TAB = 9
  val VK_RETURN = 13
  val VK_SHIFT  = 16
  val VK_CTRL   = 17
  val VK_ALT    = 18
  val VK_PAUSE  = 19
  val VK_CAPS_LOCK = 20
  val VK_ESCAPE   = 27
  val VK_PAGE_UP = 33
  val VK_PAGE_DOWN = 34
  val VK_END = 35
  val VK_HOME = 36

  val VK_LEFT   = 37
  val VK_UP     = 38
  val VK_RIGHT  = 39
  val VK_DOWN   = 40
  val VK_INSERT = 45
  val VK_DELETE = 46

  val VK_0      = 48
  val VK_1      = 49
  val VK_2      = 50
  val VK_3      = 51
  val VK_4      = 52
  val VK_5      = 53
  val VK_6      = 54
  val VK_7      = 55
  val VK_8      = 56
  val VK_9      = 57

  val VK_A      = 65
  val VK_B      = 66
  val VK_C      = 67
  val VK_D      = 68
  val VK_E      = 69
  val VK_F      = 70
  val VK_G      = 71
  val VK_H      = 72
  val VK_I      = 73
  val VK_J      = 74
  val VK_K      = 75
  val VK_L      = 76
  val VK_M      = 77
  val VK_N      = 78
  val VK_O      = 79
  val VK_P      = 80
  val VK_Q      = 81
  val VK_R      = 82
  val VK_S      = 83
  val VK_T      = 84
  val VK_U      = 85
  val VK_V      = 86
  val VK_W      = 87
  val VK_X      = 88
  val VK_Y      = 89
  val VK_Z      = 90

  val VK_F1     = 112
  val VK_F2     = 113
  val VK_F3     = 114
  val VK_F4     = 115
  val VK_F5     = 116
  val VK_F6     = 117
  val VK_F7     = 118
  val VK_F8     = 119
  val VK_F9     = 120
  val VK_F10    = 121
  val VK_F11    = 122
  val VK_F12    = 123

  val VK_SEMICOLON = 186
  val VK_EQUALS    = 187
  val VK_COMMA     = 188
  val VK_MINUS     = 189
  val VK_PERIOD    = 190
  val VK_SLASH     = 191
  val VK_QUOTE     = 192
  val VK_OPEN_BRACKET  = 219
  val VK_CLOSE_BRACKET = 221
  val VK_NUMBER_SIGN          = 222
  val VK_BACK_QUOTE     = 223
}

trait KeyEventReceiver {
  def keyDown(e: KeyDown): Unit = {}
  def keyUp(e: KeyUp): Unit = {}
  def keyTyped(e: KeyTyped): Unit = {}

  /** Make this KeyEventReceiver have focus. */
  def focus(): Unit

  /** Make this KeyEventReceiver lose focus. */
  def blur(): Unit
}

object Keyboard {
  private[polymorph] var _setContext: (String, Int, Boolean) => Unit = null
  def setContext(string: String, position: Int, multiSelect: Boolean = false): Unit =
    if(_setContext != null)
      _setContext(string, position, multiSelect)
}