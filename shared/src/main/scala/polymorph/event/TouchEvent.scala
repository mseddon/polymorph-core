package polymorph.event

import scryetek.vecmath.Vec2

/**
 * A touch event.
 */
sealed trait TouchEvent extends Event

/**
 * A single touch on the surface.
 * @param id the id of this touch.
 * @param target the target receiver for this touch.
 * @param position the position of this touch, local to the receiver.
 * @param viewportPosition the position of this touch, in screen coordinates.
 * @param globalPosition the position of this touch, in document coordinates.
 */
case class Touch(id: Long, target: Object, position: Vec2, viewportPosition: Vec2, globalPosition: Vec2)

/**
 * Indicates a touch has appeared on the surface.
 * @param source the source receiver of this event.
 * @param when the time this event occurred.
 * @param modifiers any key modifiers in force when this event occurred.
 * @param touches the list of active touches.
 */
case class TouchStart(source: TouchEventReceiver, when: Long, modifiers: Int, touches: Seq[Touch], changedTouches: Seq[Touch], originalEvent: RawEvent = null) extends TouchEvent

/**
 * Indicates a touch has disappeared from the surface.
 *
 * @param source the source receiver of this event.
 * @param when the time this event occurred.
 * @param modifiers any key modifiers in force when this event occurred.
 * @param touches the list of active touches.
 */
case class TouchEnd(source: TouchEventReceiver, when: Long, modifiers: Int, touches: Seq[Touch], changedTouches: Seq[Touch], originalEvent: RawEvent = null) extends TouchEvent

/**
 * Indicates a touch has moved across the surface.
 *
 * @param source the source receiver of this event.
 * @param when the time this event occurred.
 * @param modifiers any key modifiers in force when this event occurred.
 * @param touches the list of active touches.
 */
case class TouchMove(source: TouchEventReceiver, when: Long, modifiers: Int, touches: Seq[Touch], changedTouches: Seq[Touch], originalEvent: RawEvent = null) extends TouchEvent

/**
 * Indicates this touch gesture has been cancelled.
 *
 * @param source the source receiver of this event.
 * @param when the time this event occurred.
 * @param modifiers any key modifiers in force when this event occurred.
 * @param touches the list of active touches.
 */
case class TouchCancel(source: TouchEventReceiver, when: Long, modifiers: Int, touches: Seq[Touch], changedTouches: Seq[Touch], originalEvent: RawEvent = null) extends TouchEvent

object TouchEventReceiver {
  /** The set of all touch event receivers who are processing touch events */
  var capturedTouches: Set[TouchEventReceiver] = Set()
}

/**
 * An event receiver for touch events.
 */
trait TouchEventReceiver {
  def globalPosition: Vec2
  /**
   * Called when a new touch appears on the surface.
   */
  def touchStart(e: TouchStart): Unit = {}

  /**
   * Called when a touch disappears from the surface.
   */
  def touchEnd(e: TouchEnd): Unit = {}

  /**
   * Called when a parent control has cancelled this touch event.
   */
  def touchCancel(e: TouchCancel): Unit = {}

  /**
   * Called when a touch moves across the surface.
   */
  def touchMove(e: TouchMove): Unit = {}

  def interceptTouches(): Unit = {
    if(TouchEventReceiver.capturedTouches.contains(this)) {
      val when = System.currentTimeMillis()
      for(receiver <- TouchEventReceiver.capturedTouches)
        receiver.touchCancel(TouchCancel(receiver, when, 0, Seq(), Seq()))
      TouchEventReceiver.capturedTouches = Set(this)
    }
  }
}
