package polymorph.event

/**
 * An opaque handle to the original raw event from the operating systme.
 */
trait RawEvent {
  def consume()
}

/**
 * An abstract Event.
 */
trait Event {
  val originalEvent: RawEvent

  def consume(): Unit =
    if(originalEvent != null)
      originalEvent.consume()
}

/**
 * Defines some useful constants.
 */
object Event {
  /** Indicates that the mouse button has been pressed. */
  val Button1Down = 1

  /** Indicates that the mouse button has been pressed. */
  val Button2Down = 2

  /** Indicates that the mouse button has been pressed. */
  val Button3Down = 4

  /** Indicates the shift key was down during this event. */
  val ShiftDown = 8

  /** Indicates the control key was down during this event. */
  val CtrlDown = 16

  /** Indicates the meta key was down during this event. */
  val MetaDown = 32

  /** Indicates the alt key was down during this event. */
  val AltDown = 64

}