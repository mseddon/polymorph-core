package polymorph.event

import scryetek.vecmath.Vec2

/**
 * An abstract MouseEvent trait.
 */
sealed trait MouseEvent extends Event {
  import Event._

  /** The source of this event. */
  val source: Object

  /** The time this event was triggered in milliseconds since the epoch. */
  val when: Long

  /** The modifiers for this event. */
  val modifiers: Int

  /** The position of the mouse in the source's local coordinates. */
  val position: Vec2

  /** The position of the mouse in the viewport. */
  val viewportPosition: Vec2

  /** The position of the mouse in window coordinates. */
  val globalPosition: Vec2

  /** Determines if alt was held down during this event. */
  def isAltDown = (modifiers & AltDown) != 0

  /** Determines if ctrl was held down during this event. */
  def isCtrlDown = (modifiers & CtrlDown) != 0

  /** Determines if meta was held down during this event. */
  def isMetaDown = (modifiers & MetaDown) != 0

  /** Determines if shift was held down during this event. */
  def isShiftDown = (modifiers & ShiftDown) != 0
}

/**
 * Represents a mouse down event.
 * @param button the button that was pressed. Buttons are indexed from 1.
 */
case class MouseDown(
    source: Object,
    when: Long,
    modifiers: Int,
    position: Vec2,
    viewportPosition: Vec2,
    globalPosition: Vec2,
    /** The button that was pressed */
    button: Int,
    originalEvent: RawEvent = null) extends MouseEvent

/**
 * Represents a mouse up event.
 * @param button the button that was released. Buttons are indexed from 1.
 */
case class MouseUp(
    source: Object,
    when: Long,
    modifiers: Int,
    position: Vec2,
    viewportPosition: Vec2,
    globalPosition: Vec2,
    /** The button that was released */
    button: Int,
    originalEvent: RawEvent = null) extends MouseEvent

/** Represents a mouse move event. */
case class MouseMove(
    source: Object,
    when: Long,
    modifiers: Int,
    position: Vec2,
    viewportPosition: Vec2,
    globalPosition: Vec2,
    originalEvent: RawEvent = null) extends MouseEvent

/** Represents a mouse drag event. */
case class MouseDrag(
    source: Object,
    when: Long,
    modifiers: Int,
    position: Vec2,
    viewportPosition: Vec2,
    globalPosition: Vec2,
    originalEvent: RawEvent = null) extends MouseEvent

/** Represents a mouse exit event. */
case class MouseExit(
    source: Object,
    when: Long,
    modifiers: Int,
    position: Vec2,
    viewportPosition: Vec2,
    globalPosition: Vec2,
    originalEvent: RawEvent = null) extends MouseEvent

/** Represents a mouse enter event. */
case class MouseEnter(
    source: Object,
    when: Long,
    modifiers: Int,
    position: Vec2,
    viewportPosition: Vec2,
    globalPosition: Vec2,
    originalEvent: RawEvent = null) extends MouseEvent

/**
 * Represents a mouse wheel event.
 *
 * @param wheelDelta Specifies the position of the mouse wheel.
 */
case class MouseWheel(
    source: Object,
    when: Long,
    modifiers: Int,
    position: Vec2,
    viewportPosition: Vec2,
    globalPosition: Vec2,
    wheelDelta: Vec2,
    originalEvent: RawEvent = null) extends MouseEvent

/** An object that may receive mouse events. */
trait MouseEventReceiver {
  def globalPosition: Vec2

  /** Triggered when the mouse has left this receiver. */
  def mouseExit(e: MouseExit): Unit = {}

  /** Triggered when the mouse has entered this receiver. */
  def mouseEnter(e: MouseEnter): Unit = {}

  /** Triggered when the mouse has moved within this receiver and there is no drag operation. */
  def mouseMove(e: MouseMove): Unit = {}

  /** Triggered when the mouse is dragged from this receiver. */
  def mouseDrag(e: MouseDrag): Unit = {}

  /** Triggered when the mouse is pressed within this receiver. */
  def mouseDown(e: MouseDown): Unit = {}

  /** Triggered when the mouse is released and this receiver has captured the mouse. */
  def mouseUp(e: MouseUp): Unit = {}

  /** Triggered when the mouse wheel is rolled within this receiver. */
  def mouseWheel(e: MouseWheel): Unit = {}
}

object MouseEventReceiver {
  /** The receiver that has currently captured the mouse for a drag operation. */
  var capture: MouseEventReceiver = null

  /** The receiver that the mouse is currently hovering over. */
  var hover: MouseEventReceiver = null
}