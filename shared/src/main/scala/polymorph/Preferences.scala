package polymorph

private[polymorph] trait PreferencesPeer {
  def getString(key: String): Option[String]
  def getBoolean(key: String): Option[Boolean]
  def getInt(key: String): Option[Int]
  def getFloat(key: String): Option[Float]
  def getLong(key: String): Option[Long]

  def contains(key: String): Boolean

  def withWriter(body: PreferencesWriter => Unit): Unit
}

trait PreferencesWriter {
  def putString(key: String, value: String): Unit
  def putBoolean(key: String, value: Boolean): Unit
  def putInt(key: String, value: Int): Unit
  def putFloat(key: String, value: Float): Unit
  def putLong(key: String, value: Long): Unit

  def remove(key: String): Unit
  def clear(): Unit
}

object Preferences {
  private[polymorph] var impl: PreferencesPeer = null

  def getString(key: String): Option[String] =
    impl.getString(key)

  def getBoolean(key: String): Option[Boolean] =
    impl.getBoolean(key)

  def getInt(key: String): Option[Int] =
    impl.getInt(key)

  def getFloat(key: String): Option[Float] =
    impl.getFloat(key)

  def getLong(key: String): Option[Long] =
    impl.getLong(key)

  def withWriter(body: PreferencesWriter => Unit): Unit =
    impl.withWriter(body)
}
