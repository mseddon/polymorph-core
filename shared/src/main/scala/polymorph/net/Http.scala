package polymorph.net

import java.nio.ByteBuffer

import polymorph.net.Http.HttpData

import scala.concurrent.Future
import scala.language.implicitConversions

private [polymorph] abstract class HttpPeer {
  def apply(
      method: String,
      url: String,
      data: HttpData,
      timeout: Int,
      headers: Map[String, String],
      withCredentials: Boolean): Future[HttpResponse]
}

case class HttpResponse(status: Int, statusText: String, headers: Map[String, String], data: ByteBuffer) {
  def asString(charSet: String = "UTF-8"): String =
    new String(asByteArray, charSet)

  def asByteBuffer: ByteBuffer =
    data

  lazy val asByteArray: Array[Byte] = {
    val array = new Array[Byte](data.remaining())
    data.get(array)
    array
  }
}

object Http {
  sealed trait HttpData

  final case class StringData(data: String) extends HttpData
  final case class ByteBufferData(data: ByteBuffer) extends HttpData
  final case class ByteArrayData(data: Array[Byte]) extends HttpData

  implicit def str2data(string: String): HttpData =
    StringData(string)

  implicit def bytebuffer2data(bytebuffer: ByteBuffer): HttpData =
    ByteBufferData(bytebuffer)

  implicit def array2data(array: Array[Byte]): HttpData =
    ByteArrayData(array)

  private[polymorph] var httpClient: HttpPeer = null

  def apply(
      method: String,
      url: String,
      data: Http.HttpData = null,
      timeout: Int,
      headers: Map[String, String],
      withCredentials: Boolean): Future[HttpResponse] =
    httpClient.apply(method, url, data, timeout, headers, withCredentials)

  def get(
      url: String,
      data: Http.HttpData = null,
      timeout: Int,
      headers: Map[String, String],
      withCredentials: Boolean): Future[HttpResponse] =
    httpClient.apply("GET", url, data, timeout, headers, withCredentials)

  def post(
      url: String,
      data: Http.HttpData,
      timeout: Int,
      headers: Map[String, String],
      withCredentials: Boolean): Future[HttpResponse] =
    httpClient.apply("GET", url, data, timeout, headers, withCredentials)

  def delete(
      url: String,
      data: Http.HttpData,
      timeout: Int,
      headers: Map[String, String],
      withCredentials: Boolean): Future[HttpResponse] =
    httpClient.apply("DELETE", url, data, timeout, headers, withCredentials)

  def put(
      url: String,
      data: Http.HttpData,
      timeout: Int,
      headers: Map[String, String],
      withCredentials: Boolean,
      responseType: String): Future[HttpResponse] =
    httpClient.apply("PUT", url, data, timeout, headers, withCredentials)
}