package polymorph

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.language.postfixOps

abstract class App {
  val initTimeout = 10 seconds
  def init(args: Array[String]): Future[_] = {
    import scala.concurrent.ExecutionContext.Implicits.global
    Future {}
  }
  def main(args: Array[String]): Unit
}
