package polymorph

/**
 * Created by Matt on 30/07/2015.
 */
object Prelude {
  private [polymorph] var _log: (String, String) => Unit = null
  def log(target: String, string: String): Unit = _log(target, string)
}
