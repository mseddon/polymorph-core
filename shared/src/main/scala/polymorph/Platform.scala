package polymorph

private[polymorph] abstract class PlatformPeer {
  def isAndroid: Boolean = false
  def isIOS: Boolean = false
  def isWeb: Boolean = false
  def isDesktop: Boolean = false
}

object Platform {
  private[polymorph] var _peer: PlatformPeer = null
  private[polymorph] var _supportsOpenGL = false

  def supportsOpenGL: Boolean =
    _supportsOpenGL

  def isAndroid: Boolean =
    _peer.isAndroid

  def isIOS: Boolean =
    _peer.isIOS

  def isWeb: Boolean =
    _peer.isWeb

  def isDesktop: Boolean =
    _peer.isDesktop

  def isMobileApp: Boolean =
    isAndroid || isIOS
}
