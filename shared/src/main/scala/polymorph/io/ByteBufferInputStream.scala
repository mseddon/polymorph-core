package polymorph.io

import java.io.InputStream
import java.nio.ByteBuffer

class ByteBufferInputStream(buffer: ByteBuffer) extends InputStream {
  override def read(array: Array[Byte]): Int = {
    val toRead = array.length min buffer.remaining()
    buffer.get(toRead)
    toRead
  }

  override def read(array: Array[Byte], offset: Int, len: Int): Int = {
    val toRead = array.length-offset min buffer.remaining() min len
    buffer.get(array, offset, toRead)
    toRead
  }

  override def read(): Int = {
    if(buffer.remaining() == 0)
      -1
    else
      buffer.get() & 0xFF
  }

  override def available(): Int =
    buffer.remaining()
}
