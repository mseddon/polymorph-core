package polymorph

/**
 * Created by matt on 05/10/15.
 */
package object timer {
  trait TimerHandle

  private[polymorph] var _setTimeout: Double => (() => Unit) => TimerHandle = null
  private[polymorph] var _setInterval: (Double, Double) => (() => Unit) => TimerHandle = null
  private[polymorph] var _clearTimer: TimerHandle => Unit = null

  def setTimeout(delay: Double)(handler: => Unit): TimerHandle =
    _setTimeout(delay)(() => handler)

  def setInterval(delay: Double, interval: Double)(handler: => Unit): TimerHandle =
    _setInterval(delay, interval)(() => handler)

  def clearTimer(timerHandle: TimerHandle): Unit =
    _clearTimer(timerHandle)
}
