package polymorph.gfx

import scryetek.vecmath.{Mat2d, Vec2}

/**
 * Created by Matt on 11/07/2015.
 */

sealed trait LineCap
object LineCap {
  case object Butt extends LineCap
  case object Round extends LineCap
  case object Square extends LineCap
}

sealed trait LineJoin
object LineJoin {
  case object Bevel extends LineJoin
  case object Round extends LineJoin
  case object Miter extends LineJoin
}

sealed trait GlobalCompositeOperation
object GlobalCompositeOperation {
  case object SrcOver extends GlobalCompositeOperation
  case object SrcIn extends GlobalCompositeOperation
  case object SrcOut extends GlobalCompositeOperation
  case object SrcAtop extends GlobalCompositeOperation
  case object DstOver extends GlobalCompositeOperation
  case object DstIn extends GlobalCompositeOperation
  case object DstOut extends GlobalCompositeOperation
  case object DstAtop extends GlobalCompositeOperation
//  case object Multiply extends GlobalCompositeOperation
//  case object Screen extends GlobalCompositeOperation
//  case object Add extends GlobalCompositeOperation
}

object Path2d {
  private [polymorph] var init: () => Path2d = null

  def apply(): Path2d =
    init()
}

trait Path2d {
  def moveTo(x: Float, y: Float): Unit
  def lineTo(x: Float, y: Float): Unit
  def quadricCurveTo(cpx: Float, cpy: Float, x: Float, y: Float): Unit
  def bezierCurveTo(cp0x: Float, cp0y: Float, cp1x: Float, cp1y: Float, x: Float, y: Float): Unit
  def arc(x: Float, y: Float, radius: Float, startAngle: Float, endAngle: Float, anticlockwise: Boolean = false): Unit
  def arcTo(x1: Float, y1: Float, x2: Float, y2: Float, radius: Float) = {
    val Vec2(x0, y0) = currentPosition
    val dir = (y1-y0)*(x2-x1) - (y2-y1)*(x1-x0)
    if(x0 == x1 && y0 == y1 || x1 == x2 && y1 == y2 || radius == 0 || dir == 0) {
      // points are colinear, radius is 0, add line to endpoint.
      lineTo(x2, y2)
    } else {
      val a2 = (x0-x1)*(x0-x1) + (y0-y1)*(y0-y1)
      val b2 = (x1-x2)*(x1-x2) + (y1-y2)*(y1-y2)
      val c2 = (x0-x2)*(x0-x2) + (y0-y2)*(y0-y2)
      val cosx = (a2+b2-c2)/(2*Math.sqrt(a2*b2)).toFloat

      val sinx = Math.sqrt(1 - cosx*cosx).toFloat
      val d = radius / ((1 - cosx) / sinx)

      val anx = (x1-x0) / Math.sqrt(a2).toFloat
      val any = (y1-y0) / Math.sqrt(a2).toFloat
      val bnx = (x1-x2) / Math.sqrt(b2).toFloat
      val bny = (y1-y2) / Math.sqrt(b2).toFloat
      val x3 = x1 - anx*d
      val y3 = y1 - any*d
      val x4 = x1 - bnx*d
      val y4 = y1 - bny*d
      val anticlockwise = dir < 0
      val cx = x3 + any*radius*(if(anticlockwise) 1 else  -1)
      val cy = y3 - anx*radius*(if(anticlockwise) 1 else -1)
      val angle0 = Math.atan2(y3-cy, x3-cx).toFloat
      val angle1 = Math.atan2(y4-cy, x4-cx).toFloat

      lineTo(x3, y3)
      arc(cx, cy, radius, angle0, angle1, anticlockwise)
    }
  }
  def close()
  def currentPosition: Vec2
}

trait Gfx {
  def lineWidth: Float

  def lineWidth_=(value: Float): Unit

  def lineDash: Seq[Float]

  def lineDash_=(pattern: Seq[Float]): Unit

  def lineDashOffset: Float

  def lineDashOffset_=(offset: Float): Unit

  def style: PaintStyle

  def style_=(style: PaintStyle): Unit

  def globalAlpha: Float

  def globalAlpha_=(value: Float): Unit

  def globalCompositeOperation: GlobalCompositeOperation

  def globalCompositeOperation_=(value: GlobalCompositeOperation): Unit

  def miterLimit: Float

  def miterLimit_=(value: Float): Unit

  def lineCap: LineCap

  def lineCap_=(value: LineCap): Unit

  def lineJoin: LineJoin

  def lineJoin_=(value: LineJoin): Unit

  def stroke(path: Path2d): Unit

  def fill(path: Path2d): Unit

  def clip(path: Path2d): Unit

  def fillRect(x: Float, y: Float, width: Float, height: Float)

  def strokeRect(x: Float, y: Float, width: Float, height: Float)

  def clearRect(x: Float, y: Float, width: Float, height: Float)

  def save(): Unit

  def restore(): Unit

  def drawImage(image: Image, dx: Float, dy: Float): Unit

  def drawImage(image: Image, dx: Float, dy: Float, dWidth: Float, dHeight: Float): Unit

  def drawImage(image: Image, sx: Float, sy: Float, sWidth: Float, sHeight: Float, dx: Float, dy: Float, dWidth: Float, dHeight: Float): Unit

  def translate(tx: Float, ty: Float): Unit

  def translate(t: Vec2): Unit =
    translate(t.x, t.y)

  def scale(sx: Float, sy: Float): Unit

  def scale(s: Vec2): Unit =
    scale(s.x, s.y)

  def rotate(angle: Float): Unit

  def setTransform(a: Float, b: Float, c: Float, d: Float, e: Float, f: Float): Unit

  def setTransform(m: Mat2d) : Unit =
    setTransform(m.m0, m.m1, m.m2, m.m3, m.m4, m.m5)

  def font: Font

  def font_=(font: Font): Unit

  def strokeText(text: String, x: Float, y: Float): Unit

  def fillText(text: String, x: Float, y: Float): Unit

  def textAlign: TextAlign

  def textAlign_=(align: TextAlign): Unit

  def textBaseline: TextBaseline

  def textBaseline_=(align: TextBaseline): Unit
}

sealed trait TextAlign
object TextAlign {
  case object Start extends TextAlign
  case object End extends TextAlign
  case object Left extends TextAlign
  case object Right extends TextAlign
  case object Center extends TextAlign
}

sealed trait TextBaseline
object TextBaseline {
  case object Top extends TextBaseline
  case object Hanging extends TextBaseline
  case object Middle extends TextBaseline
  case object Alphabetic extends TextBaseline
  case object Bottom extends TextBaseline
}