package polymorph.gfx

import java.nio.ByteBuffer

import scala.concurrent.Future

/**
 * Created by Matt on 27/07/2015.
 */
trait Image {
  def width: Int
  def height: Int
  def gfx: Gfx
}

object Image {
  private [polymorph] var _imageCreate: (Int, Int) => Image = null
  private [polymorph] var _imageFromResource: (String) => Future[Image] = null
  private [polymorph] var _imageFromByteBuffer: (ByteBuffer) => Future[Image] = null

  def apply(width: Int, height: Int): Image =
    _imageCreate(width, height)

  def fromResource(file: String): Future[Image] =
    _imageFromResource(file)

  def fromByteBuffer(byteBuffer: ByteBuffer): Future[Image] =
    _imageFromByteBuffer(byteBuffer)
}