package polymorph.gfx

import scryetek.vecmath.{Vec2, Vec4}

/**
 * Created by Matt on 11/07/2015.
 */

sealed trait PaintStyle
case class Pattern private [polymorph] (peer: Any) extends PaintStyle

object Pattern {
  private [polymorph] var init: (Image) => Pattern = null

  def apply(image: Image): Pattern =
    init(image)
}

sealed trait Gradient extends PaintStyle
class LinearGradient private [polymorph] (val peer: Any) extends Gradient
object LinearGradient {
  private [polymorph] var init: (Vec2, Vec2, Seq[(Float, Color)]) => LinearGradient = null

  def apply(start: Vec2, end: Vec2, colors: Seq[(Float, Color)]): LinearGradient =
    init(start, end, colors)
}

class RadialGradient private [polymorph] (val peer: Any) extends Gradient
object RadialGradient {
  private [polymorph] var init: (Vec2, Float, Seq[(Float, Color)]) => RadialGradient = null

  def apply(center: Vec2, radius: Float, stops: Seq[(Float, Color)]): RadialGradient =
    init(center, radius, stops)
}

case class Color(r: Float, g: Float, b: Float, a: Float = 1) extends PaintStyle {
  // toHSV: Vec4
  // toHSL: Vec4
}

object Color {
  def fromHSV(hsva: Vec4): Color =
    fromHSV(hsva.x, hsva.y, hsva.z, hsva.w)

  def fromHSV(h: Float, s: Float, v: Float, a: Float = 1): Color = {
    val c = v * s
    val h1 = h / 60
    val x = c * (1 - Math.abs((h1 % 2) - 1));
    val m = v - c

    if (h1 < 1)      Color(c+m, x+m, m, a)
    else if (h1 < 2) Color(x+m, c+m, m, a)
    else if (h1 < 3) Color(m, c+m, x+m, a)
    else if (h1 < 4) Color(m, x+m, c+m, a)
    else if (h1 < 5) Color(x+m, m, c+m, a)
    else             Color(c+m, m, x+m, a)
  }
  // fromHSL(h, s, l, a)
}