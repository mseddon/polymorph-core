package polymorph.gfx

/**
 * Created by Matt on 22/07/2015.
 */
abstract class Font private [polymorph] (val name: String, val style: Font.Style, val size: Float) {
  def ascent: Float
  def descent: Float
  def height: Float
  def stringWidth(string: String): Float
}

object Font {
  def apply(name: String, style: Font.Style, size: Float): Font =
    init(name, style, size)

  private [polymorph] var init: (String, Font.Style, Float) => Font = null

  sealed trait Style
  case object Plain extends Style
  case object Bold extends Style
  case object Italic extends Style
  case object BoldItalic extends Style
}