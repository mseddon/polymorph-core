package polymorph.gl

import java.nio._

/**
 * Created by Matt on 23/07/2015.
 */
private [polymorph] trait IBufferUtils {
  def allocateByteBuffer(length: Int): ByteBuffer

  def allocateByteBuffer(array: Array[Byte]): ByteBuffer

  def allocateShortBuffer(length: Int): ShortBuffer

  def allocateShortBuffer(array: Array[Short]): ShortBuffer

  def allocateIntBuffer(length: Int): IntBuffer

  def allocateIntBuffer(array: Array[Int]): IntBuffer

  def allocateFloatBuffer(length: Int): FloatBuffer

  def allocateFloatBuffer(array: Array[Float]): FloatBuffer

  def size(buffer: Buffer): Int
}

object BufferUtils {
  private [polymorph] var impl: IBufferUtils = null

  def allocateByteBuffer(length: Int): ByteBuffer =
    impl.allocateByteBuffer(length)

  def allocateByteBuffer(array: Array[Byte]): ByteBuffer =
    impl.allocateByteBuffer(array)

  def allocateShortBuffer(length: Int): ShortBuffer =
    impl.allocateShortBuffer(length)

  def allocateShortBuffer(array: Array[Short]): ShortBuffer =
    impl.allocateShortBuffer(array)

  def allocateIntBuffer(length: Int): IntBuffer =
    impl.allocateIntBuffer(length)

  def allocateIntBuffer(array: Array[Int]): IntBuffer =
    impl.allocateIntBuffer(array)

  def allocateFloatBuffer(length: Int): FloatBuffer =
    impl.allocateFloatBuffer(length)

  def allocateFloatBuffer(array: Array[Float]): FloatBuffer =
    impl.allocateFloatBuffer(array)

  def size(buffer: Buffer): Int =
    impl.size(buffer)
}