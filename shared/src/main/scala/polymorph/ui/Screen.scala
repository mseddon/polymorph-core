package polymorph.ui

import scryetek.vecmath.Vec2

object Screen {
  private [polymorph] var _dpi: () => Int = null
  private [polymorph] var _screenSize: () => Vec2 = null

  /** Return the reported primary screen dpi. */
  def dpi: Int = _dpi()

  /** Return the dimensions of the main screen in pixels. */
  def size: Vec2 = _screenSize()
}
