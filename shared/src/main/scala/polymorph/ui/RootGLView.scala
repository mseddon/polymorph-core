package polymorph.ui

import polymorph.event.{KeyEventReceiver, MouseEventReceiver, TouchEventReceiver}
import polymorph.gl.GL
import scryetek.vecmath.Vec2

abstract class RootGLViewPeer(val rootView: RootGLView) extends RootViewPeer {
  def withGL(body: GL => Unit): Unit

  def animate: Boolean
  def animate_=(state: Boolean): Unit
}

class RootGLView(_title: String, _withId: String=null) extends AbstractRootView {
  protected[polymorph] val peer = RootGLView._peer(this, _title, _withId)

  def init(gl: GL): Unit = {}

  def paint(gl: GL): Unit = {}

  /**
   * All OpenGL calls *must* occur on the opengl thread, otherwise they will fail silently.
   * This allow this behaviour within arbitrary futures and functions.
   *
   * @param body the body to invoke, a 1-ary function which is passed the GL context.
   *             only withGL blocks, init, display and dispose are guaranteed to have access to a valid
   *             opengl context.  In particular- <i>it is possible for opengl calls outside of these scopes to
   *             fail silently, without showing up in gl.getError</i>
   */
  def withGL(body: GL => Unit): Unit =
    peer.asInstanceOf[RootGLViewPeer].withGL(body)
}


object RootGLView {
  private[polymorph] var _peer: (RootGLView, String, String) => RootGLViewPeer = null
}
