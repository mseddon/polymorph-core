package polymorph.ui

import polymorph.event._
import polymorph.gfx.Gfx
import scryetek.vecmath.Vec2

trait RootViewPeer {
  def globalPosition: Vec2
  def width: Float
  def height: Float

  def title: String
  def title_=(title: String)

  def visible: Boolean
  def visible_=(visible: Boolean)

  def repaint(): Unit

  def focus(): Unit
  def blur(): Unit

  def animate: Boolean
  def animate_=(value: Boolean): Unit

  def cursor: Cursor
  def cursor_=(cursor: Cursor): Unit
}

abstract private [polymorph] class RootGfxViewPeer(val rootView: RootGfxView) extends RootViewPeer

abstract private [polymorph] class AbstractRootView extends MouseEventReceiver with KeyEventReceiver with TouchEventReceiver {
  protected[polymorph] val peer: RootViewPeer

  def globalPosition =
    peer.globalPosition

  def width: Float =
    peer.width

  def height: Float =
    peer.height

  def title: String =
    peer.title

  def title_=(title: String) =
    peer.title = title

  def visible: Boolean =
    peer.visible

  def visible_=(visible: Boolean) =
    peer.visible = visible

  def repaint(): Unit =
    peer.repaint()

  def focus(): Unit =
    peer.focus()

  def blur(): Unit =
    peer.blur()


  def animate: Boolean =
    peer.animate

  def animate_=(state: Boolean): Unit =
    peer.animate = state

  def cursor_=(cursor: Cursor): Unit =
    peer.cursor = cursor

  def cursor =
    peer.cursor
}


/**
 * A toplevel view.  Under the JVM, this is a Window, under html, it is a Canvas,
 * on mobile it is the whole screen.
 */
class RootGfxView(_title: String, _withId: String=null) extends AbstractRootView {
  protected[polymorph] val peer = RootGfxView._peer(this, _title, _withId)
  def paint(gfx: Gfx): Unit = {}
}

object RootGfxView {
  private[polymorph] var _peer: (RootGfxView, String, String) => RootGfxViewPeer = null
}
