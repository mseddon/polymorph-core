package polymorph.ui

import polymorph.gfx.Image

class Cursor

trait CursorPeer {
  val CrossHair: Cursor
  val Default: Cursor
  val ResizeE: Cursor
  val Hand: Cursor
  val Move: Cursor
  val ResizeN: Cursor
  val ResizeNE: Cursor
  val ResizeNW: Cursor
  val ResizeS: Cursor
  val ResizeSE: Cursor
  val ResizeSW: Cursor
  val ResizeW: Cursor
  val Text: Cursor
  val Wait: Cursor

  def fromImage(image: Image, hotspotX: Int, hotspotY: Int): Cursor
}

object Cursor {
  private[polymorph] var cursorImpl: CursorPeer = null

  def CrossHair: Cursor = cursorImpl.CrossHair
  def Default: Cursor = cursorImpl.Default
  def ResizeE: Cursor = cursorImpl.ResizeE
  def Hand: Cursor = cursorImpl.Hand
  def Move: Cursor = cursorImpl.Move
  def ResizeN: Cursor = cursorImpl.ResizeN
  def ResizeNE: Cursor = cursorImpl.ResizeNE
  def ResizeNW: Cursor = cursorImpl.ResizeNW
  def ResizeS: Cursor = cursorImpl.ResizeS
  def ResizeSE: Cursor = cursorImpl.ResizeSE
  def ResizeSW: Cursor = cursorImpl.ResizeSW
  def ResizeW: Cursor = cursorImpl.ResizeW
  def Text: Cursor = cursorImpl.Text
  def Wait: Cursor = cursorImpl.Wait

  def fromImage(image: Image, hotspotX: Int, hotspotY: Int): Cursor =
    cursorImpl.fromImage(image, hotspotX, hotspotY)
}