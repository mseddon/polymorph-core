package polymorph.event.impl
import java.awt.{AWTEvent, Component}
import java.awt.event.InputEvent

import polymorph.event._
import scryetek.vecmath.Vec2

trait MouseEventReceiverProxy { this: Component =>
  val receiver: MouseEventReceiver
  
  private def convertMouseEventModifiers(e: java.awt.event.MouseEvent): Int = {
    var mask = 0
    if(e.isAltDown) mask |= Event.AltDown
    if(e.isControlDown) mask |= Event.CtrlDown
    if(e.isShiftDown) mask |= Event.ShiftDown
    if(e.isMetaDown) mask |= Event.MetaDown
    if((e.getModifiersEx & InputEvent.BUTTON1_DOWN_MASK) != 0) mask |= Event.Button1Down
    if((e.getModifiersEx & InputEvent.BUTTON2_DOWN_MASK) != 0) mask |= Event.Button2Down
    if((e.getModifiersEx & InputEvent.BUTTON3_DOWN_MASK) != 0) mask |= Event.Button3Down
    mask
  }

  override def processMouseEvent(e: java.awt.event.MouseEvent): Unit = {
    val pos = Vec2(e.getPoint.x, e.getPoint.y)
    e.getID match {
      case java.awt.event.MouseEvent.MOUSE_ENTERED =>
        val me = MouseEnter(receiver, e.getWhen, convertMouseEventModifiers(e), pos, pos, pos)
        receiver.mouseEnter(me)
      case java.awt.event.MouseEvent.MOUSE_EXITED =>
        val me = MouseExit(receiver, e.getWhen, convertMouseEventModifiers(e), pos, pos, pos)
        receiver.mouseExit(me)
      case java.awt.event.MouseEvent.MOUSE_PRESSED =>
        val me = MouseDown(receiver, e.getWhen, convertMouseEventModifiers(e), pos, pos, pos, e.getButton)
        receiver.mouseDown(me)
      case java.awt.event.MouseEvent.MOUSE_RELEASED =>
        val me = MouseUp(receiver, e.getWhen, convertMouseEventModifiers(e), pos, pos, pos, e.getButton)
        receiver.mouseUp(me)
      case _ =>
    }
  }

  override def processMouseMotionEvent(e: java.awt.event.MouseEvent): Unit = {
    val pos = Vec2(e.getPoint.x, e.getPoint.y)
    e.getID match {
      case java.awt.event.MouseEvent.MOUSE_MOVED =>
        val me = MouseMove(receiver, e.getWhen, convertMouseEventModifiers(e), pos, pos, pos)
        receiver.mouseMove(me)
      case java.awt.event.MouseEvent.MOUSE_DRAGGED =>
        val me = MouseDrag(receiver, e.getWhen, convertMouseEventModifiers(e), pos, pos, pos)
        receiver.mouseDrag(me)
      case _ =>
    }
  }

  override def processMouseWheelEvent(e: java.awt.event.MouseWheelEvent): Unit = {
    val pos = Vec2(e.getPoint.x, e.getPoint.y)
    val me = MouseWheel(receiver, e.getWhen, convertMouseEventModifiers(e), pos, pos, pos, Vec2(0, e.getPreciseWheelRotation.toFloat))
    receiver.mouseWheel(me)
  }
}

