package polymorph.event.impl

import java.awt.Component
import polymorph.event._

trait KeyEventReceiverProxy { this: Component =>
  val receiver: KeyEventReceiver
  import KeyEventReceiverProxy._

  override def processKeyEvent(e: java.awt.event.KeyEvent): Unit = {
    def convertKeyEventModifiers(e: java.awt.event.KeyEvent): Int = {
      var mask = 0
      if(e.isAltDown)   mask |= Event.AltDown
      if(e.isControlDown)  mask |= Event.CtrlDown
      if(e.isMetaDown)  mask |= Event.MetaDown
      if(e.isShiftDown) mask |= Event.ShiftDown
      mask
    }
    e.getID match {
      case java.awt.event.KeyEvent.KEY_PRESSED =>
        receiver.keyDown(KeyDown(this, System.currentTimeMillis(), convertKeyEventModifiers(e), convertKeyCode(e.getKeyCode)))
      case java.awt.event.KeyEvent.KEY_RELEASED =>
        receiver.keyUp(KeyUp(this, System.currentTimeMillis(), convertKeyEventModifiers(e), convertKeyCode(e.getKeyCode)))
      case java.awt.event.KeyEvent.KEY_TYPED =>
        receiver.keyTyped(KeyTyped(this, System.currentTimeMillis(), convertKeyEventModifiers(e), convertKeyCode(e.getKeyCode), e.getKeyChar))
      case _ =>
    }
  }

  setFocusable(true)
}

object KeyEventReceiverProxy {
  import java.awt.event.{ KeyEvent => KE }
  private val keyMap = Map[Int, Int](
    KE.VK_ENTER -> KeyEvent.VK_RETURN,
    KE.VK_PAUSE -> KeyEvent.VK_PAUSE,
    KE.VK_SHIFT -> KeyEvent.VK_SHIFT,
    KE.VK_CONTROL -> KeyEvent.VK_CTRL,
    KE.VK_ALT -> KeyEvent.VK_ALT,
    KE.VK_CAPS_LOCK -> KeyEvent.VK_CAPS_LOCK,
    KE.VK_ESCAPE -> KeyEvent.VK_ESCAPE,
    KE.VK_PAGE_UP -> KeyEvent.VK_PAGE_UP,
    KE.VK_PAGE_DOWN -> KeyEvent.VK_PAGE_DOWN,
    KE.VK_END -> KeyEvent.VK_END,
    KE.VK_HOME -> KeyEvent.VK_HOME,
    KE.VK_LEFT -> KeyEvent.VK_LEFT,
    KE.VK_UP -> KeyEvent.VK_UP,
    KE.VK_RIGHT -> KeyEvent.VK_RIGHT,
    KE.VK_DOWN -> KeyEvent.VK_DOWN,
    KE.VK_INSERT -> KeyEvent.VK_INSERT,
    KE.VK_DELETE -> KeyEvent.VK_DELETE,
    KE.VK_0 -> KeyEvent.VK_0,
    KE.VK_1 -> KeyEvent.VK_1,
    KE.VK_2 -> KeyEvent.VK_2,
    KE.VK_3 -> KeyEvent.VK_3,
    KE.VK_4 -> KeyEvent.VK_4,
    KE.VK_5 -> KeyEvent.VK_5,
    KE.VK_6 -> KeyEvent.VK_6,
    KE.VK_7 -> KeyEvent.VK_7,
    KE.VK_8 -> KeyEvent.VK_8,
    KE.VK_9 -> KeyEvent.VK_9,

    KE.VK_A -> KeyEvent.VK_A,
    KE.VK_B -> KeyEvent.VK_B,
    KE.VK_C -> KeyEvent.VK_C,
    KE.VK_D -> KeyEvent.VK_D,
    KE.VK_E -> KeyEvent.VK_E,
    KE.VK_F -> KeyEvent.VK_F,
    KE.VK_G -> KeyEvent.VK_G,
    KE.VK_H -> KeyEvent.VK_H,
    KE.VK_I -> KeyEvent.VK_I,
    KE.VK_J -> KeyEvent.VK_J,
    KE.VK_K -> KeyEvent.VK_K,
    KE.VK_L -> KeyEvent.VK_L,
    KE.VK_M -> KeyEvent.VK_M,
    KE.VK_N -> KeyEvent.VK_N,
    KE.VK_O -> KeyEvent.VK_O,
    KE.VK_P -> KeyEvent.VK_P,
    KE.VK_Q -> KeyEvent.VK_Q,
    KE.VK_R -> KeyEvent.VK_R,
    KE.VK_S -> KeyEvent.VK_S,
    KE.VK_T -> KeyEvent.VK_T,
    KE.VK_U -> KeyEvent.VK_U,
    KE.VK_V -> KeyEvent.VK_V,
    KE.VK_W -> KeyEvent.VK_W,
    KE.VK_X -> KeyEvent.VK_X,
    KE.VK_Y -> KeyEvent.VK_Y,
    KE.VK_Z -> KeyEvent.VK_Z,

    KE.VK_F1 -> KeyEvent.VK_F1,
    KE.VK_F2 -> KeyEvent.VK_F2,
    KE.VK_F3 -> KeyEvent.VK_F3,
    KE.VK_F4 -> KeyEvent.VK_F4,
    KE.VK_F5 -> KeyEvent.VK_F5,
    KE.VK_F6 -> KeyEvent.VK_F6,
    KE.VK_F7 -> KeyEvent.VK_F7,
    KE.VK_F8 -> KeyEvent.VK_F8,
    KE.VK_F9 -> KeyEvent.VK_F9,
    KE.VK_F10 -> KeyEvent.VK_F10,
    KE.VK_F11 -> KeyEvent.VK_F11,
    KE.VK_F12 -> KeyEvent.VK_F12,

    KE.VK_SEMICOLON -> KeyEvent.VK_SEMICOLON,
    KE.VK_EQUALS -> KeyEvent.VK_EQUALS,
    KE.VK_COMMA -> KeyEvent.VK_COMMA,
    KE.VK_MINUS -> KeyEvent.VK_MINUS,
    KE.VK_PERIOD -> KeyEvent.VK_PERIOD,
    KE.VK_SLASH -> KeyEvent.VK_SLASH,
    KE.VK_QUOTE -> KeyEvent.VK_QUOTE,
    KE.VK_OPEN_BRACKET -> KeyEvent.VK_OPEN_BRACKET,
    KE.VK_CLOSE_BRACKET -> KeyEvent.VK_CLOSE_BRACKET,
    KE.VK_NUMBER_SIGN-> KeyEvent.VK_NUMBER_SIGN,
    KE.VK_BACK_QUOTE -> KeyEvent.VK_BACK_QUOTE
  )

  private def convertKeyCode(e: Int): Int =
    keyMap.getOrElse(e, e)
}