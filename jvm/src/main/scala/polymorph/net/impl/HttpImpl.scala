package polymorph.net.impl

import java.io.ByteArrayOutputStream
import java.net._
import java.nio.ByteBuffer
import java.util.concurrent.Executors
import scala.collection.JavaConversions._

import polymorph.net.Http.HttpData
import polymorph.net.{Http, HttpResponse, HttpPeer}

import scala.concurrent.{ExecutionContext, Future}


class CookieBox extends CookieHandler {
  val policy: CookiePolicy = null
  override def get(uri: URI, requestHeaders: java.util.Map[String, java.util.List[String]]): java.util.Map[String, java.util.List[String]] = {

    if(uri == null || requestHeaders == null)
      throw new IllegalArgumentException()
    if(HttpImpl.withCredentials.get())
      HttpImpl.defaultHandler.get(uri, requestHeaders)
    else
      requestHeaders
  }

  override def put(uri: URI, requestHeaders: java.util.Map[String, java.util.List[String]]): Unit = {
    if(uri == null || requestHeaders == null)
      throw new IllegalArgumentException()
    if(HttpImpl.withCredentials.get())
      HttpImpl.defaultHandler.put(uri, requestHeaders)
  }
}

object HttpImpl {
  val withCredentials = new ThreadLocal[Boolean] {
    override def initialValue(): Boolean = false
  }


  val defaultHandler = new CookieManager()
  CookieHandler.setDefault(new CookieBox)

  implicit val ec = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(10))
}

class HttpImpl extends HttpPeer {
  import HttpImpl._
  override def apply(
    method: String,
    url: String,
    data: HttpData,
    timeout: Int,
    headers: Map[String, String],
    withCredentials: Boolean): Future[HttpResponse] = Future {
    val connection = new URL(url).openConnection().asInstanceOf[HttpURLConnection]
    connection.setConnectTimeout(timeout)
    connection.setRequestMethod(method)
    HttpImpl.withCredentials.set(withCredentials)
    for((header, value) <- headers)
      connection.addRequestProperty(header, value)
    if(method == "POST") {
      connection.setDoOutput(true)

      val (contentLength: Int, rawdata: Array[Byte]) = data match {
        case Http.StringData(str) => (str.length, str.getBytes("UTF-8"))
        case Http.ByteArrayData(barry) => (barry.length, barry)
        case Http.ByteBufferData(bbuf) => {
          val bytes = new Array[Byte](bbuf.remaining)
          bbuf.get(bytes)
          (bbuf.remaining, bytes)
        }
        case null => (0, Array[Byte]())
      }
      connection.getOutputStream.write(rawdata)
      connection.getOutputStream.flush()
    }

    val status = connection.getResponseCode


    val is = if(status >= 400 && status < 500)
      connection.getErrorStream
    else
      connection.getInputStream
    val out = new ByteArrayOutputStream()
    val buf = new Array[Byte](0xFFFF)
    var len = is.read(buf)

    while (len != -1) {
      out.write(buf, 0, len)
      len = is.read(buf)
    }
    out.flush()
    val outheaders =
      for((header, value) <- connection.getHeaderFields) yield (header, value.mkString(","))
    HttpResponse(connection.getResponseCode, connection.getResponseMessage, headers, ByteBuffer.wrap(out.toByteArray))
  }
}
