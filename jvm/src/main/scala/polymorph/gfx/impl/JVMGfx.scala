package polymorph.gfx.impl

import java.awt.geom._
import java.awt.{AlphaComposite, BasicStroke, Graphics2D, LinearGradientPaint, RadialGradientPaint, RenderingHints, TexturePaint}

import polymorph.gfx._
import scryetek.vecmath.Vec2

class JVMPath2d extends Path2d {
  val path: Path2D = new Path2D.Float()
  def moveTo(x: Float, y: Float): Unit = {
    path.moveTo(x, y)
    _currentPosition = Vec2(x, y)
  }

  def lineTo(x: Float, y: Float): Unit = {
    path.lineTo(x, y)
    _currentPosition = Vec2(x, y)
  }

  def arc(x: Float, y: Float, radius: Float, startAngle: Float, endAngle: Float, anticlockwise: Boolean = false): Unit = {
    // correct for Java2D arc starting at top of the circle
    if(anticlockwise)
      path.append(new Arc2D.Float(x-radius, y-radius, radius*2, radius*2, Math.toDegrees(startAngle).toFloat, 360-Math.toDegrees(endAngle-startAngle).toFloat, Arc2D.OPEN), true)
    else
      path.append(new Arc2D.Float(x-radius, y-radius, radius*2, radius*2, Math.toDegrees(startAngle).toFloat, Math.toDegrees(startAngle-endAngle).toFloat, Arc2D.OPEN), true)
    _currentPosition = Vec2(Math.cos(endAngle).toFloat*radius + x, Math.sin(endAngle).toFloat*radius + y)
  }

  def quadricCurveTo(cpx: Float, cpy: Float, x: Float, y: Float): Unit = {
    path.quadTo(cpx, cpy, x, y)
    _currentPosition = Vec2(x, y)
  }

  def bezierCurveTo(cp1x: Float, cp1y: Float, cp2x: Float, cp2y: Float, x: Float, y: Float): Unit = {
    path.curveTo(cp1x, cp1y, cp2x, cp2y, x, y)
    _currentPosition = Vec2(x, y)
  }

  def close(): Unit =
    path.closePath()

  private var _currentPosition = Vec2()

  def currentPosition = _currentPosition
}

object JVMPath2d {
  def apply(): JVMPath2d =
    new JVMPath2d()
}

class JVMGfx(var g: Graphics2D) extends Gfx {
  g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
  g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON)
  g.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON)
  g.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY)
  g.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_NORMALIZE)

  private case class State(
    var g: Graphics2D,
    var lineWidth: Float = 1f,
    var miterLimit: Float = 1f,
    var lineDashOffset: Float = 0,
    var lineDash: Seq[Float] = Seq(),
    var lineCap: LineCap = LineCap.Round,
    var lineJoin: LineJoin = LineJoin.Round,
    var globalAlpha: Float = 1f,
    var globalCompositeOperation: GlobalCompositeOperation = GlobalCompositeOperation.SrcOver,
    var font: JVMFont = new JVMFont("Arial", Font.Plain, 12),
    var textAlign: TextAlign = TextAlign.Start,
    var textBaseline: TextBaseline = TextBaseline.Alphabetic
  )

  private var _strokeChanged: Boolean = true
  private var _compositeChanged: Boolean = true
  private var _paintStyle: PaintStyle = Color(0, 0, 0, 1)

  private var currentState = new State(g = g)
  private val stack = collection.mutable.Stack[State]()

  def style: PaintStyle = _paintStyle

  def style_=(style: PaintStyle): Unit =
    style match {
      case c@Color(r, g, b, a) =>
        _paintStyle = c
        this.g.setPaint(null)
        this.g.setColor(new java.awt.Color(r, g, b, a))
      case p:Pattern =>
        _paintStyle = p
        this.g.setPaint(p.peer.asInstanceOf[TexturePaint])
      case g:LinearGradient =>
        _paintStyle = g
        this.g.setPaint(g.peer.asInstanceOf[LinearGradientPaint])
      case g:RadialGradient =>
        _paintStyle = g
        this.g.setPaint(g.peer.asInstanceOf[RadialGradientPaint])
    }

  def lineWidth: Float = currentState.lineWidth

  def lineWidth_=(value: Float): Unit =
    if(currentState.lineWidth != value) {
      currentState.lineWidth = value
      _strokeChanged = true
    }

  def lineDash = currentState.lineDash

  def lineDash_=(pattern: Seq[Float]): Unit = {
    currentState.lineDash = pattern
    _strokeChanged = true
  }

  def lineDashOffset = currentState.lineDashOffset

  def lineDashOffset_=(offset: Float) = {
    currentState.lineDashOffset = offset
    _strokeChanged = true
  }

  def globalAlpha: Float = currentState.globalAlpha

  def globalAlpha_=(value: Float): Unit =
    if(currentState.globalAlpha != value) {
      currentState.globalAlpha = value
      _compositeChanged = true
    }

  def globalCompositeOperation: GlobalCompositeOperation = currentState.globalCompositeOperation

  def globalCompositeOperation_=(value: GlobalCompositeOperation): Unit = {
    if(currentState.globalCompositeOperation != value) {
      currentState.globalCompositeOperation = value
      _compositeChanged = true
    }
  }

  def miterLimit: Float = currentState.miterLimit

  def miterLimit_=(value: Float): Unit =
    if(currentState.miterLimit != value) {
      currentState.miterLimit = value
      _strokeChanged = true
    }

  def lineCap: LineCap = currentState.lineCap

  def lineCap_=(value: LineCap): Unit =
    if(currentState.lineCap != value) {
      currentState.lineCap = value
      _strokeChanged = true
    }

  def lineJoin: LineJoin = currentState.lineJoin

  def lineJoin_=(value: LineJoin): Unit =
    if(currentState.lineJoin != value) {
      currentState.lineJoin = value
      _strokeChanged = true
    }


  def stroke(path: Path2d): Unit =
    ensuringComposite {
      ensuringStroke {
        g.draw(path.asInstanceOf[JVMPath2d].path)
      }
    }

  def fill(path: Path2d): Unit =
    ensuringComposite {
      ensuringStroke {
        g.fill(path.asInstanceOf[JVMPath2d].path)
      }
    }

  def clip(path: Path2d): Unit =
    ensuringComposite {
      ensuringStroke {
        g.clip(path.asInstanceOf[JVMPath2d].path)
      }
    }


  def save(): Unit = {
    stack.push(currentState)
    g = g.create().asInstanceOf[Graphics2D]
    currentState = currentState.copy(g = g)
  }

  def restore(): Unit = {
    currentState = stack.pop()
    g = currentState.g
    _compositeChanged = true
    _strokeChanged = true
  }

  def fillRect(x: Float, y: Float, width: Float, height: Float): Unit =
    ensuringComposite {
      g.fillRect(x.toInt, y.toInt, width.toInt, height.toInt)
    }

  def strokeRect(x: Float, y: Float, width: Float, height: Float): Unit =
    ensuringStroke {
      g.drawRect(x.toInt, y.toInt, width.toInt, height.toInt)
    }

  def clearRect(x: Float, y: Float, width: Float, height: Float): Unit = {
    save()
    g.setComposite(AlphaComposite.Clear)
    g.fillRect(x.toInt, y.toInt, width.toInt, height.toInt)
    restore()
  }

  private def ensuringComposite(body: => Unit): Unit = {
    if(_compositeChanged) {
      _compositeChanged = false
      import GlobalCompositeOperation._
      g.setComposite(
        AlphaComposite.getInstance(
          currentState.globalCompositeOperation match {
            case SrcIn => AlphaComposite.SRC_IN
            case SrcOut => AlphaComposite.SRC_OUT
            case SrcAtop => AlphaComposite.SRC_ATOP
            case SrcOver => AlphaComposite.SRC_OVER
            case DstAtop => AlphaComposite.DST_ATOP
            case DstIn => AlphaComposite.DST_IN
            case DstOut => AlphaComposite.DST_OUT
            case DstOver => AlphaComposite.DST_OVER
          }, currentState.globalAlpha))
    }
    body
  }

  private def ensuringStroke(body: => Unit): Unit = {
    if(_strokeChanged)
      g.setStroke(
        new BasicStroke(
          currentState.lineWidth,
          currentState.lineCap match {
            case LineCap.Round => BasicStroke.CAP_ROUND
            case LineCap.Butt => BasicStroke.CAP_BUTT
            case LineCap.Square => BasicStroke.CAP_SQUARE
          },
          currentState.lineJoin match {
            case LineJoin.Bevel => BasicStroke.JOIN_BEVEL
            case LineJoin.Miter => BasicStroke.JOIN_MITER
            case LineJoin.Round => BasicStroke.JOIN_ROUND
          },
          currentState.miterLimit,
          if(currentState.lineDash.isEmpty) null
          else currentState.lineDash.toArray,
          currentState.lineDashOffset))
    body
  }

  def drawImage(image: Image, dx: Float, dy: Float): Unit =
    ensuringComposite {
      g.drawImage(image.asInstanceOf[ImageImpl].peer, dx.toInt, dy.toInt, null)
    }

  def drawImage(image: Image, dx: Float, dy: Float, dWidth: Float, dHeight: Float): Unit =
    ensuringComposite {
      g.drawImage(image.asInstanceOf[ImageImpl].peer, dx.toInt, dy.toInt, dWidth.toInt, dHeight.toInt, null)
    }

  def drawImage(image: Image, sx: Float, sy: Float, sWidth: Float, sHeight: Float, dx: Float, dy: Float, dWidth: Float, dHeight: Float): Unit =
    ensuringComposite {
      g.drawImage(image.asInstanceOf[ImageImpl].peer, dx.toInt, dy.toInt, dx.toInt + dWidth.toInt, dy.toInt + dHeight.toInt, sx.toInt, sy.toInt, sx.toInt + sWidth.toInt, sy.toInt + sHeight.toInt, null)
    }

  def translate(tx: Float, ty: Float): Unit =
    g.translate(tx, ty)

  def scale(sx: Float, sy: Float): Unit =
    g.scale(sx, sy)

  def rotate(angle: Float): Unit =
    g.rotate(angle)

  def transform(a: Float, b: Float, c: Float, d: Float, e: Float, f: Float): Unit =
    g.transform(new AffineTransform(a, b, c, d, e, f))

  def setTransform(a: Float, b: Float, c: Float, d: Float, e: Float, f: Float): Unit =
    g.setTransform(new AffineTransform(a, b, c, d, e, f))

  def textAlign =
    currentState.textAlign

  def textAlign_=(align: TextAlign): Unit =
    currentState.textAlign = align

  def textBaseline =
    currentState.textBaseline

  def textBaseline_=(baseline: TextBaseline) =
    currentState.textBaseline = baseline

  def font =
    currentState.font

  def font_=(font: Font) {
    currentState.font = font.asInstanceOf[JVMFont]
    g.setFont(font.asInstanceOf[JVMFont].peer)
  }

  private def offsetText(text: String, x: Float, y: Float): (Float, Float) = {
    val x1 = textAlign match {
      case TextAlign.Left => x
      case TextAlign.Start => x
      case TextAlign.Center => x-font.stringWidth(text)/2
      case TextAlign.Right => x-font.stringWidth(text)
      case TextAlign.End => x-font.stringWidth(text)
    }

    val y1 = textBaseline match {
      case TextBaseline.Alphabetic => y
      case TextBaseline.Middle => y+font.ascent/2-font.descent
      case TextBaseline.Bottom => y-font.descent
      case TextBaseline.Top => y+font.ascent
      case TextBaseline.Hanging => y+font.ascent+font.descent
    }
    (x1, y1)
  }

  def strokeText(string: String, x: Float, y: Float) = ensuringStroke {
    val gv = currentState.font.peer.layoutGlyphVector(g.getFontRenderContext, string.toCharArray, 0, string.length, java.awt.Font.LAYOUT_LEFT_TO_RIGHT)
    val (x1, y1) = offsetText(string, x, y)
    g.draw(gv.getOutline(x1, y1))
  }

  def fillText(string: String, x: Float, y: Float) = ensuringComposite {
    val gv = currentState.font.peer.layoutGlyphVector(g.getFontRenderContext, string.toCharArray, 0, string.length, java.awt.Font.LAYOUT_LEFT_TO_RIGHT)
    val (x1, y1) = offsetText(string, x, y)
    g.fill(gv.getOutline(x1, y1))
  }
}