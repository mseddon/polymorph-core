package polymorph.gfx.impl

import java.awt.{LinearGradientPaint, RadialGradientPaint}

import polymorph.gfx.{Color, RadialGradient, LinearGradient}
import scryetek.vecmath.Vec2

/**
 * Created by Matt on 21/07/2015.
 */
object GradientImpl {
  def initLinear(start: Vec2, end: Vec2, colors: Seq[(Float, Color)]): LinearGradient = {
    val grad = new LinearGradientPaint(start.x, start.y, end.x, end.y,
      (for((offset, color) <- colors) yield 0f max offset min 1f).toArray,
      (for((offset, color) <- colors) yield new java.awt.Color((color.r*255).toInt, (color.g*255).toInt, (color.b*255).toInt, (color.a*255).toInt)).toArray)
    new LinearGradient(grad)
  }

  def initRadial(center: Vec2, radius: Float, colors: Seq[(Float, Color)]): RadialGradient = {
    val grad = new RadialGradientPaint(center.x, center.y, radius,
      (for((offset, color) <- colors) yield 0f max offset min 1f).toArray,
      (for((offset, color) <- colors) yield new java.awt.Color((color.r*255).toInt, (color.g*255).toInt, (color.b*255).toInt, (color.a*255).toInt)).toArray)
    new RadialGradient(grad)
  }
}
