package polymorph.gfx.impl

import java.awt.Graphics2D
import java.awt.image.BufferedImage

import polymorph.gfx.Font

/**
 * Created by Matt on 22/07/2015.
 */
class JVMFont(name: String, style: Font.Style, size: Float) extends Font(name, style, size) {
  val peer = new java.awt.Font(name, style match {
    case Font.Plain => java.awt.Font.PLAIN
    case Font.Bold => java.awt.Font.BOLD
    case Font.Italic => java.awt.Font.ITALIC
    case Font.BoldItalic => java.awt.Font.BOLD | java.awt.Font.ITALIC
  }, size.toInt)

  val fontMetrics = JVMFont.ctx.getFontMetrics(peer)

  def height = ascent + descent
  def ascent = fontMetrics.getAscent
  def descent = fontMetrics.getDescent
  def stringWidth(string: String) = fontMetrics.stringWidth(string)
}

object JVMFont {
  val ctx = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB_PRE).getGraphics.asInstanceOf[Graphics2D]
  def apply(name: String, style: Font.Style, size: Float): Font =
    new JVMFont(name, style, size)
}
