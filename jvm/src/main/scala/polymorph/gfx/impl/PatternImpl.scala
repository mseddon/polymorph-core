package polymorph.gfx.impl

import java.awt.TexturePaint
import java.awt.geom.Rectangle2D
import polymorph.gfx._

object PatternImpl {
  def createPattern(image: Image): Pattern =
    Pattern(new TexturePaint(image.asInstanceOf[ImageImpl].peer, new Rectangle2D.Float(0, 0, image.width, image.height)))
}
