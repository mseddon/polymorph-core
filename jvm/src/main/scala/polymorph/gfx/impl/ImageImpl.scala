package polymorph.gfx.impl

import java.nio.ByteBuffer

import polymorph.gfx._
import java.awt.Graphics2D
import java.awt.image.BufferedImage
import javax.imageio.ImageIO

import polymorph.io.ByteBufferInputStream

import scala.concurrent.Future

/**
 * Created by Matt on 04/07/2015.
 */
class ImageImpl private[polymorph] (private[polymorph] val peer: BufferedImage) extends Image {
  private[polymorph] def this(width: Int, height: Int) =
    this(new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR_PRE))
  def gfx = new JVMGfx(peer.getGraphics.asInstanceOf[Graphics2D])
  def width = peer.getWidth
  def height = peer.getHeight
}

object ImageImpl {
  import concurrent.ExecutionContext.Implicits.global

  def apply(width: Int, height: Int) =
    new ImageImpl(width, height)

  def fromResource(file: String): Future[Image] =
    Future( {
      val pixels = ImageIO.read(getClass.getResourceAsStream(file))
      val outImg = new BufferedImage(pixels.getWidth, pixels.getHeight, BufferedImage.TYPE_4BYTE_ABGR_PRE)
      outImg.getGraphics.drawImage(pixels, 0, 0, pixels.getWidth, pixels.getHeight, 0, 0, pixels.getWidth, pixels.getHeight, null)
      new ImageImpl(outImg)
    })

  def fromByteBuffer(byteBuffer: ByteBuffer): Future[Image] = Future {
    val pixels = ImageIO.read(new ByteBufferInputStream(byteBuffer))
    val outImg = new BufferedImage(pixels.getWidth, pixels.getHeight, BufferedImage.TYPE_4BYTE_ABGR_PRE)
    outImg.getGraphics.drawImage(pixels, 0, 0, pixels.getWidth, pixels.getHeight, 0, 0, pixels.getWidth, pixels.getHeight, null)
    new ImageImpl(outImg)
  }
}
