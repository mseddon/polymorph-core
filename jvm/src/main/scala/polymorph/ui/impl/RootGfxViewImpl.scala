package polymorph.ui.impl

import java.awt.event.{ActionEvent, ActionListener}
import java.awt.{KeyboardFocusManager, AWTEvent, Graphics, Graphics2D}
import java.util.{Timer, TimerTask}
import javax.swing.{JComponent, JFrame}

import polymorph.event.impl.{MouseEventReceiverProxy, KeyEventReceiverProxy}
import polymorph.gfx.impl.JVMGfx
import polymorph.ui.{Cursor, RootGfxView, RootGfxViewPeer}
import scryetek.vecmath.Vec2

/**
 * Created by Matt on 27/07/2015.
 */
class RootGfxViewImpl(view: RootGfxView, _title: String, withId: String) extends RootGfxViewPeer(view) {
  var _peer = new JFrame(_title)

  def width: Float =
    _canvas.getWidth

  def visible_=(visible: Boolean): Unit =
    _peer.setVisible(visible)

  def visible: Boolean =
    _peer.isVisible

  def title: String =
    _peer.getTitle

  def height: Float =
    _canvas.getHeight

  def title_=(title: String): Unit =
    _peer.setTitle(title)

  def globalPosition: Vec2 =
    Vec2(0,0)

  _peer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE)
  _peer.setSize(800, 600)

  class Canvas extends JComponent with KeyEventReceiverProxy with MouseEventReceiverProxy {
    val receiver = view
    override def paint(g: Graphics): Unit =
      view.paint(new JVMGfx(g.asInstanceOf[Graphics2D]))

    enableEvents(AWTEvent.KEY_EVENT_MASK | AWTEvent.MOUSE_EVENT_MASK | AWTEvent.MOUSE_MOTION_EVENT_MASK | AWTEvent.MOUSE_WHEEL_EVENT_MASK)
  }

  var _canvas = new Canvas

  _peer.add(_canvas)

  override def repaint(): Unit = {
    _canvas.repaint()
  }

  def focus(): Unit =
    _canvas.requestFocus()

  def blur(): Unit =
    KeyboardFocusManager.getCurrentKeyboardFocusManager.clearGlobalFocusOwner()


  var _animate = false

  def animate = _animate
  def animate_=(value: Boolean): Unit = {
    if(_animate != value) {
      _animate = value
      if(value)
        timer.schedule(new TimerTask {
          override def run(): Unit = repaint()
        }, 0, 25)
      else
        timer.cancel()
    }
  }

  val timer = new Timer()

  var _cursor: Cursor = Cursor.Default

  def cursor: Cursor =
    _cursor

  def cursor_=(cursor: Cursor) = {
    _cursor = cursor
    _peer.setCursor(cursor.asInstanceOf[JVMCursor].cursor)
  }
}

object RootGfxViewImpl {
  def apply(view: RootGfxView, title: String, withId: String): RootGfxViewPeer =
    new RootGfxViewImpl(view, title, withId)
}
