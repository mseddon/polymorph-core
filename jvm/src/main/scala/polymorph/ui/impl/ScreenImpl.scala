package polymorph.ui.impl

import java.awt.Toolkit

import scryetek.vecmath.Vec2

/**
 * Created by Matt on 31/07/2015.
 */
object ScreenImpl {
  def dpi(): Int =
    Toolkit.getDefaultToolkit.getScreenResolution

  def size(): Vec2 = {
    val size = Toolkit.getDefaultToolkit.getScreenSize
    Vec2(size.width, size.height)
  }
}
