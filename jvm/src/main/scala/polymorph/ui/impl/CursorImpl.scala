package polymorph.ui.impl

import java.awt.{Point, Toolkit}

import polymorph.gfx.Image
import polymorph.gfx.impl.ImageImpl
import polymorph.ui.{Cursor, CursorPeer}

class JVMCursor(val cursor: java.awt.Cursor) extends Cursor

class CursorImpl extends CursorPeer {
  import java.awt.Cursor.getPredefinedCursor
  import java.awt.Cursor._
  override val CrossHair: Cursor = new JVMCursor(getPredefinedCursor(CROSSHAIR_CURSOR))

  override def fromImage(image: Image, x: Int, y: Int): Cursor =
    new JVMCursor(Toolkit.getDefaultToolkit.createCustomCursor(image.asInstanceOf[ImageImpl].peer, new Point(x, y), ""))

  override val ResizeS: Cursor = new JVMCursor(getPredefinedCursor(S_RESIZE_CURSOR))
  override val Move: Cursor = new JVMCursor(getPredefinedCursor(MOVE_CURSOR))
  override val ResizeNW: Cursor = new JVMCursor(getPredefinedCursor(NW_RESIZE_CURSOR))
  override val Text: Cursor = new JVMCursor(getPredefinedCursor(TEXT_CURSOR))
  override val ResizeNE: Cursor = new JVMCursor(getPredefinedCursor(NE_RESIZE_CURSOR))
  override val Wait: Cursor = new JVMCursor(getPredefinedCursor(WAIT_CURSOR))
  override val ResizeSE: Cursor = new JVMCursor(getPredefinedCursor(SE_RESIZE_CURSOR))
  override val ResizeSW: Cursor = new JVMCursor(getPredefinedCursor(SW_RESIZE_CURSOR))
  override val ResizeE: Cursor = new JVMCursor(getPredefinedCursor(E_RESIZE_CURSOR))
  override val ResizeN: Cursor = new JVMCursor(getPredefinedCursor(N_RESIZE_CURSOR))
  override val ResizeW: Cursor = new JVMCursor(getPredefinedCursor(W_RESIZE_CURSOR))

  override val Hand: Cursor = new JVMCursor(getPredefinedCursor(HAND_CURSOR))
  override val Default: Cursor = new JVMCursor(getPredefinedCursor(DEFAULT_CURSOR))
}
