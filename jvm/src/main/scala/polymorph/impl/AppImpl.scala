package polymorph.impl

import scala.concurrent.Await

class AppImpl(app: polymorph.App) {
  def main(args: Array[String]): Unit = {
    Await.result(app.init(Array()), app.initTimeout)
    app.main(Array())
  }
}
