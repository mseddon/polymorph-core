package polymorph.impl

import collection.JavaConversions
import polymorph.{PreferencesWriter, PreferencesPeer}
import java.util.prefs.{Preferences => JPrefs }
/**
 * Created by Matt on 08/08/2015.
 */
class PreferencesImpl extends PreferencesPeer {
  val prefs = JPrefs.userNodeForPackage(Class.forName(new Throwable().getStackTrace.last.getClassName))

  override def getString(key: String): Option[String] =
    if(contains(key))
      Some(prefs.get(key, ""))
    else
      None

  override def getFloat(key: String): Option[Float] =
    if(contains(key))
      Some(prefs.getFloat(key, 0))
    else
      None

  override def getLong(key: String): Option[Long] =
    if(contains(key))
      Some(prefs.getLong(key, 0))
    else
      None

  override def getBoolean(key: String): Option[Boolean] =
    if(contains(key))
      Some(prefs.getBoolean(key, false))
    else
      None

  override def getInt(key: String): Option[Int] =
    if(contains(key))
      Some(prefs.getInt(key, 0))
    else
      None

  override def contains(key: String): Boolean =
    prefs.keys().contains(key)

  override def withWriter(body: PreferencesWriter => Unit): Unit = {
    body(writer)
    prefs.sync()
  }

  val writer = new PreferencesWriter {
    override def putString(key: String, value: String): Unit =
      prefs.put(key, value)

    override def clear(): Unit =
      prefs.clear()

    override def putFloat(key: String, value: Float): Unit =
      prefs.putFloat(key, value)

    override def remove(key: String): Unit =
      prefs.remove(key)

    override def putBoolean(key: String, value: Boolean): Unit =
      prefs.putBoolean(key, value)

    override def putInt(key: String, value: Int): Unit =
      prefs.putInt(key, value)

    override def putLong(key: String, value: Long): Unit =
      prefs.putLong(key, value)
  }
}
