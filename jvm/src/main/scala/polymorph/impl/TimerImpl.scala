package polymorph.impl
import java.util.Timer
import java.util.TimerTask

import polymorph.timer.TimerHandle

object TimerImpl {
  case class Handle(id: TimerTask) extends TimerHandle
  val timer = new Timer()
  def setTimeout(delay: Double)(body: () => Unit): TimerHandle = {
    val task = new TimerTask {
      override def run(): Unit = body()
    }
    timer.schedule(task, (delay * 1000).toLong)
    Handle(task)
  }

  def setInterval(delay: Double, interval: Double)(body: () => Unit): TimerHandle = {
    val task = new TimerTask {
      override def run(): Unit = body()
    }
    timer.scheduleAtFixedRate(task, (delay * 1000).toLong, (interval * 1000).toLong)
    Handle(task)
  }

  def clearTimer(timerHandle: TimerHandle): Unit =
    timerHandle match {
      case Handle(id) =>
        id.cancel()
        timer.purge()
    }
}
