name := "Scryetek Polymorph Core"
publish := {}
publishLocal := {}

lazy val commonSettings = Seq(
  scalaVersion := "2.11.7",
  resolvers += "Nuonix Repo" at "https://repo-nuonix.rhcloud.com/artifactory/hijack-local",
  credentials += Credentials(Path.userHome / ".ivy2" / ".credentials"),
  libraryDependencies += "com.scryetek" %%% "vecmath" % "0.1",
  publishTo := Some("Nuonix Repo" at "https://repo-nuonix.rhcloud.com/artifactory/hijack-local")
)

lazy val polymorphCore = polymorphLibrary.in(file("."))
  .settings(commonSettings: _*)
  .settings(
    name := "polymorph-core",
    organization := "com.scryetek",
    version := "0.1-SNAPSHOT",
    polymorphLinkerObject := Some("polymorph.impl.Linker")
  ).jsSettings(
    libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.8.2-SNAPSHOT"
  ).iosSettings(
    libraryDependencies ++= Seq(
      "org.robovm" % "robovm-objc" % "1.6.0",
      "org.robovm" % "robovm-cocoatouch" % "1.6.0",
      "org.robovm" % "robovm-rt" % "1.6.0"
    )
  )

lazy val polymorphCoreJS = polymorphCore.js
lazy val polymorphCoreJVM = polymorphCore.jvm
lazy val polymorphCoreIOS = polymorphCore.ios
lazy val polymorphCoreAndroid = polymorphCore.android
