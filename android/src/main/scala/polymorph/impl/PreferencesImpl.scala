package polymorph.impl

import android.content.SharedPreferences
import polymorph.{PreferencesWriter, PreferencesPeer}

/**
 * Created by Matt on 09/08/2015.
 */
class PreferencesImpl extends PreferencesPeer {
  val prefs = AppImpl.activity.getPreferences(0)
  override def getString(key: String): Option[String] =
    if(prefs.contains(key)) Some(prefs.getString(key, "")) else None

  override def getFloat(key: String): Option[Float] =
    if(prefs.contains(key)) Some(prefs.getFloat(key, 0)) else None

  override def getLong(key: String): Option[Long] =
    if(prefs.contains(key)) Some(prefs.getLong(key, 0)) else None

  override def withWriter(body: (PreferencesWriter) => Unit): Unit = {
    val editor = prefs.edit()
    val writer = new PreferencesWriter {
      override def putString(key: String, value: String): Unit =
        editor.putString(key, value)

      override def clear(): Unit =
        editor.clear()

      override def putFloat(key: String, value: Float): Unit =
        editor.putFloat(key, value)

      override def remove(key: String): Unit =
        editor.remove(key)

      override def putBoolean(key: String, value: Boolean): Unit =
        editor.putBoolean(key, value)

      override def putInt(key: String, value: Int): Unit =
        editor.putInt(key, value)

      override def putLong(key: String, value: Long): Unit =
        editor.putLong(key, value)
    }
    body(writer)
    editor.commit()
  }

  override def getBoolean(key: String): Option[Boolean] =
    if(prefs.contains(key)) Some(prefs.getBoolean(key, false)) else None

  override def contains(key: String): Boolean =
    prefs.contains(key)

  override def getInt(key: String): Option[Int] =
    if(prefs.contains(key)) Some(prefs.getInt(key, 0)) else None
}
