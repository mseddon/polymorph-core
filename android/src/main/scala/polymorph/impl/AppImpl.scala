package polymorph.impl

import android.app.Activity

import scala.concurrent.Await

/**
 * Created by Matt on 27/07/2015.
 */
class AppImpl(app: polymorph.App) {
  def main(): Unit = {
    Await.result(app.init(Array()), app.initTimeout)
    app.main(Array())
  }
}

object AppImpl {
  var activity: Activity = null
}