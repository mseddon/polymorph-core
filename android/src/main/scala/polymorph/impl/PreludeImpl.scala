package polymorph.impl

import android.util.Log

/**
 * Created by Matt on 30/07/2015.
 */
object PreludeImpl {
  def log(target: String, string: String): Unit =
    Log.i(target, string)
}
