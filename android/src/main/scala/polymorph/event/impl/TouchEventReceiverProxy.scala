package polymorph.event.impl

import android.view.{View, MotionEvent}
import polymorph.Prelude
import polymorph.event._
import scryetek.vecmath.Vec2

trait TouchEventReceiverProxy extends View {
  val receiver: TouchEventReceiver
  private val coords = new MotionEvent.PointerCoords
  private val properties = new MotionEvent.PointerProperties
  private var previousTouches = Seq[Touch]()

  private def convertMouseModifiers(motionEvent: android.view.MotionEvent): Int = {
    val state = motionEvent.getMetaState
    var flags = 0
    if((state & android.view.KeyEvent.META_ALT_ON) != 0)
      flags |= Event.AltDown
    if((state & android.view.KeyEvent.META_SHIFT_ON) != 0)
      flags |= Event.ShiftDown
    if((state & android.view.KeyEvent.META_CTRL_ON) != 0)
      flags |= Event.CtrlDown
    if((state & android.view.KeyEvent.META_META_ON) != 0)
      flags |= Event.MetaDown
    flags
  }

  private def optToSeq[A](opt: Option[A]): Seq[A] =
    if(opt.isEmpty) Seq() else Seq(opt.get)

  private def findChangedTouches(touches: Seq[Touch]): Seq[Touch] =
    touches.flatMap { touch =>
      optToSeq(previousTouches.find(_.id == touch.id)).filter(_ != touch)
    }


  override def onTouchEvent(e: MotionEvent): Boolean = {
    super.onTouchEvent(e)
    def getCoords(n: Int): MotionEvent.PointerCoords = {
      e.getPointerCoords(n, coords)
      coords
    }
    def getProperties(n: Int): MotionEvent.PointerProperties = {
      e.getPointerProperties(n, properties)
      properties
    }
    def getMouse: Int = {
      for(i <- 0 until e.getPointerCount) {
        if(getProperties(i).toolType == MotionEvent.TOOL_TYPE_MOUSE)
          return i
      }
      -1
    }
    def getTouches = {
      def isTouch(toolType: Int): Boolean =
        toolType == MotionEvent.TOOL_TYPE_FINGER || toolType == MotionEvent.TOOL_TYPE_STYLUS

      for {
        i <- 0 until e.getPointerCount if isTouch(getProperties(i).toolType)
        coords = getCoords(i)
      } yield {
        val pos = Vec2(coords.x, coords.y)
        Touch(e.getPointerId(i), receiver, pos, pos, pos)
      }
    }
    val touches = getTouches
    e.getActionMasked match {
      case MotionEvent.ACTION_MOVE =>
        val changedTouches = findChangedTouches(touches)
        if(changedTouches.nonEmpty)
          receiver.touchMove(TouchMove(receiver, System.currentTimeMillis(), convertMouseModifiers(e), touches, changedTouches))
      case MotionEvent.ACTION_DOWN =>
        val changedTouches = optToSeq(touches.find(_.id == e.getPointerId(e.getActionIndex)))
        // changed touch is pointer 0 from android. we may have just filtered the mouse.
        if(changedTouches.nonEmpty)
          receiver.touchStart(TouchStart(receiver, System.currentTimeMillis(), convertMouseModifiers(e), touches, changedTouches))
        if(touches.nonEmpty)
          TouchEventReceiver.capturedTouches += receiver
      case MotionEvent.ACTION_UP =>
        val changedTouches = optToSeq(touches.find(_.id == e.getPointerId(e.getActionIndex)))
        if(TouchEventReceiver.capturedTouches.contains(receiver))
          receiver.touchEnd(TouchEnd(receiver, System.currentTimeMillis(), convertMouseModifiers(e), touches, changedTouches))
        if(touches.isEmpty)
          TouchEventReceiver.capturedTouches -= receiver
        val mouse = getMouse
      case MotionEvent.ACTION_CANCEL if touches.nonEmpty =>
        if(TouchEventReceiver.capturedTouches.contains(receiver))
          receiver.touchCancel(TouchCancel(receiver, System.currentTimeMillis(), convertMouseModifiers(e), touches, touches))
        TouchEventReceiver.capturedTouches -= receiver
      case _ =>
    }
    previousTouches = touches
    true
  }

}
