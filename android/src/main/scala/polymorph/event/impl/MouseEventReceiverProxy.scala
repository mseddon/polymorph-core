package polymorph.event.impl

import android.util.Log
import android.view.{MotionEvent, View}
import polymorph.event._
import scryetek.vecmath.Vec2

trait MouseEventReceiverProxy extends View {
  val receiver: MouseEventReceiver
  private val coords = new MotionEvent.PointerCoords
  private val properties = new MotionEvent.PointerProperties

  private def convertMouseModifiers(motionEvent: android.view.MotionEvent): Int = {
    val state = motionEvent.getMetaState
    var flags = 0
    if((state & android.view.KeyEvent.META_ALT_ON) != 0)
      flags |= Event.AltDown
    if((state & android.view.KeyEvent.META_SHIFT_ON) != 0)
      flags |= Event.ShiftDown
    if((state & android.view.KeyEvent.META_CTRL_ON) != 0)
      flags |= Event.CtrlDown
    if((state & android.view.KeyEvent.META_META_ON) != 0)
      flags |= Event.MetaDown
    flags
  }

  override def onTouchEvent(e: MotionEvent): Boolean = {
    super.onTouchEvent(e)

    def getCoords(n: Int): MotionEvent.PointerCoords = {
      e.getPointerCoords(n, coords)
      coords
    }
    def getProperties(n: Int): MotionEvent.PointerProperties = {
      e.getPointerProperties(n, properties)
      properties
    }
    def getMouse: Int = {
      for(i <- 0 until e.getPointerCount) {
        if(getProperties(i).toolType == MotionEvent.TOOL_TYPE_MOUSE)
          return i
      }
      -1
    }
    def getTouches = {
      def isTouch(toolType: Int): Boolean =
        toolType == MotionEvent.TOOL_TYPE_FINGER || toolType == MotionEvent.TOOL_TYPE_STYLUS

      for {
        i <- 0 until e.getPointerCount if isTouch(getProperties(i).toolType)
        coords = getCoords(i)
      } yield {
        val pos = Vec2(coords.x, coords.y)
        Touch(e.getPointerId(i), receiver, pos, pos, pos)
      }
    }

    e.getActionMasked match {
      case MotionEvent.ACTION_MOVE =>
        val mouse = getMouse
        if(mouse != -1) {
          getCoords(mouse)
          val pos = Vec2(coords.x, coords.y)
          receiver.mouseDrag(MouseDrag(receiver, System.currentTimeMillis(), convertMouseModifiers(e), pos, pos, pos))
        }
      case MotionEvent.ACTION_DOWN =>
        val mouse = getMouse
        if(mouse != -1) {
          getCoords(mouse)
          val pos = Vec2(coords.x, coords.y)
          receiver.mouseDown(MouseDown(receiver, System.currentTimeMillis(), convertMouseModifiers(e), pos, pos, pos, 0))
        }
      case MotionEvent.ACTION_UP =>
        val mouse = getMouse
        if(mouse != -1) {
          getCoords(mouse)
          val pos = Vec2(coords.x, coords.y)
          receiver.mouseUp(MouseUp(receiver, System.currentTimeMillis(), convertMouseModifiers(e), pos, pos, pos, 0))
        }
      case _ =>
    }
    true
  }

  override def onGenericMotionEvent(e: MotionEvent): Boolean = {
    e.getActionMasked match {
      case MotionEvent.ACTION_HOVER_MOVE =>
        // mouse is moving over the surface
        e.getPointerCoords(e.getActionIndex, coords)
        receiver.mouseMove(MouseMove(receiver, System.currentTimeMillis(), convertMouseModifiers(e), Vec2(coords.x, coords.y), Vec2(coords.x, coords.y), Vec2(coords.x, coords.y)))
      case MotionEvent.ACTION_SCROLL =>
        def deltaConvert(x: Double) = x match {
          case 0 => 0f
          case x if x > 0 => 1f
          case x => -1f
        }
        val pos = Vec2(e.getX(), e.getY())
        receiver.mouseWheel(
          MouseWheel(receiver,
            System.currentTimeMillis(),
            convertMouseModifiers(e),
            pos,
            pos,
            pos,
            Vec2(deltaConvert(e.getAxisValue(MotionEvent.AXIS_HSCROLL)), deltaConvert(e.getAxisValue(MotionEvent.AXIS_VSCROLL)))))
      case _ =>
        Log.i("nx", e.toString)
    }
    true
  }


}