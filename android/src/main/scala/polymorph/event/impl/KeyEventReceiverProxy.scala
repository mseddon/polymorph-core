package polymorph.event.impl

import android.view.View
import polymorph.event._

/**
 * Created by Matt on 31/07/2015.
 */
trait KeyEventReceiverProxy extends View {
  val receiver: KeyEventReceiver
  import KeyEventReceiverProxy._

  def convertKeyModifiers(keyEvent: android.view.KeyEvent): Int = {
    val state = keyEvent.getMetaState
    var flags = 0
    if((state & android.view.KeyEvent.META_ALT_ON) != 0)
      flags |= Event.AltDown
    if((state & android.view.KeyEvent.META_SHIFT_ON) != 0)
      flags |= Event.ShiftDown
    if((state & android.view.KeyEvent.META_CTRL_ON) != 0)
      flags |= Event.CtrlDown
    if((state & android.view.KeyEvent.META_META_ON) != 0)
      flags |= Event.MetaDown
    flags
  }

  override def onKeyDown(keyCode: Int, e: android.view.KeyEvent): Boolean = {
    e.startTracking()
    receiver.keyDown(KeyDown(receiver, System.currentTimeMillis(), convertKeyModifiers(e), convertKeyCode(e.getKeyCode)))
    true
  }

  override def onKeyUp(keyCode: Int, e: android.view.KeyEvent): Boolean = {
    if(e.getAction == android.view.KeyEvent.ACTION_UP)
      receiver.keyUp(KeyUp(receiver, System.currentTimeMillis(), convertKeyModifiers(e), convertKeyCode(e.getKeyCode)))
    true
  }

  setFocusableInTouchMode(true)
  setFocusable(true)
}

object KeyEventReceiverProxy {
  import android.view.{KeyEvent => KE}
  private val keyMap = Map[Int, Int](
    KE.KEYCODE_ENTER -> KeyEvent.VK_RETURN,
    KE.KEYCODE_BREAK -> KeyEvent.VK_PAUSE,
    KE.KEYCODE_SHIFT_LEFT -> KeyEvent.VK_SHIFT,
    KE.KEYCODE_SHIFT_RIGHT -> KeyEvent.VK_SHIFT,
    KE.KEYCODE_CTRL_LEFT -> KeyEvent.VK_CTRL,
    KE.KEYCODE_CTRL_RIGHT -> KeyEvent.VK_CTRL,
    KE.KEYCODE_ALT_LEFT -> KeyEvent.VK_ALT,
    KE.KEYCODE_ALT_RIGHT -> KeyEvent.VK_ALT,
    KE.KEYCODE_CAPS_LOCK -> KeyEvent.VK_CAPS_LOCK,
    KE.KEYCODE_ESCAPE -> KeyEvent.VK_ESCAPE,
    KE.KEYCODE_PAGE_UP -> KeyEvent.VK_PAGE_UP,
    KE.KEYCODE_PAGE_DOWN -> KeyEvent.VK_PAGE_DOWN,
    KE.KEYCODE_MOVE_END -> KeyEvent.VK_END,
    KE.KEYCODE_MOVE_HOME -> KeyEvent.VK_HOME,
    KE.KEYCODE_DPAD_LEFT -> KeyEvent.VK_LEFT,
    KE.KEYCODE_DPAD_UP -> KeyEvent.VK_UP,
    KE.KEYCODE_DPAD_RIGHT -> KeyEvent.VK_RIGHT,
    KE.KEYCODE_DPAD_DOWN -> KeyEvent.VK_DOWN,
    KE.KEYCODE_INSERT -> KeyEvent.VK_INSERT,
    KE.KEYCODE_DEL -> KeyEvent.VK_DELETE,
    KE.KEYCODE_0 -> KeyEvent.VK_0,
    KE.KEYCODE_1 -> KeyEvent.VK_1,
    KE.KEYCODE_2 -> KeyEvent.VK_2,
    KE.KEYCODE_3 -> KeyEvent.VK_3,
    KE.KEYCODE_4 -> KeyEvent.VK_4,
    KE.KEYCODE_5 -> KeyEvent.VK_5,
    KE.KEYCODE_6 -> KeyEvent.VK_6,
    KE.KEYCODE_7 -> KeyEvent.VK_7,
    KE.KEYCODE_8 -> KeyEvent.VK_8,
    KE.KEYCODE_9 -> KeyEvent.VK_9,

    KE.KEYCODE_A -> KeyEvent.VK_A,
    KE.KEYCODE_B -> KeyEvent.VK_B,
    KE.KEYCODE_C -> KeyEvent.VK_C,
    KE.KEYCODE_D -> KeyEvent.VK_D,
    KE.KEYCODE_E -> KeyEvent.VK_E,
    KE.KEYCODE_F -> KeyEvent.VK_F,
    KE.KEYCODE_G -> KeyEvent.VK_G,
    KE.KEYCODE_H -> KeyEvent.VK_H,
    KE.KEYCODE_I -> KeyEvent.VK_I,
    KE.KEYCODE_J -> KeyEvent.VK_J,
    KE.KEYCODE_K -> KeyEvent.VK_K,
    KE.KEYCODE_L -> KeyEvent.VK_L,
    KE.KEYCODE_M -> KeyEvent.VK_M,
    KE.KEYCODE_N -> KeyEvent.VK_N,
    KE.KEYCODE_O -> KeyEvent.VK_O,
    KE.KEYCODE_P -> KeyEvent.VK_P,
    KE.KEYCODE_Q -> KeyEvent.VK_Q,
    KE.KEYCODE_R -> KeyEvent.VK_R,
    KE.KEYCODE_S -> KeyEvent.VK_S,
    KE.KEYCODE_T -> KeyEvent.VK_T,
    KE.KEYCODE_U -> KeyEvent.VK_U,
    KE.KEYCODE_V -> KeyEvent.VK_V,
    KE.KEYCODE_W -> KeyEvent.VK_W,
    KE.KEYCODE_X -> KeyEvent.VK_X,
    KE.KEYCODE_Y -> KeyEvent.VK_Y,
    KE.KEYCODE_Z -> KeyEvent.VK_Z,

    KE.KEYCODE_F1 -> KeyEvent.VK_F1,
    KE.KEYCODE_F2 -> KeyEvent.VK_F2,
    KE.KEYCODE_F3 -> KeyEvent.VK_F3,
    KE.KEYCODE_F4 -> KeyEvent.VK_F4,
    KE.KEYCODE_F5 -> KeyEvent.VK_F5,
    KE.KEYCODE_F6 -> KeyEvent.VK_F6,
    KE.KEYCODE_F7 -> KeyEvent.VK_F7,
    KE.KEYCODE_F8 -> KeyEvent.VK_F8,
    KE.KEYCODE_F9 -> KeyEvent.VK_F9,
    KE.KEYCODE_F10 -> KeyEvent.VK_F10,
    KE.KEYCODE_F11 -> KeyEvent.VK_F11,
    KE.KEYCODE_F12 -> KeyEvent.VK_F12,

    KE.KEYCODE_SEMICOLON -> KeyEvent.VK_SEMICOLON,
    KE.KEYCODE_EQUALS -> KeyEvent.VK_EQUALS,
    KE.KEYCODE_COMMA -> KeyEvent.VK_COMMA,
    KE.KEYCODE_MINUS -> KeyEvent.VK_MINUS,
    KE.KEYCODE_PERIOD -> KeyEvent.VK_PERIOD,
    KE.KEYCODE_SLASH -> KeyEvent.VK_SLASH,
    KE.KEYCODE_APOSTROPHE -> KeyEvent.VK_QUOTE,
    KE.KEYCODE_LEFT_BRACKET -> KeyEvent.VK_OPEN_BRACKET,
    KE.KEYCODE_RIGHT_BRACKET -> KeyEvent.VK_CLOSE_BRACKET,
    KE.KEYCODE_POUND -> KeyEvent.VK_NUMBER_SIGN,
    KE.KEYCODE_GRAVE -> KeyEvent.VK_BACK_QUOTE
  )

  private def convertKeyCode(e: Int): Int =
    keyMap.getOrElse(e, e)
}