package polymorph.gfx.impl

import android.graphics._
import polymorph.gfx._

/**
 * Created by Matt on 21/07/2015.
 */
object PatternImpl {
  def init(image: Image): Pattern =
    new Pattern(new BitmapShader(image.asInstanceOf[ImageImpl].peer, Shader.TileMode.REPEAT, Shader.TileMode.REPEAT))
}
