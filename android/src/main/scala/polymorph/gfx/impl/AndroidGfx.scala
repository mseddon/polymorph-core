package polymorph.gfx.impl

import android.graphics.{Bitmap, BitmapShader, Canvas, DashPathEffect, Matrix, Paint, Path, PorterDuff, PorterDuffXfermode, RectF}
import polymorph.gfx._
import scryetek.vecmath.Vec2

import scala.language.implicitConversions

object AndroidPath {
  def apply(): Path2d =
    new AndroidPath
}

class AndroidPath extends Path2d {
  val path: Path = new Path()

  def moveTo(x: Float, y: Float): Unit = {
    path.moveTo(x, y)
    _currentPosition = Vec2(x, y)
  }

  def lineTo(x: Float, y: Float): Unit = {
    path.lineTo(x, y)
    _currentPosition = Vec2(x, y)
  }

  def arc(x: Float, y: Float, radius: Float, startAngle: Float, endAngle: Float, anticlockwise: Boolean = false): Unit = {
    if(anticlockwise)
      path.arcTo(new RectF(x-radius, y-radius, x+radius, y+radius), Math.toDegrees(startAngle).toFloat, -(360-Math.toDegrees(endAngle-startAngle)).toFloat)
    else
      path.arcTo(new RectF(x-radius, y-radius, x+radius, y+radius), Math.toDegrees(startAngle).toFloat, -Math.toDegrees(startAngle-endAngle).toFloat)
    _currentPosition = Vec2(Math.cos(endAngle).toFloat*radius + x, Math.sin(endAngle).toFloat*radius + y)
  }

  def quadricCurveTo(cpx: Float, cpy: Float, x: Float, y: Float): Unit = {
    path.quadTo(cpx, cpy, x, y)
    _currentPosition = Vec2(x, y)
  }

  def bezierCurveTo(cp1x: Float, cp1y: Float, cp2x: Float, cp2y: Float, x: Float, y: Float): Unit = {
    path.cubicTo(cp1x, cp1y, cp2x, cp2y, x, y)
    _currentPosition = Vec2(x, y)
  }

  def close(): Unit =
    path.close()

  private var _currentPosition = Vec2()
  def currentPosition = _currentPosition
}

class AndroidGfx(var canvas: Canvas) extends Gfx {
  private case class State(
      var lineWidth: Float = 1,
      var lineDash: Seq[Float] = Seq(),
      var lineDashOffset: Float = 0,
      var lineCap: LineCap = LineCap.Round,
      var lineJoin: LineJoin = LineJoin.Round,
      var miterLimit: Float = 1,
      var globalAlpha: Float = 1,
      var globalCompositeOperation: GlobalCompositeOperation = GlobalCompositeOperation.SrcOver,
      var style: PaintStyle = Color(0, 0, 0),
      var font: Font = Font("Arial", Font.Plain, 12),
      var textAlign: TextAlign = TextAlign.Start,
      var textBaseline: TextBaseline = TextBaseline.Alphabetic
  )

  private var currentState = new State()
  private val stack = collection.mutable.Stack[State]()
  private var paintChanged = true
  private val paint = new Paint()
  private val initialMatrix = canvas.getMatrix
  paint.setAntiAlias(true);
  paint.setSubpixelText(true)

  private def ensuringPaint(body: => Unit): Unit = {
    if(paintChanged) {
      paint.reset()
      currentState.style match {
        case Color(r,g,b,a) =>
          paint.setColor(
            (((a*255).toInt & 0xFF) << 24) |
                (((r*255).toInt & 0xFF) << 16) |
                (((g*255).toInt & 0xFF) << 8) |
                ((b*255).toInt & 0xFF))
          paint.setShader(null)
        case p: Pattern =>
          paint.setShader(p.peer.asInstanceOf[BitmapShader])
        case g: LinearGradient =>
          paint.setShader(g.peer.asInstanceOf[android.graphics.LinearGradient])
        case g: RadialGradient =>
          paint.setShader(g.peer.asInstanceOf[android.graphics.RadialGradient])
      }
      paint.setStrokeWidth(currentState.lineWidth)
      if(currentState.lineDash.isEmpty)
        paint.setPathEffect(null)
      else
        paint.setPathEffect(new DashPathEffect(currentState.lineDash.toArray, currentState.lineDashOffset))
      paint.setAlpha((currentState.globalAlpha * 255).toInt)
      paint.setXfermode(globalCompositeOperation match {
        case GlobalCompositeOperation.SrcAtop => new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP)
        case GlobalCompositeOperation.SrcIn => new PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        case GlobalCompositeOperation.SrcOver => new PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)
        case GlobalCompositeOperation.SrcOut => new PorterDuffXfermode(PorterDuff.Mode.SRC_OUT)
        case GlobalCompositeOperation.DstAtop => new PorterDuffXfermode(PorterDuff.Mode.DST_ATOP)
        case GlobalCompositeOperation.DstIn => new PorterDuffXfermode(PorterDuff.Mode.DST_IN)
        case GlobalCompositeOperation.DstOver => new PorterDuffXfermode(PorterDuff.Mode.DST_OVER)
        case GlobalCompositeOperation.DstOut => new PorterDuffXfermode(PorterDuff.Mode.DST_OUT)
//        case GlobalCompositeOperation.Multiply => new PorterDuffXfermode(PorterDuff.Mode.MULTIPLY)
//        case GlobalCompositeOperation.Screen => new PorterDuffXfermode(PorterDuff.Mode.SCREEN)
//        case GlobalCompositeOperation.Add => new PorterDuffXfermode(PorterDuff.Mode.ADD)
      })
      paint.setStrokeCap(currentState.lineCap match {
        case LineCap.Round => Paint.Cap.ROUND
        case LineCap.Square => Paint.Cap.SQUARE
        case LineCap.Butt => Paint.Cap.BUTT
      })
      paint.setStrokeJoin(currentState.lineJoin match {
        case LineJoin.Bevel => Paint.Join.BEVEL
        case LineJoin.Miter => Paint.Join.MITER
        case LineJoin.Round => Paint.Join.ROUND
      })
      paint.setStrokeMiter(currentState.miterLimit)

      paint.setTypeface(currentState.font.asInstanceOf[FontImpl].tf)
      paint.setTextSize(currentState.font.size)
    }
    body
  }

  def lineWidth: Float =
    currentState.lineWidth

  def lineWidth_=(value: Float): Unit = {
    currentState.lineWidth = value
    paintChanged = true
  }

  def lineDash: Seq[Float] =
    currentState.lineDash

  def lineDash_=(pattern: Seq[Float]): Unit = {
    currentState.lineDash = pattern
    paintChanged = true
  }

  def lineDashOffset: Float =
    currentState.lineDashOffset

  def lineDashOffset_=(offset: Float): Unit = {
    currentState.lineDashOffset = offset
    paintChanged = true
  }

  def style: PaintStyle =
    currentState.style

  def style_=(style: PaintStyle): Unit = {
    currentState.style = style
    paintChanged = true
  }

  def globalAlpha: Float =
    currentState.globalAlpha

  def globalAlpha_=(value: Float): Unit = {
    currentState.globalAlpha = value
    paintChanged = true
  }

  def globalCompositeOperation: GlobalCompositeOperation =
    currentState.globalCompositeOperation

  def globalCompositeOperation_=(value: GlobalCompositeOperation): Unit = {
    currentState.globalCompositeOperation = value
    paintChanged = true
  }

  def miterLimit: Float =
    currentState.miterLimit

  def miterLimit_=(value: Float): Unit = {
    currentState.miterLimit = value
    paintChanged = true
  }

  def lineCap: LineCap =
    currentState.lineCap

  def lineCap_=(value: LineCap): Unit = {
    currentState.lineCap = value
    paintChanged = true
  }

  def lineJoin: LineJoin =
    currentState.lineJoin

  def lineJoin_=(value: LineJoin): Unit = {
    currentState.lineJoin = value
    paintChanged = true
  }

  def stroke(path: Path2d): Unit = {
    ensuringPaint {
      paint.setStyle(Paint.Style.STROKE)
      canvas.drawPath(path.asInstanceOf[AndroidPath].path, paint)
    }
  }

  def fill(path: Path2d): Unit =
    ensuringPaint {
      paint.setStyle(Paint.Style.FILL)
      canvas.drawPath(path.asInstanceOf[AndroidPath].path, paint)
    }

  def clip(path: Path2d): Unit =
    canvas.clipPath(path.asInstanceOf[AndroidPath].path)

  def save(): Unit = {
    canvas.save()
    stack.push(currentState)
    currentState = currentState.copy()
  }

  def restore(): Unit = {
    canvas.restore()
    currentState = stack.pop()
  }

  def fillRect(x: Float, y: Float, width: Float, height: Float): Unit =
    ensuringPaint {
      paint.setStyle(Paint.Style.FILL)
      canvas.drawRect(x, y, x+width, y+height, paint)
    }

  def strokeRect(x: Float, y: Float, width: Float, height: Float): Unit =
    ensuringPaint {
      paint.setStyle(Paint.Style.STROKE)
      canvas.drawRect(x, y, x+width, y+height, paint)
    }

  def clearRect(x: Float, y: Float, width: Float, height: Float): Unit =
    ensuringPaint {
      val mode = paint.getXfermode
      paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR))
      val style = paint.getStyle
      paint.setStyle(Paint.Style.FILL)
      canvas.drawRect(x, y, x+width, y+height, paint)
      paint.setStyle(style)
      paint.setXfermode(mode)
    }


  implicit private def imgToPeer(img: Image): Bitmap =
    img.asInstanceOf[ImageImpl].peer

  def drawImage(image: Image, dx: Float, dy: Float): Unit =
    canvas.drawBitmap(image.asInstanceOf[ImageImpl].peer, dx, dy, paint)

  def drawImage(image: Image, dx: Float, dy: Float, dWidth: Float, dHeight: Float): Unit = {
    val r = new android.graphics.RectF(dx, dy, dx+dWidth, dy+dHeight)
    ensuringPaint {
      canvas.drawBitmap(image, new android.graphics.Rect(0, 0, image.width, image.height), r, paint)
    }
  }

  def drawImage(image: Image, sx: Float, sy: Float, sWidth: Float, sHeight: Float, dx: Float, dy: Float, dWidth: Float, dHeight: Float): Unit = {
    val r = new android.graphics.RectF(dx, dy, dx+dWidth, dy+dHeight)
    ensuringPaint {
      canvas.drawBitmap(image, new android.graphics.Rect(sx.toInt, sy.toInt, (sx+sWidth).toInt, (sy+sHeight).toInt), r, paint)
    }
  }

  def translate(tx: Float, ty: Float): Unit =
    canvas.translate(tx, ty)

  def scale(sx: Float, sy: Float): Unit =
    canvas.scale(sx, sy)

  def rotate(angle: Float): Unit =
    canvas.rotate(angle)

  def transform(a: Float, b: Float, c: Float, d: Float, e: Float, f: Float): Unit = {
    val m = new Matrix()
    m.setValues(Array(a, b, c, d, e, f))
    val out = canvas.getMatrix
    out.preConcat(m)
    canvas.setMatrix(out)
  }

  def setTransform(a: Float, b: Float, c: Float, d: Float, e: Float, f: Float): Unit = {
    val m = new Matrix()
    m.setValues(Array(a, b, c, d, e, f))
    m.postConcat(initialMatrix)
    canvas.setMatrix(m)
  }

  def textAlign =
    currentState.textAlign

  def textAlign_=(align: TextAlign): Unit =
    currentState.textAlign = align

  def textBaseline =
    currentState.textBaseline

  def textBaseline_=(baseline: TextBaseline): Unit =
    currentState.textBaseline = baseline

  def font =
    currentState.font

  def font_=(font: Font): Unit = {
    currentState.font = font
    paintChanged = true
  }

  private def offsetText(text: String, x: Float, y: Float): (Float, Float) = {
    val x1 = textAlign match {
      case TextAlign.Left => x
      case TextAlign.Start => x
      case TextAlign.Center => x-font.stringWidth(text)/2
      case TextAlign.Right => x-font.stringWidth(text)
      case TextAlign.End => x-font.stringWidth(text)
    }

    val y1 = textBaseline match {
      case TextBaseline.Alphabetic => y
      case TextBaseline.Middle => y+font.ascent/2-font.descent
      case TextBaseline.Bottom => y-font.descent
      case TextBaseline.Top => y+font.ascent
      case TextBaseline.Hanging => y+font.ascent+font.descent
    }
    (x1, y1)
  }

  def fillText(text: String, x: Float, y: Float): Unit =
    ensuringPaint {
      paint.setStyle(Paint.Style.FILL)
      val (x1, y1) = offsetText(text, x, y)
      canvas.drawText(text, x1, y1, paint)
    }

  def strokeText(text: String, x: Float, y: Float): Unit =
    ensuringPaint {
      paint.setStyle(Paint.Style.STROKE)
      val (x1, y1) = offsetText(text, x, y)
      canvas.drawText(text, x1, y1, paint)
    }
}
