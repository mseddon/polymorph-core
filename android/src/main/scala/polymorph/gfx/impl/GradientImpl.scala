package polymorph.gfx.impl

import android.graphics.Shader.TileMode
import polymorph.gfx.{Color, RadialGradient, LinearGradient}
import scryetek.vecmath.Vec2

/**
 * Created by Matt on 21/07/2015.
 */
object GradientImpl {
  def initLinear(start: Vec2, end: Vec2, colors: Seq[(Float, Color)]): LinearGradient = {
    val grad = new android.graphics.LinearGradient(start.x, start.y, end.x, end.y,
      (for((offset, color) <- colors) yield ((color.a*255).toInt << 24) | ((color.r*255).toInt << 16) | ((color.g*255).toInt << 8) | (color.b*255).toInt).toArray,
      (for((offset, color) <- colors) yield 0f max offset min 1f).toArray,
      TileMode.CLAMP
    )
    new LinearGradient(grad)
  }
  def initRadial(center: Vec2, radius: Float, colors: Seq[(Float, Color)]): RadialGradient = {
    val grad = new android.graphics.RadialGradient(center.x, center.y, radius,
      (for((offset, color) <- colors) yield ((color.a*255).toInt << 24) | ((color.r*255).toInt << 16) | ((color.g*255).toInt << 8) | (color.b*255).toInt).toArray,
      (for((offset, color) <- colors) yield 0f max offset min 1f).toArray,
      TileMode.CLAMP
    )
    new RadialGradient(grad)
  }
}
