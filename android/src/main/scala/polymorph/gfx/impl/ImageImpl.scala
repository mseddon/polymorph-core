package polymorph.gfx.impl

import java.nio.ByteBuffer

import android.graphics.{Bitmap, BitmapFactory, Canvas}
import polymorph.gfx._
import polymorph.io.ByteBufferInputStream
import scala.concurrent.Future

/**
 * Created by Matt on 04/07/2015.
 */
class ImageImpl private[polymorph] (private[polymorph] val peer: Bitmap) extends Image {
  def width = peer.getWidth
  def height = peer.getHeight
  def gfx = new AndroidGfx(new Canvas(peer))
}

object ImageImpl {
  import scala.concurrent.ExecutionContext.Implicits.global
  def apply(width: Int, height: Int): Image =
    new ImageImpl(Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888))

  private val options = new BitmapFactory.Options()
  options.inScaled = false
  def fromResource(file: String): Future[Image] = Future {
    val f = if(file(0) == '/') file.drop(1) else file
    new ImageImpl(BitmapFactory.decodeStream(getClass.getResourceAsStream("/assets/"+f), null, options).copy(Bitmap.Config.ARGB_8888, true))
  }

  def fromByteBuffer(byteBuffer: ByteBuffer): Future[Image] = Future {
    new ImageImpl(BitmapFactory.decodeStream(new ByteBufferInputStream(byteBuffer)))
  }
}