package polymorph.gfx.impl

import android.graphics.{Paint, Typeface}
import polymorph.gfx.Font

/**
 * Created by Matt on 22/07/2015.
 */
class FontImpl(name: String, style: Font.Style, size: Float) extends Font(name, style, size) {
  val tf = Typeface.create(name, style match {
    case Font.Bold => Typeface.BOLD
    case Font.Plain => Typeface.NORMAL
    case Font.Italic => Typeface.ITALIC
    case Font.BoldItalic => Typeface.BOLD_ITALIC
  })

  val (ascent, descent, height) = {
    FontImpl.paint.setTypeface(tf)
    FontImpl.paint.setTextSize(size)
    val fm = FontImpl.paint.getFontMetrics
    (-fm.ascent, fm.descent, -fm.ascent+fm.descent)
  }

  def stringWidth(text: String): Float = {
    FontImpl.paint.setTypeface(tf)
    FontImpl.paint.setTextSize(size)
    FontImpl.paint.measureText(text)
  }
}

object FontImpl {
  val paint = new Paint()
  def apply(name: String, style: Font.Style, size: Float): Font =
    new FontImpl(name, style, size)
}
