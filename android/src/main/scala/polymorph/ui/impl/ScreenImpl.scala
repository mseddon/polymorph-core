package polymorph.ui.impl

import android.graphics.Point
import android.util.DisplayMetrics
import polymorph.impl.AppImpl
import scryetek.vecmath.Vec2

/**
  * Created by Matt on 31/07/2015.
  */
object ScreenImpl {
  val metrics = new DisplayMetrics()
  AppImpl.activity.getWindowManager.getDefaultDisplay.getMetrics(metrics)

  val _size = new Point()
  AppImpl.activity.getWindowManager.getDefaultDisplay.getSize(_size)

  def dpi(): Int =
    ((metrics.xdpi+metrics.ydpi)/2).toInt

  def size(): Vec2 =
     Vec2(_size.x, _size.y)
 }
