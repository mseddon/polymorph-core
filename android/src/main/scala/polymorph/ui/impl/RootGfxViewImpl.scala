package polymorph.ui.impl

import java.util.{TimerTask, Timer}

import android.content.Context
import android.graphics.{Paint, Canvas}
import android.view.View
import android.view.inputmethod.InputMethodManager
import polymorph.event.impl.{KeyEventReceiverProxy, MouseEventReceiverProxy, TouchEventReceiverProxy}
import polymorph.gfx.impl.AndroidGfx
import polymorph.impl.AppImpl
import polymorph.ui.{Cursor, RootGfxViewPeer, RootGfxView}
import scryetek.vecmath.Vec2

/**
 * Created by Matt on 27/07/2015.
 */
class RootGfxViewImpl(view: RootGfxView, _title: String, withId: String) extends RootGfxViewPeer(view) {
  val _peer = new View(AppImpl.activity)
      with TouchEventReceiverProxy
      with MouseEventReceiverProxy
      with KeyEventReceiverProxy {
    val receiver = view
    val paint = new Paint()
    paint.setColor(0xFFFFFFFF)
    paint.setStyle(Paint.Style.FILL)
    override def onDraw(c: Canvas): Unit = {
      c.clipRect(0, 0, width, height)
      c.drawRect(0, 0, width, height, paint)
      view.paint(new AndroidGfx(c))
    }
  }

  AppImpl.activity.setContentView(_peer)

  override def globalPosition: Vec2 = Vec2()

  override def visible_=(visible: Boolean): Unit = _peer.setVisibility(if(visible) View.VISIBLE else View.GONE)

  override def repaint(): Unit = _peer.postInvalidate()

  override def visible: Boolean = _peer.getVisibility == View.VISIBLE

  override def title: String = ""

  override def height: Float = _peer.getHeight

  override def width: Float = _peer.getWidth

  override def title_=(title: String): Unit = {}

  def focus(): Unit = {
    _peer.requestFocus()
    val imm = AppImpl.activity.getSystemService(Context.INPUT_METHOD_SERVICE).asInstanceOf[InputMethodManager]
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, InputMethodManager.HIDE_IMPLICIT_ONLY)
  }

  def blur(): Unit = {
    _peer.clearFocus()
    val imm = AppImpl.activity.getSystemService(Context.INPUT_METHOD_SERVICE).asInstanceOf[InputMethodManager]
    imm.hideSoftInputFromInputMethod(_peer.getWindowToken, 0)
  }

  var _animate = false

  def animate = _animate
  def animate_=(value: Boolean): Unit = {
    if(_animate != value) {
      _animate = value
      if(value)
        timer.schedule(new TimerTask {
          override def run(): Unit = repaint()
        }, 0, 25)
      else
        timer.cancel()
    }
  }

  val timer = new Timer

  var _cursor: Cursor = Cursor.Default

  def cursor: Cursor =
    _cursor

  def cursor_=(cursor: Cursor) =
    _cursor = cursor
}


object RootGfxViewImpl {
  def apply(view: RootGfxView, title: String, withId: String): RootGfxViewPeer =
    new RootGfxViewImpl(view, title, withId)
}
