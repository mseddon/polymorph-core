package scryetek.gl

import scryetek.vecmath.{Mat4, Vec2, Vec3}

import scala.concurrent.Future

class MainApp extends GLApp {
  var modelView: GLUniformLocation = null
  var projection: GLUniformLocation = null
  var program: GLProgram = null
  var sampler: GLUniformLocation = null
  var touches = false
  val fragmentShader =
    """#ifdef GL_ES
      |precision mediump float;
      |#endif
      |varying vec2 texCoord;
      |uniform sampler2D texture;
      |
      |void main(void) {
      |    gl_FragColor = texture2D(texture, texCoord);
      |}
    """.stripMargin('|')

  val vertexShader =
    """attribute vec3 aVertexPosition;
      |attribute vec2 aTexCoord;
      |uniform mat4 modelView;
      |uniform mat4 projection;
      |varying vec2 texCoord;
      |void main(void) {
      |    texCoord = aTexCoord;
      |    gl_Position = projection * modelView * vec4(aVertexPosition, 1.0);
      |}
    """.stripMargin('|')

  var image: Future[GLTexture] = null

  override def init(gl: GL): Future[_] = {
    position = Vec3(-1.5f, 0, -7)

    val fp = gl.createShader(GL.FRAGMENT_SHADER)
    val vp = gl.createShader(GL.VERTEX_SHADER)

    gl.shaderSource(fp, fragmentShader)
    gl.compileShader(fp)

    gl.shaderSource(vp, vertexShader)
    gl.compileShader(vp)

    program = gl.createProgram()
    gl.attachShader(program, fp)
    gl.attachShader(program, vp)
    gl.linkProgram(program)
    gl.useProgram(program)
    println(gl.getError())

    println(gl.getShaderInfoLog(fp))

    val vertexAttribute = gl.getAttribLocation(program, "aVertexPosition")
    modelView = gl.getUniformLocation(program, "modelView")
    projection = gl.getUniformLocation(program, "projection")

    val points = BufferUtils.allocateFloatBuffer(Array[Float](
      -1f, -1f, 0f,
      0,  0,
      1f, -1f, 0f,
      1,  0,
      -1f,  1f, 0f,
      0,  1,
      1f,  1f, 0f,
      1, 1))

    val triangles = gl.createBuffer()
    gl.bindBuffer(GL.ARRAY_BUFFER, triangles)
    gl.bufferData(GL.ARRAY_BUFFER, points, GL.STATIC_DRAW)
    gl.vertexAttribPointer(vertexAttribute, 3, GL.FLOAT, false, 5*4, 0)
    gl.enableVertexAttribArray(vertexAttribute)

    val texCoord = gl.getAttribLocation(program, "aTexCoord")
    gl.enableVertexAttribArray(texCoord)
    gl.vertexAttribPointer(texCoord, 2, GL.FLOAT, false, 5*4, 3*4)


    mouseDown = { e =>
      touches = true
      repaint()
    }

    mouseUp = { e =>
      touches = false
      animate = !animate
      repaint()
    }

    mouseDrag = { e =>
      val pos = e.position
      position = Vec3(pos.x/width, pos.y/height, position.z)
      repaint()
    }

    keyDown = { e =>
      keyIsDown = true
      repaint()
    }

    keyUp = { e =>
      keyIsDown = false
      repaint()
    }


    touchMove = { e =>
      val pos = e.touches.head.position
      position = Vec3(pos.x/width, pos.y/height, position.z)
      repaint()
    }

    touchStart = { e =>
      touches = true
      repaint()
    }

    touchEnd = { e =>
      touches = false
      repaint()
    }

    animate = true

    val rect = Path2d()
    rect.moveTo(0, 0)
    rect.lineTo(64, 0)
    rect.lineTo(64, 64)
    rect.lineTo(0, 64)
    rect.close()

    val tri = Path2d()
    tri.moveTo(0, 0)
    tri.lineTo(10, 10)
    tri.lineTo(10, 0)
    tri.close()

    Image.fromResource("/nehe.jpg").map { image =>
      Image.fromResource("/folder_closed.png").map { folder =>
        val img = Image(64, 64)
        val g = img.gfx
        val pattern = RadialGradient(Vec2(32,32), 32, Seq(0f -> Color(0, 0, 0), 1f -> Color(1,1,1)))
        g.style = pattern
        g.fill(rect)
        g.style = Color(1, 1, 0)
        g.font = Font("Arial", Font.Bold, 24)
        g.textAlign = TextAlign.Center
        g.textBaseline = TextBaseline.Middle
        g.fillText("Hello", 32, 32)
        g.style = Color(0, 0, 0)
        g.lineWidth = 1
        g.strokeText("Hello", 32, 32)
        g.style = Color(1, 0, 0)
        g.strokeRect(0, 25-g.font.ascent, g.font.stringWidth("Hello"), g.font.height)
        withGL { gl =>
          texture = gl.createTexture()
          gl.activeTexture(GL.TEXTURE0)
          gl.bindTexture(GL.TEXTURE_2D, texture)
          gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MAG_FILTER, GL.LINEAR)
          gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_MIN_FILTER, GL.LINEAR)
          gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_S, GL.REPEAT)
          gl.texParameteri(GL.TEXTURE_2D, GL.TEXTURE_WRAP_T, GL.REPEAT)
          gl.texImage2D(GL.TEXTURE_2D, 0, GL.RGBA, GL.RGBA, GL.UNSIGNED_BYTE, image)
          gl.texSubImage2D(GL.TEXTURE_2D, 0, 0, 0, GL.RGBA, GL.UNSIGNED_BYTE, img)
        }
      }
    }

  }
  var texture: GLTexture = null
  var keyIsDown = false
  var position: Vec3 = null

  override def display(gl: GL): Unit = {
    gl.viewport(0, 0, width, height)
    gl.clearColor(0.1f, 0.1f, Math.random().toFloat, 1)

    gl.activeTexture(GL.TEXTURE0)

    if(touches)
      gl.clearColor(1, 0, 0, 1)
    if(keyIsDown)
      gl.clearColor(0, 0, 1, 1)

    gl.clear(GL.COLOR_BUFFER_BIT | GL.DEPTH_BUFFER_BIT)
    gl.uniformMatrix4fv(modelView, transpose = false, Mat4().translate(position).toArray(new Array[Float](16)))
    gl.uniformMatrix4fv(projection, transpose = false, Mat4().perspective(45, 1f, 0.1f, 100.0f).toArray(new Array[Float](16)))
    gl.uniform1i(gl.getUniformLocation(program, "texture"), 0)
    gl.drawArrays(GL.TRIANGLE_STRIP, 0, 4)
  }

  override def dispose(gl: GL): Unit = {
  }
}
