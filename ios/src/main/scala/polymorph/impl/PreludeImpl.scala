package polymorph.impl

import org.robovm.apple.foundation.Foundation

/**
 * Created by Matt on 30/07/2015.
 */
object PreludeImpl {
  def log(target: String, string: String): Unit =
    Foundation.log(target+": "+string)
}
