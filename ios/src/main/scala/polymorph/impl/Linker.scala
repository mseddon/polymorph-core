package polymorph.impl

import polymorph.{PreferencesPeer, Preferences, Platform}
import polymorph.gfx._
import polymorph.gfx.impl._
import polymorph.ui.{CursorImpl, Cursor, RootGfxView}
import polymorph.ui.impl.RootGfxViewImpl

/**
 * Created by Matt on 23/07/2015.
 */
object Linker {
  def link(): Unit = {
    Image._imageCreate = ImageImpl.apply
    Image._imageFromResource = ImageImpl.fromResource
    Image._imageFromByteBuffer= ImageImpl.fromByteBuffer
    Pattern.init = PatternImpl.init
    LinearGradient.init = GradientImpl.initLinear
    RadialGradient.init = GradientImpl.initRadial
    Path2d.init = IOSPath2d.apply
    Font.init = IOSFont.apply
    RootGfxView._peer = RootGfxViewImpl.apply
    Platform._peer = new PlatformImpl
    polymorph.net.Http.httpClient = new polymorph.net.impl.HttpImpl
    polymorph.Prelude._log = PreludeImpl.log
    polymorph.ui.Screen._dpi = polymorph.ui.impl.ScreenImpl.dpi
    polymorph.ui.Screen._screenSize = polymorph.ui.impl.ScreenImpl.size
    polymorph.Resource._getResource = ResourceImpl.getResource
    Preferences.impl = new PreferencesImpl
    Cursor.cursorImpl = new CursorImpl

    polymorph.timer._clearTimer  = TimerImpl.clearTimer
    polymorph.timer._setTimeout  = TimerImpl.setTimeout
    polymorph.timer._setInterval = TimerImpl.setInterval
  }
}
