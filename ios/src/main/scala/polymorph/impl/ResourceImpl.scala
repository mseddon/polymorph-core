package polymorph.impl

import java.io.ByteArrayOutputStream
import java.nio.ByteBuffer

import scala.concurrent.Future

object ResourceImpl {
  import concurrent.ExecutionContext.Implicits.global
   def getResource(file: String): Future[ByteBuffer] = Future {
     val out = new ByteArrayOutputStream()
     val is = getClass.getResourceAsStream(file)
     val buf = new Array[Byte](0xFFFF)
     var len = is.read(buf)

     while (len != -1) {
       out.write(buf, 0, len)
       len = is.read(buf)
     }
     out.flush()
     ByteBuffer.wrap(out.toByteArray)
   }
 }
