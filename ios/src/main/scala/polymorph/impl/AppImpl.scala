package polymorph.impl

import org.robovm.apple.foundation.NSAutoreleasePool
import org.robovm.apple.uikit.{UIApplication, UIApplicationDelegateAdapter, UIApplicationDelegate}

import scala.concurrent.Await

/**
 * Created by Matt on 27/07/2015.
 */

class AppDelegate extends UIApplicationDelegateAdapter {
  override def didFinishLaunching(application: UIApplication): Unit = {
    val app = AppImpl.app
    Await.result(app.init(Array()), app.initTimeout)
    app.main(Array())
  }
}

object AppImpl {
  var app: polymorph.App = null
}

class AppImpl(app: polymorph.App) extends App {
  override def main(args: Array[String]): Unit = {
    AppImpl.app = app
    val pool = new NSAutoreleasePool()
    UIApplication.main(Array[String](), null, classOf[AppDelegate])
    pool.drain()
  }
}
