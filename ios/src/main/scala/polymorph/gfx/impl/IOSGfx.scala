package polymorph.gfx.impl

import org.robovm.apple.corefoundation.{CFAllocator, CFAttributedString, CFBoolean, CFDictionary}
import org.robovm.apple.coregraphics._
import org.robovm.apple.coretext.{CTAttributedStringAttribute, CTLine}
import org.robovm.apple.foundation.NSAttributedString
import polymorph.Prelude
import polymorph.gfx._
import scryetek.vecmath.Vec2

import scala.language.implicitConversions

  object IOSPath2d {
    def apply(): Path2d =
      new IOSPath2d
  }

  class IOSPath2d extends Path2d {
    private [polymorph] sealed trait Path2dOp {
      def eval(ctx: CGContext): Unit
    }

    private [polymorph] case class MoveTo(x: Float, y: Float) extends Path2dOp {
      def eval(ctx: CGContext): Unit =
        ctx.moveToPoint(x, y)
    }

    private [polymorph] case class LineTo(x: Float, y: Float) extends Path2dOp {
      def eval(ctx: CGContext): Unit =
        ctx.addLineToPoint(x, y)
    }

    private [polymorph] case class QuadricCurveTo(cpx: Float, cpy: Float, x: Float, y: Float) extends Path2dOp {
      def eval(ctx: CGContext): Unit =
        ctx.addQuadCurveToPoint(cpx, cpy, x, y)
    }

    private [polymorph] case class BezierCurveTo(cp0x: Float, cp0y: Float, cp1x: Float, cp1y: Float, x: Float, y: Float) extends Path2dOp {
      def eval(ctx: CGContext): Unit =
        ctx.addCurveToPoint(cp0x, cp0y, cp1x, cp1y, x, y)
    }

    private [polymorph] case class Arc(x: Float, y: Float, radius: Float, startAngle: Float, endAngle: Float, anticlockwise: Boolean = false) extends Path2dOp {
      def eval(ctx: CGContext): Unit =
        ctx.addArc(x, y, radius, startAngle, endAngle, if (anticlockwise) 0 else 1)
    }

    private [polymorph] case object Close extends Path2dOp {
      def eval(ctx: CGContext): Unit =
        ctx.closePath()
    }

    var ops = Seq[Path2dOp]()

    def moveTo(x: Float, y: Float): Unit = {
      ops :+= MoveTo(x, y)
      _currentPosition = Vec2(x, y)
    }

    def lineTo(x: Float, y: Float): Unit = {
      ops :+= LineTo(x, y)
      _currentPosition = Vec2(x, y)
    }

    def quadricCurveTo(cpx: Float, cpy: Float, x: Float, y: Float): Unit = {
      ops :+= QuadricCurveTo(cpx, cpy, x, y)
      _currentPosition = Vec2(x, y)
    }

    def bezierCurveTo(cp0x: Float, cp0y: Float, cp1x: Float, cp1y: Float, x: Float, y: Float): Unit = {
      ops :+= BezierCurveTo(cp0x, cp0y, cp1x, cp1y, x, y)
      _currentPosition = Vec2(x, y)
    }

    def arc(x: Float, y: Float, radius: Float, startAngle: Float, endAngle: Float, anticlockwise: Boolean = false): Unit = {
      ops :+= Arc(x, y, radius, startAngle, endAngle, anticlockwise)
      _currentPosition = Vec2(Math.cos(endAngle).toFloat*radius + x, Math.sin(endAngle).toFloat*radius + y)
    }

    private [polymorph] def execute(ctx: CGContext): Unit = {
      ctx.beginPath()
      for(op <- ops)
        op.eval(ctx)
    }

    def close(): Unit =
      ops :+= Close

    private var _currentPosition = Vec2(0, 0)

    def currentPosition = _currentPosition
  }

  /**
   * Created by Matt on 12/07/2015.
   */
  class IOSGfx(var ctx: CGContext) extends Gfx {

    private case class State(
        var lineWidth: Float = 1,
        var lineDashOffset: Float = 0,
        var lineDash: Seq[Float] = Seq(),
        var lineCap: LineCap = LineCap.Round,
        var lineJoin: LineJoin = LineJoin.Round,
        var miterLimit: Float = 0,
        var globalAlpha: Float = 1,
        var globalCompositeOperation: GlobalCompositeOperation = GlobalCompositeOperation.SrcOver,
        var style: PaintStyle = Color(0, 0, 0),
        var font: Font = Font("Arial", Font.Plain, 12),
        var textAlign: TextAlign = TextAlign.Left,
        var textBaseline: TextBaseline = TextBaseline.Alphabetic
    )

    private val stack = collection.mutable.Stack[State]()

    private [polymorph] var initialMatrix = ctx.getCTM
    private var currentState = State()
    private var changedLineDash = true
    private var changedLineCap = true
    private var changedLineJoin = true
    private var changedLineWidth = true
    private var changedMiterLimit = true
    private var changedStyle = true
    private var changedGlobalAlpha = true
    private var changedGlobalCompositeOperation = true
    var _changed = false

    ctx.setTextMatrix(new CGAffineTransform(1, 0, 0, -1, 0, 0))

    private val cs = CGColorSpace.createDeviceRGB()
    private val ps = CGColorSpace.createPattern(null)

    private def ensureSync(body: => Unit): Unit = {
      if (changedLineDash) {
        changedLineDash = false
        ctx.setLineDash(currentState.lineDashOffset, lineDash.map(_.toDouble).toArray)
      }
      if (changedLineCap) {
        ctx.setLineCap(currentState.lineCap match {
          case LineCap.Round => CGLineCap.Round
          case LineCap.Butt => CGLineCap.Butt
          case LineCap.Square => CGLineCap.Square
        })
        changedLineCap = false
      }
      if (changedLineJoin) {
        ctx.setLineJoin(currentState.lineJoin match {
          case LineJoin.Bevel => CGLineJoin.Bevel
          case LineJoin.Round => CGLineJoin.Round
          case LineJoin.Miter => CGLineJoin.Miter
        })
        changedLineJoin = false
      }
      if (changedLineWidth) {
        ctx.setLineWidth(currentState.lineWidth)
        changedLineWidth = false
      }
      if (changedMiterLimit) {
        ctx.setMiterLimit(currentState.miterLimit)
        changedMiterLimit = false
      }
      if (changedStyle) {
        currentState.style match {
          case Color(r, g, b, a) =>
            ctx.setFillColorSpace(cs)
            ctx.setStrokeColorSpace(cs)
            ctx.setRGBStrokeColor(r, g, b, a)
            ctx.setRGBFillColor(r, g, b, a)
          case p: Pattern =>
            ctx.setFillColorSpace(ps)
            ctx.setStrokeColorSpace(ps)
            ctx.setFillPattern(p.peer.asInstanceOf[CGPattern], Array(1f))
            ctx.setStrokePattern(p.peer.asInstanceOf[CGPattern], Array(1f))
          case g: LinearGradient =>
            ctx.setFillColorSpace(ps)
            ctx.setStrokeColorSpace(ps)
            ctx.setFillPattern(g.peer.asInstanceOf[CGPattern], Array(1f))
            ctx.setStrokePattern(g.peer.asInstanceOf[CGPattern], Array(1f))
          case g: RadialGradient =>
            ctx.setFillColorSpace(ps)
            ctx.setStrokeColorSpace(ps)
            ctx.setFillPattern(g.peer.asInstanceOf[CGPattern], Array(1f))
            ctx.setStrokePattern(g.peer.asInstanceOf[CGPattern], Array(1f))
        }
        changedStyle = false
      }
      if (changedGlobalAlpha) {
        ctx.setAlpha(currentState.globalAlpha)
        changedGlobalAlpha = false
      }
      if (changedGlobalCompositeOperation) {
        ctx.setBlendMode(currentState.globalCompositeOperation match {
          case GlobalCompositeOperation.SrcAtop => CGBlendMode.SourceAtop
          case GlobalCompositeOperation.SrcIn => CGBlendMode.SourceIn
          case GlobalCompositeOperation.SrcOut => CGBlendMode.SourceOut
          case GlobalCompositeOperation.SrcOver => CGBlendMode.Normal
          case GlobalCompositeOperation.DstAtop => CGBlendMode.DestinationAtop
          case GlobalCompositeOperation.DstIn => CGBlendMode.DestinationIn
          case GlobalCompositeOperation.DstOut => CGBlendMode.DestinationOut
          case GlobalCompositeOperation.DstOver => CGBlendMode.DestinationOver
//          case GlobalCompositeOperation.Multiply => CGBlendMode.Multiply
//          case GlobalCompositeOperation.Screen => CGBlendMode.Screen
//          case GlobalCompositeOperation.Add => CGBlendMode.PlusLighter
        })
        changedGlobalCompositeOperation = false
      }
      body
    }

    def lineWidth: Float =
      currentState.lineWidth

    def lineWidth_=(value: Float): Unit = {
      currentState.lineWidth = value
      changedLineWidth = true
    }

    def lineDash: Seq[Float] =
      currentState.lineDash

    def lineDash_=(pattern: Seq[Float]): Unit = {
      changedLineDash = true
      currentState.lineDash = pattern
    }

    def lineDashOffset: Float =
      currentState.lineDashOffset

    def lineDashOffset_=(offset: Float): Unit = {
      currentState.lineDashOffset = offset
      changedLineDash = true
    }

    def style: PaintStyle =
      currentState.style

    def style_=(style: PaintStyle): Unit = {
      currentState.style = style
      changedStyle = true
    }

    def globalAlpha: Float =
      currentState.globalAlpha

    def globalAlpha_=(value: Float): Unit = {
      currentState.globalAlpha = value
      changedGlobalAlpha = true
    }

    def globalCompositeOperation: GlobalCompositeOperation =
      currentState.globalCompositeOperation

    def globalCompositeOperation_=(value: GlobalCompositeOperation): Unit = {
      currentState.globalCompositeOperation = value
      changedGlobalCompositeOperation = true
    }

    def miterLimit: Float =
      currentState.miterLimit

    def miterLimit_=(value: Float): Unit = {
      currentState.miterLimit = value
      changedMiterLimit = true
    }

    def lineCap: LineCap =
      currentState.lineCap

    def lineCap_=(value: LineCap): Unit = {
      currentState.lineCap = value
      changedLineCap = true
    }

    def lineJoin: LineJoin =
      currentState.lineJoin

    def lineJoin_=(value: LineJoin): Unit = {
      currentState.lineJoin = value
      changedLineJoin = true
    }

    def stroke(path: Path2d): Unit =
      ensureSync {
        path.asInstanceOf[IOSPath2d].execute(ctx)
        ctx.strokePath()
        _changed = true
      }

    def fill(path: Path2d): Unit =
      ensureSync {
        path.asInstanceOf[IOSPath2d].execute(ctx)
        ctx.fillPath()
        _changed = true
      }

    def clip(path: Path2d): Unit =
      ensureSync {
        path.asInstanceOf[IOSPath2d].execute(ctx)
        ctx.clip()
        _changed = true
      }

    def save(): Unit = {
      ctx.saveGState()
      stack.push(currentState)
      currentState = currentState.copy()
    }

    def restore(): Unit = {
      ctx.restoreGState()
      currentState = stack.pop()
    }

    def fillRect(x: Float, y: Float, width: Float, height: Float): Unit =
      ensureSync {
        ctx.fillRect(new CGRect(x, y, width, height))
      }

    def strokeRect(x: Float, y: Float, width: Float, height: Float): Unit =
      ensureSync {
        ctx.strokeRect(new CGRect(x, y, width, height))
      }

    def clearRect(x: Float, y: Float, width: Float, height: Float): Unit =
      ensureSync {
        ctx.clearRect(new CGRect(x, y, width, height))
      }

    private implicit def image2CGImage(image: Image): CGImage =
      image.asInstanceOf[ImageImpl].peer

    def drawImage(image: Image, dx: Float, dy: Float): Unit =
      ensureSync {
        ctx.saveGState()
        ctx.translateCTM(0, dy+image.height)
        ctx.scaleCTM(1, -1)
        ctx.drawImage(new CGRect(dx, 0, image.width, image.height), image)
        ctx.restoreGState()
        _changed = true
      }

    def drawImage(image: Image, dx: Float, dy: Float, dWidth: Float, dHeight: Float): Unit =
      ensureSync {
        ctx.saveGState()
        ctx.translateCTM(0, dy+dHeight)
        ctx.scaleCTM(1, -1)
        ctx.drawImage(new CGRect(dx, 0, dWidth, dHeight), image)
        ctx.restoreGState()
        _changed = true
      }

    def drawImage(image: Image, sx: Float, sy: Float, sWidth: Float, sHeight: Float, dx: Float, dy: Float, dWidth: Float, dHeight: Float): Unit =
      ensureSync {
        ctx.saveGState()
        ctx.translateCTM(0, dy+dHeight)
        ctx.scaleCTM(1, -1)
        ctx.drawImage(new CGRect(dx, 0, dWidth, dHeight), CGImage.createWithImageInRect(image, new CGRect(sx, sy, sWidth, sHeight)))
        ctx.restoreGState()
        _changed = true
      }

    def translate(tx: Float, ty: Float): Unit =
      ctx.translateCTM(tx, ty)

    def scale(sx: Float, sy: Float): Unit =
      ctx.scaleCTM(sx, sy)

    def rotate(angle: Float): Unit =
      ctx.rotateCTM(angle)

    def transform(a: Float, b: Float, c: Float, d: Float, e: Float, f: Float): Unit =
      ctx.concatCTM(new CGAffineTransform(a, b, c, d, e, f))

    def setTransform(a: Float, b: Float, c: Float, d: Float, e: Float, f: Float): Unit =
      ctx.concatCTM(ctx.getCTM.invert().concat(initialMatrix).concat(new CGAffineTransform(a, b, c, d, e, f)))

    private def _drawText(text: String, x: Float, y: Float): Unit = {
      import scala.collection.JavaConversions._
      val x1 = textAlign match {
        case TextAlign.Left => x
        case TextAlign.Start => x
        case TextAlign.Center => x-font.stringWidth(text)/2
        case TextAlign.Right => x-font.stringWidth(text)
        case TextAlign.End => x-font.stringWidth(text)
      }

      val y1 = textBaseline match {
        case TextBaseline.Alphabetic => y
        case TextBaseline.Middle => y+font.ascent/2-font.descent
        case TextBaseline.Bottom => y-font.descent
        case TextBaseline.Top => y+font.ascent
        case TextBaseline.Hanging => y+font.ascent+font.descent
      }
      ctx.setTextPosition (x1, y1)
      CTLine.create(CFAttributedString.create(
        CFAllocator.getDefaultAllocator,
        text,
        CFDictionary.create(
          List(CTAttributedStringAttribute.Font.value(), CTAttributedStringAttribute.ForegroundColorFromContext.value()),
          List(currentState.font.asInstanceOf[IOSFont].fnt, CFBoolean.True()))).as(classOf[NSAttributedString])).draw(ctx)
    }

    def strokeText(text: String, x: Float, y: Float): Unit = ensureSync {
      ctx.setTextDrawingMode(CGTextDrawingMode.Stroke)
      _drawText(text, x, y)
      _changed = true
    }

    def fillText(text: String, x: Float, y: Float): Unit = ensureSync {
      ctx.setTextDrawingMode(CGTextDrawingMode.Fill)
      _drawText(text, x, y)
      _changed = true
    }

    def textAlign =
      currentState.textAlign

    def textAlign_=(align: TextAlign): Unit =
      currentState.textAlign = align

    def textBaseline =
      currentState.textBaseline

    def textBaseline_=(baseline: TextBaseline): Unit =
      currentState.textBaseline = baseline

    def font =
      currentState.font

    def font_=(font: Font): Unit =
      currentState.font = font
  }