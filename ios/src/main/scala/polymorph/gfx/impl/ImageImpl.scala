package polymorph.gfx.impl

import java.io.ByteArrayOutputStream
import java.nio.ByteBuffer

import org.robovm.apple.coregraphics._
import org.robovm.apple.foundation.NSData
import org.robovm.apple.uikit.UIImage
import polymorph.gfx._

import scala.concurrent.Future
/**
 * Created by Matt on 04/07/2015.
 */
class ImageImpl private (val width: Int, val height: Int) extends Image {
  private var _peer: CGImage = null
  private var _gfx: IOSGfx = null
  _gfx = new IOSGfx(CGBitmapContext.create(width, height, 8, width*4, CGColorSpace.createDeviceRGB(), CGImageAlphaInfo.PremultipliedLast))
  _gfx.translate(0, height)
  _gfx.scale(1, -1)
  _gfx.initialMatrix = _gfx.ctx.getCTM
  private [polymorph] def this(peer: CGImage) {
    this(peer.getWidth.toInt, peer.getHeight.toInt)
    _peer = peer
    _gfx.ctx.saveGState()
    _gfx.ctx.translateCTM(0, peer.getHeight)
    _gfx.ctx.scaleCTM(1, -1)
    _gfx.ctx.drawImage(new CGRect(0, 0, _peer.getWidth, _peer.getHeight), _peer)
    _gfx.ctx.restoreGState()
    _gfx.initialMatrix = _gfx.ctx.getCTM
    _gfx._changed = true
  }

  def gfx: Gfx =
    _gfx

  def peer: CGImage = {
    if(_peer == null || _gfx._changed)
      _peer = _gfx.ctx.asInstanceOf[CGBitmapContext].toImage
    _gfx._changed = false
    _peer
  }
}

object ImageImpl {
  import scala.concurrent.ExecutionContext.Implicits.global
  def apply(width: Int, height: Int): Image =
    new ImageImpl(width, height)

  def fromResource(file: String): Future[ImageImpl] = Future {
    val out = new ByteArrayOutputStream()
    val is = getClass.getResourceAsStream(file)
    val buf = new Array[Byte](0xFFFF)
    if(is == null)
      throw new Error("Could not locate resource: "+file)
    var len = is.read(buf)

    while (len != -1) {
      out.write(buf, 0, len)
      len = is.read(buf)
    }
    out.flush()
    new ImageImpl(UIImage.create(new NSData(out.toByteArray)).getCGImage)
  }

  def fromByteBuffer(byteBuffer: ByteBuffer): Future[ImageImpl] = Future {
    val bytes = new Array[Byte](byteBuffer.remaining())
    byteBuffer.get(bytes)
    new ImageImpl(UIImage.create(new NSData(bytes)).getCGImage)
  }
}
