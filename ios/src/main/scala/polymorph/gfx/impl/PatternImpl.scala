package polymorph.gfx.impl

import org.robovm.apple.coregraphics.CGPattern.DrawPattern
import org.robovm.apple.coregraphics._
import polymorph.gfx._
/**
 * Created by Matt on 21/07/2015.
 */
object PatternImpl {
  def init(image: Image): Pattern = {
    val img = image.asInstanceOf[ImageImpl].peer
    val r = new CGRect(0, 0, image.width, image.height)
    val pat = CGPattern.create(r, new CGAffineTransform(1, 0, 0, 1, 0, 0), image.width, image.height, CGPatternTiling.ConstantSpacing, true, new DrawPattern {
      override def drawPattern(context: CGContext): Unit = {
        context.drawImage(r, img)
      }
    })
    Pattern(pat)
  }
}
