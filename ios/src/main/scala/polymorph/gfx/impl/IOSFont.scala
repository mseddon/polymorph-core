package polymorph.gfx.impl

import org.robovm.apple.corefoundation.{CFAllocator, CFAttributedString, CFDictionary, CFRange}
import org.robovm.apple.coregraphics.CGSize
import org.robovm.apple.coretext._
import org.robovm.apple.foundation.NSAttributedString
import org.robovm.apple.uikit.UIFont
import polymorph.gfx.Font

/**
 * Created by Matt on 22/07/2015.
 */
class IOSFont(name: String, style: Font.Style, size: Float) extends Font(name, style, size) {
  val fnt = {
    val fnt = UIFont.getFont(name, size).as(classOf[CTFont])
    val traits = style match {
      case Font.Plain => CTFontSymbolicTraits.None.value()
      case Font.Bold => CTFontSymbolicTraits.BoldTrait.value()
      case Font.Italic => CTFontSymbolicTraits.ItalicTrait.value()
      case Font.BoldItalic => CTFontSymbolicTraits.ItalicTrait.value() | CTFontSymbolicTraits.BoldTrait.value()
    }
    if(traits == 0)
      fnt
    else
      CTFont.createCopy(fnt, 0, null, new CTFontSymbolicTraits(traits), new CTFontSymbolicTraits(traits))
  }
  val ascent = fnt.getAscent.toFloat
  val descent = fnt.getDescent.toFloat
  val height = (fnt.getAscent + fnt.getDescent).toFloat
  def stringWidth(string: String): Float = {
    import scala.collection.JavaConversions._
    CTFramesetter.create(
      CFAttributedString.create(
        CFAllocator.getDefault,
        string,
        CFDictionary.create(
          List(CTAttributedStringAttribute.Font.value()),
          List(fnt))
      ).as(classOf[NSAttributedString]))
        .suggestFrameSize(
          new CFRange(0, string.length),
          null,
          new CGSize(Float.MaxValue, Float.MaxValue),
          null).getWidth.toFloat
  }
}

object IOSFont {
  def apply(name: String, style: Font.Style, size: Float): Font =
    new IOSFont(name, style, size)
}