package polymorph.gfx.impl

import org.robovm.apple.coregraphics.CGPattern.DrawPattern
import org.robovm.apple.coregraphics._
import polymorph.gfx.{Color, RadialGradient, LinearGradient}
import scryetek.vecmath.Vec2
/**
 * Created by Matt on 21/07/2015.
 */
object GradientImpl {
  def initLinear(start: Vec2, end: Vec2, colors: Seq[(Float, Color)]): LinearGradient = {
    val grad = CGGradient.create(CGColorSpace.createDeviceRGB(),
      (for((offset, color) <- colors) yield Seq(color.r, color.g, color.b, color.a)).flatten.toArray,
      (for((offset, color) <- colors) yield 0f max offset min 1f).toArray)

    val s = new CGPoint(start.x, start.y)
    val e = new CGPoint(end.x, end.y)
    val both = CGGradientDrawingOptions.AfterEndLocation.set(CGGradientDrawingOptions.BeforeStartLocation)
    val pat = CGPattern.create(new CGRect(0, 0, 8192, 8192), new CGAffineTransform(1, 0, 0, 1, 0, 0), 8192, 8192, CGPatternTiling.ConstantSpacing, true, new DrawPattern {
      override def drawPattern(context: CGContext): Unit = {
        context.drawLinearGradient(grad, s, e, both)
      }
    })

    new LinearGradient(pat)
  }

  def initRadial(center: Vec2, radius: Float, colors: Seq[(Float, Color)]): RadialGradient = {
    val grad = CGGradient.create(CGColorSpace.createDeviceRGB(),
      (for((offset, color) <- colors) yield Seq(color.r, color.g, color.b, color.a)).flatten.toArray,
      (for((offset, color) <- colors) yield 0f max offset min 1f).toArray)

    val c = new CGPoint(center.x, center.y)
    val both = CGGradientDrawingOptions.AfterEndLocation.set(CGGradientDrawingOptions.BeforeStartLocation)
    val pat = CGPattern.create(new CGRect(0, 0, 8192, 8192), new CGAffineTransform(1, 0, 0, 1, 0, 0), 8192, 8192, CGPatternTiling.ConstantSpacing, true, new DrawPattern {
      override def drawPattern(context: CGContext): Unit = {
        context.drawRadialGradient(grad, c, 0, c, radius, both)
      }
    })

    new RadialGradient(pat)
  }
}
