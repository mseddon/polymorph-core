package polymorph.event.impl

import org.robovm.apple.foundation.NSSet
import org.robovm.apple.uikit.{UIViewController, UIEvent, UITouch, UIView}
import polymorph.event._
import scryetek.vecmath.Vec2


trait TouchEventReceiverProxy extends UIViewController {
  val receiver: TouchEventReceiver
  val view: UIView
  def getTouches(touches: NSSet[UITouch]): Seq[Touch] = {
    val values = touches.getValues
    for (i <- 0 until values.size()) yield {
      val touch = values.get(0)
      val loc = touch.getLocationInView(view)
      val pos = Vec2(loc.getX.toFloat, loc.getY.toFloat).scale(view.getContentScaleFactor.toFloat)
      Touch(touch.getHandle, receiver, pos, pos, pos)
    }
  }

  override def touchesBegan(touches: NSSet[UITouch], uiEvent: UIEvent): Unit =
    receiver.touchStart(TouchStart(receiver, System.currentTimeMillis(), 0, getTouches(touches), getTouches(uiEvent.getAllTouches)))

  override def touchesEnded(touches: NSSet[UITouch], uiEvent: UIEvent): Unit =
    receiver.touchEnd(TouchEnd(receiver, System.currentTimeMillis(), 0, getTouches(touches), getTouches(uiEvent.getAllTouches)))

  override def touchesCancelled(touches: NSSet[UITouch], uiEvent: UIEvent): Unit =
    receiver.touchCancel(TouchCancel(receiver, System.currentTimeMillis(), 0, getTouches(touches), getTouches(uiEvent.getAllTouches)))

  override def touchesMoved(touches: NSSet[UITouch], uiEvent: UIEvent): Unit =
    receiver.touchMove(TouchMove(receiver, System.currentTimeMillis(), 0, getTouches(touches), getTouches(uiEvent.getAllTouches)))
}
