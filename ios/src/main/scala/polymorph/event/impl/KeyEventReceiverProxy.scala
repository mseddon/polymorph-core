package polymorph.event.impl

import org.robovm.apple.foundation.NSRange
import org.robovm.apple.uikit._
import polymorph.Prelude
import polymorph.event._

class KeyEventReceiverProxy(view: KeyEventReceiver) extends UITextField {
  val delegate = new UITextFieldDelegateAdapter {
    override def shouldChangeCharacters(textField: UITextField, range: NSRange, replacementString: String): Boolean = {
      val now = System.currentTimeMillis()
      for(i <- 0 until range.getLength.toInt)
        view.keyTyped(KeyTyped(view, now, 0, 0, 8))
      for(i <- replacementString.indices)
        view.keyTyped(KeyTyped(view, now, 0, 0, replacementString.charAt(i)))
      true
    }
  }

  setDelegate(delegate)
}
