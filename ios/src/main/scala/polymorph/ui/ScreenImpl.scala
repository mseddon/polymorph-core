package polymorph.ui.impl

import org.robovm.apple.uikit.UIScreen
import scryetek.vecmath.Vec2

/**
 * Created by Matt on 31/07/2015.
 */
object ScreenImpl {
  val _dpi = (163*UIScreen.getMainScreen.getScale).toInt

  def dpi(): Int = _dpi

  def size(): Vec2 = {
    val bounds = UIScreen.getMainScreen.getBounds
    val scale = UIScreen.getMainScreen.getScale.toFloat
    Vec2(bounds.getWidth.toFloat*scale, bounds.getHeight.toFloat*scale)
  }
}
