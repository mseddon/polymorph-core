package polymorph.ui.impl

import java.util.{Timer, TimerTask}

import org.robovm.apple.coregraphics.CGRect
import org.robovm.apple.foundation.NSOperationQueue
import org.robovm.apple.uikit._
import polymorph.event.{KeyTyped, KeyUp, KeyEvent, KeyDown}
import polymorph.event.impl.{KeyEventReceiverProxy, TouchEventReceiverProxy}
import polymorph.gfx.impl.IOSGfx
import polymorph.impl.AppImpl
import polymorph.ui.{Cursor, RootGfxViewPeer, RootGfxView}
import scryetek.vecmath.Vec2

class RootGfxViewImpl (view: RootGfxView, _title: String, withId: String) extends RootGfxViewPeer(view) {
  var _peer = new Canvas
  val _window = new UIWindow(UIScreen.getMainScreen.getBounds)

  _peer.setBackgroundColor(UIColor.white())

  _window.makeKeyAndVisible()

  def width: Float =
    (_peer.getBounds.getWidth*UIScreen.getMainScreen.getScale).toFloat

  def visible_=(visible: Boolean): Unit =
    _peer.setHidden(!visible)

  def visible: Boolean =
    !_peer.isHidden

  def title: String = ""

  def height: Float =
    (_peer.getBounds.getHeight.toFloat*UIScreen.getMainScreen.getScale).toFloat

  def title_=(title: String): Unit = {}

  def globalPosition: Vec2 =
    Vec2(0,0)

  def repaint(): Unit =
    NSOperationQueue.getMainQueue.addOperation(new Runnable() {
      override def run(): Unit =
        _peer.setNeedsDisplay()
    })

  class Canvas extends UIView {
    override def draw(rect: CGRect): Unit = {
      val gfx = new IOSGfx(UIGraphics.getCurrentContext)
      val scale = UIScreen.getMainScreen.getScale.toFloat
      gfx.scale(1/scale, 1/scale)
      gfx.initialMatrix = gfx.ctx.getCTM
      view.paint(gfx)
    }
  }

  class CanvasController extends UIViewController with TouchEventReceiverProxy {
    val receiver = RootGfxViewImpl.this.view
    val view = _peer
  }

  val _controller = new CanvasController
  _controller.setView(_peer)

  _window.setRootViewController(_controller)

  def focus(): Unit =
    keyboard.becomeFirstResponder()

  def blur(): Unit =
    keyboard.resignFirstResponder()

  val keyboard = new KeyEventReceiverProxy(view)

  _peer.addSubview(keyboard)

  var _animate = false

  def animate = _animate
  def animate_=(value: Boolean): Unit = {
    if(_animate != value) {
      _animate = value
      if(value)
        timer.schedule(new TimerTask {
          override def run(): Unit = repaint()
        }, 0, 16)
      else
        timer.cancel()
    }
  }

  val timer = new Timer()

  var _cursor: Cursor = Cursor.Default
  def cursor_=(cursor: Cursor): Unit =
    _cursor = cursor

  def cursor =
    _cursor
}

object RootGfxViewImpl {
  def apply(view: RootGfxView, _title: String, withId: String): RootGfxViewPeer =
    new RootGfxViewImpl(view, _title, withId)
}
