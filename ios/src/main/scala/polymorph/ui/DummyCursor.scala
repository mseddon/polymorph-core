package polymorph.ui

import polymorph.gfx.Image

object DummyCursor extends Cursor

class CursorImpl extends CursorPeer {
  override val CrossHair: Cursor = DummyCursor

  override def fromImage(image: Image, hotspotX: Int, hotspotY: Int): Cursor = DummyCursor

  override val ResizeS: Cursor = DummyCursor
  override val Move: Cursor = DummyCursor
  override val ResizeNW: Cursor = DummyCursor
  override val Text: Cursor = DummyCursor
  override val ResizeNE: Cursor = DummyCursor
  override val Wait: Cursor = DummyCursor
  override val ResizeSE: Cursor = DummyCursor
  override val ResizeSW: Cursor = DummyCursor
  override val ResizeE: Cursor = DummyCursor
  override val ResizeN: Cursor = DummyCursor
  override val ResizeW: Cursor = DummyCursor
  override val Hand: Cursor = DummyCursor
  override val Default: Cursor = DummyCursor
}
