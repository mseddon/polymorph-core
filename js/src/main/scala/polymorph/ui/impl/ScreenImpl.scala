package polymorph.ui.impl

import org.scalajs.dom
import scryetek.vecmath.Vec2
/**
 * Created by Matt on 31/07/2015.
 */
object ScreenImpl {
  lazy val _dpi = {
    // ah, web browsers.  Why have a way to get the screen dpi when you can have a guessing game?
    var min: Int = 1
    var max: Int = 1000
    def mid = min+(max-min)/2
    while(min < max)
      if(dpiGte(mid)) min = mid+1
      else max = mid-1
    mid
  }

  def dpiGte(n: Int): Boolean =
    dom.window.matchMedia(s"(min-resolution: ${n}dpi)").matches

  def dpiLte(n: Int): Boolean =
    dom.window.matchMedia(s"(max-resolution: ${n}dpi)").matches

  def dpi(): Int = _dpi

  def size(): Vec2 =
    Vec2(dom.window.screen.width.toFloat, dom.window.screen.height.toFloat)
}
