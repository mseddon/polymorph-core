package polymorph.ui.impl

import polymorph.gfx.Image
import polymorph.gfx.impl.ImageImpl
import polymorph.ui._

class JSCursor(val css: String) extends Cursor
class JSCursorImpl extends CursorPeer {
  override val CrossHair: Cursor = new JSCursor("crosshair")

  override def fromImage(image: Image, hotspotX: Int, hotspotY: Int): Cursor =
    new JSCursor(s"url(${image.asInstanceOf[ImageImpl].peer.toDataURL("image/png")}) $hotspotX $hotspotY")

  override val ResizeS: Cursor = new JSCursor("s-resize")
  override val Move: Cursor = new JSCursor("move")
  override val ResizeNW: Cursor = new JSCursor("nw-resize")
  override val Text: Cursor = new JSCursor("text")
  override val ResizeNE: Cursor = new JSCursor("ne-resize")
  override val Wait: Cursor = new JSCursor("wait")
  override val ResizeSE: Cursor = new JSCursor("se-resize")
  override val ResizeSW: Cursor = new JSCursor("sw-resize")
  override val ResizeE: Cursor = new JSCursor("e-resize")
  override val ResizeN: Cursor = new JSCursor("n-resize")
  override val ResizeW: Cursor = new JSCursor("w-resize")
  override val Hand: Cursor = new JSCursor("pointer")
  override val Default: Cursor = new JSCursor("default")
}
