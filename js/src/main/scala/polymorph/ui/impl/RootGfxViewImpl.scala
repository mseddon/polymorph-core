package polymorph.ui.impl

import org.scalajs.dom
import polymorph.event.impl.{KeyEventReceiverProxy, TouchEventReceiverProxy, MouseEventReceiverProxy}
import polymorph.gfx.Gfx
import polymorph.gfx.impl.JSGfx

import polymorph.ui.{Cursor, RootGfxViewPeer, RootGfxView}
import scryetek.vecmath.Vec2

class RootGfxViewImpl(view: RootGfxView, _title: String, withId: String) extends RootGfxViewPeer(view) {
  val _peer: dom.html.Canvas = if(withId == null) {
    val canvas = dom.document.createElement("canvas").asInstanceOf[dom.html.Canvas]
    canvas.width = 800
    canvas.height = 800
    dom.document.body.appendChild(canvas)
    canvas
  } else {
    dom.document.getElementById(withId).asInstanceOf[dom.html.Canvas]
  }

  val _mouseReceiverProxy = new MouseEventReceiverProxy(_peer, view)
  val _touchReceiverProxy = new TouchEventReceiverProxy(_peer, view)

  val gfx = new JSGfx(_peer)

  def globalPosition =
    polymorph.event.impl.Util.globalPosition(_peer)

  def width: Float =
    _peer.clientWidth

  def height: Float =
    _peer.clientHeight

  def title: String =
    _peer.getAttribute("alt")

  def title_=(title: String) =
    _peer.setAttribute("alt", title)

  def visible: Boolean =
    true

  def visible_=(visible: Boolean) = {}

  private var pendingRepaint = false
  def repaint(): Unit = {
    if(pendingRepaint)
      return
    pendingRepaint = true
    dom.window.requestAnimationFrame { t: Double =>
      pendingRepaint = false
      gfx.save()
      gfx.clearRect(0, 0, width, height)
      view.paint(gfx)
      gfx.restore()
      if(_animate)
        repaint()
    }
  }

  def focus(): Unit = {
    KeyEventReceiverProxy.keyboard.focus()
    KeyEventReceiverProxy.receiver = view
  }

  def blur(): Unit = {
    KeyEventReceiverProxy.keyboard.blur()
    KeyEventReceiverProxy.receiver = null
  }

  var _animate = false
  def animate = _animate
  def animate_=(value: Boolean): Unit = {
    if(value != _animate) {
      _animate = value
      if (_animate)
        repaint()
    }
  }

  var _cursor: Cursor = Cursor.Default

  def cursor: Cursor =
    _cursor

  def cursor_=(cursor: Cursor) = {
    _cursor = cursor
    _peer.style.cursor = cursor.asInstanceOf[JSCursor].css
  }
}


object RootGfxViewImpl {
  def apply(view: RootGfxView, title: String, withId: String): RootGfxViewPeer = {
    val impl = new RootGfxViewImpl(view, title, withId)
    impl.repaint()
    impl
  }
}
