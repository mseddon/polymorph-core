package polymorph.gfx.impl

import polymorph.gfx._
import org.scalajs.dom
import polymorph.gfx.Pattern

object PatternImpl {
  val canvas = dom.document.createElement("canvas").asInstanceOf[dom.html.Canvas]
  canvas.width = 1
  canvas.height = 1
  val ctx = canvas.getContext("2d").asInstanceOf[dom.raw.CanvasRenderingContext2D]

  def createPattern(image: Image): Pattern =
    Pattern(ctx.createPattern(image.asInstanceOf[ImageImpl].peer, "repeat"))
}