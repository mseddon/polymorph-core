package polymorph.gfx.impl

import java.nio.ByteBuffer

import polymorph.gfx._

import org.scalajs.dom
import org.scalajs.dom.raw.HTMLCanvasElement

import scala.concurrent.{Future, Promise}
import scala.scalajs.js

class ImageImpl private[polymorph] (private[polymorph] val peer: HTMLCanvasElement) extends Image {
  def this(width: Int, height: Int) {
    this {
      val canvas = dom.document.createElement("canvas").asInstanceOf[HTMLCanvasElement]
      canvas.width = width
      canvas.height = height
      canvas
    }
  }
  def width = peer.width
  def height = peer.height
  def gfx = new JSGfx(peer)
}

object ImageImpl {
  def apply(width: Int, height: Int): Image =
    new ImageImpl(width, height)

  def fromResource(file: String): Future[Image] = {
    val promise = Promise[Image]()
    val image = dom.document.createElement("img").asInstanceOf[dom.raw.HTMLImageElement]
    image.addEventListener("load", { e: dom.Event =>
      val canvas = dom.document.createElement("canvas").asInstanceOf[dom.raw.HTMLCanvasElement]
      canvas.width = image.naturalWidth
      canvas.height = image.naturalHeight
      canvas.getContext("2d").asInstanceOf[dom.raw.CanvasRenderingContext2D].drawImage(image, 0, 0)
      promise.success(new ImageImpl(canvas))
    })

    image.addEventListener("error", { e: dom.Event =>
      promise.failure(new Error("Error"))
    })
    image.src = "data:image/png;base64,"+dom.window.asInstanceOf[js.Dynamic].selectDynamic("nxResources").selectDynamic(file)
    promise.future
  }

  def fromByteBuffer(buffer: ByteBuffer): Future[Image] = {
    val promise = Promise[Image]()
    val image = dom.document.createElement("img").asInstanceOf[dom.raw.HTMLImageElement]
    image.addEventListener("load", { e: dom.Event =>
      val canvas = dom.document.createElement("canvas").asInstanceOf[dom.raw.HTMLCanvasElement]
      canvas.width = image.naturalWidth
      canvas.height = image.naturalHeight
      canvas.getContext("2d").asInstanceOf[dom.raw.CanvasRenderingContext2D].drawImage(image, 0, 0)
      promise.success(new ImageImpl(canvas))
    })

    image.addEventListener("error", { e: dom.Event =>
      promise.failure(new Error("Error"))
    })
    val array = new Array[Byte](buffer.remaining())
    buffer.get(array)
    image.src = "data:image/png;base64,"+dom.window.btoa(new String(array, "ISO-8859-1"))
    promise.future
  }
}
