package polymorph.gfx.impl

import polymorph.gfx.{Color, RadialGradient, LinearGradient}
import scryetek.vecmath.Vec2
/**
 * Created by Matt on 21/07/2015.
 */
object GradientImpl {
  def initLinear(start: Vec2, end: Vec2, colors: Seq[(Float, Color)]): LinearGradient = {
    val grad = PatternImpl.ctx.createLinearGradient(start.x, start.y, end.x, end.y)
    for((offset, color) <- colors)
      grad.addColorStop(0f max offset min 1f, "rgba("+color.r*255+","+color.g*255+","+color.b*255+","+color.a+")")
    new LinearGradient(grad)
  }

  def initRadial(center: Vec2, radius: Float, colors: Seq[(Float, Color)]): RadialGradient = {
    val grad = PatternImpl.ctx.createRadialGradient(center.x, center.y, 0, center.x, center.y, radius)
    for((offset, color) <- colors)
      grad.addColorStop(0f max offset min 1f, "rgba("+color.r*255+","+color.g*255+","+color.b*255+","+color.a+")")
    new RadialGradient(grad)
  }
}
