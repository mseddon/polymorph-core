package polymorph.gfx.impl

import polymorph.gfx._
import scryetek.vecmath.Vec2

import org.scalajs.dom.raw.{CanvasRenderingContext2D, HTMLCanvasElement}

import scala.scalajs.js
import scala.scalajs.js.JSConverters._

/**
 * Created by Matt on 11/07/2015.
 */

object JSPath2d {
  def apply(): JSPath2d =
    new JSPath2d
}

class JSPath2d extends Path2d {
  private [polymorph] sealed trait Path2dOp {
    def eval(ctx: CanvasRenderingContext2D): Unit
  }

  private [polymorph] case class MoveTo(x: Float, y: Float) extends Path2dOp {
    def eval(ctx: CanvasRenderingContext2D): Unit =
      ctx.moveTo(x, y)
  }

  private [polymorph] case class LineTo(x: Float, y: Float) extends Path2dOp {
    def eval(ctx: CanvasRenderingContext2D): Unit =
      ctx.lineTo(x, y)
  }

  private [polymorph] case class QuadricCurveTo(cpx: Float, cpy: Float, x: Float, y: Float) extends Path2dOp {
    def eval(ctx: CanvasRenderingContext2D): Unit =
      ctx.quadraticCurveTo(cpx, cpy, x, y)
  }

  private [polymorph] case class BezierCurveTo(cp0x: Float, cp0y: Float, cp1x: Float, cp1y: Float, x: Float, y: Float) extends Path2dOp {
    def eval(ctx: CanvasRenderingContext2D): Unit =
      ctx.bezierCurveTo(cp0x, cp0y, cp1x, cp1y, x, y)
  }

  private [polymorph] case class Arc(x: Float, y: Float, radius: Float, startAngle: Float, endAngle: Float, anticlockwise: Boolean = false) extends Path2dOp {
    def eval(ctx: CanvasRenderingContext2D): Unit =
      ctx.arc(x, y, radius, startAngle, endAngle, anticlockwise)
  }

  private [polymorph] case object Close extends Path2dOp {
    def eval(ctx: CanvasRenderingContext2D): Unit =
      ctx.closePath()
  }

  var ops = Seq[Path2dOp]()

  def moveTo(x: Float, y: Float): Unit = {
    ops :+= MoveTo(x, y)
    _currentPosition = Vec2(x, y)
  }

  def lineTo(x: Float, y: Float): Unit = {
    ops :+= LineTo(x, y)
    _currentPosition = Vec2(x, y)
  }

  def quadricCurveTo(cpx: Float, cpy: Float, x: Float, y: Float): Unit = {
    ops :+= QuadricCurveTo(cpx, cpy, x, y)
    _currentPosition = Vec2(x, y)
  }

  def bezierCurveTo(cp0x: Float, cp0y: Float, cp1x: Float, cp1y: Float, x: Float, y: Float): Unit = {
    ops :+= BezierCurveTo(cp0x, cp0y, cp1x, cp1y, x, y)
    _currentPosition = Vec2(x, y)
  }

  def arc(x: Float, y: Float, radius: Float, startAngle: Float, endAngle: Float, anticlockwise: Boolean = false): Unit = {
    ops :+= Arc(x, y, radius, startAngle, endAngle, anticlockwise)
    _currentPosition = Vec2(Math.cos(endAngle).toFloat*radius + x, Math.sin(endAngle).toFloat*radius + y)
  }

  private [polymorph] def execute(ctx: CanvasRenderingContext2D): Unit = {
    ctx.beginPath()
    for(op <- ops)
      op.eval(ctx)
  }

  def close(): Unit =
    ops :+= Close

  private var _currentPosition = Vec2(0, 0)

  def currentPosition = _currentPosition
}

class JSGfx(val canvas: HTMLCanvasElement) extends Gfx {
  var ctx = canvas.getContext("2d").asInstanceOf[CanvasRenderingContext2D]
  ctx.lineCap = "round"
  ctx.lineJoin = "round"

  private case class State(var paintStyle: PaintStyle, var font: JSFont, var textAlign: TextAlign = TextAlign.Start, var textBaseline: TextBaseline = TextBaseline.Alphabetic)
  private val stack = collection.mutable.Stack[State]()
  private var currentState = State(paintStyle = Color(0, 0, 0), font = new JSFont("Arial", Font.Plain, 12))

  font = currentState.font

  def lineWidth: Float =
    ctx.lineWidth.toFloat

  def lineWidth_=(value: Float): Unit =
    ctx.lineWidth = value

  def lineDash: Seq[Float] =
    ctx.getLineDash().map(_.toFloat)

  def lineDash_=(pattern: Seq[Float]): Unit =
    ctx.setLineDash(pattern.map(_.toDouble).toJSArray)

  def lineDashOffset: Float =
    ctx.lineDashOffset.toFloat

  def lineDashOffset_=(offset: Float): Unit =
    ctx.lineDashOffset = offset

  def style: PaintStyle =
    currentState.paintStyle

  def style_=(style: PaintStyle): Unit =
    style match {
      case Color(r, g, b, a) =>
        currentState.paintStyle = style
        ctx.fillStyle = "rgba("+(r*255).toInt+","+(g*255).toInt+","+(b*255).toInt+","+a+")"
        ctx.strokeStyle = "rgba("+(r*255).toInt+","+(g*255).toInt+","+(b*255).toInt+","+a+")"
      case p: Pattern =>
        currentState.paintStyle = style
        ctx.fillStyle = p.peer.asInstanceOf[js.Any]
        ctx.strokeStyle = p.peer.asInstanceOf[js.Any]
      case g: LinearGradient =>
        currentState.paintStyle = style
        ctx.fillStyle = g.peer.asInstanceOf[js.Any]
        ctx.strokeStyle = g.peer.asInstanceOf[js.Any]
      case g: RadialGradient =>
        currentState.paintStyle = style
        ctx.fillStyle = g.peer.asInstanceOf[js.Any]
        ctx.strokeStyle = g.peer.asInstanceOf[js.Any]
    }

  def globalAlpha: Float =
    ctx.globalAlpha.toFloat

  def globalAlpha_=(value: Float): Unit =
    ctx.globalAlpha = value

  def globalCompositeOperation: GlobalCompositeOperation =
    ctx.globalCompositeOperation match {
      case "destination-atop" => GlobalCompositeOperation.DstAtop
      case "destination-in" => GlobalCompositeOperation.DstIn
      case "destination-out" => GlobalCompositeOperation.DstOut
      case "destination-over" => GlobalCompositeOperation.DstOver
      case "source-atop" => GlobalCompositeOperation.SrcAtop
      case "source-in" => GlobalCompositeOperation.SrcIn
      case "source-out" => GlobalCompositeOperation.SrcOut
      case "source-over" => GlobalCompositeOperation.SrcOver
//      case "multiply" => GlobalCompositeOperation.Multiply
//      case "screen" => GlobalCompositeOperation.Screen
//      case "lighter" => GlobalCompositeOperation.Add
    }

  def globalCompositeOperation_=(value: GlobalCompositeOperation): Unit =
    ctx.globalCompositeOperation = value match {
      case GlobalCompositeOperation.DstAtop => "destination-atop"
      case GlobalCompositeOperation.DstIn => "destination-in"
      case GlobalCompositeOperation.DstOut => "destination-out"
      case GlobalCompositeOperation.DstOver => "destination-over"
      case GlobalCompositeOperation.SrcAtop => "source-atop"
      case GlobalCompositeOperation.SrcIn => "source-in"
      case GlobalCompositeOperation.SrcOut => "source-out"
      case GlobalCompositeOperation.SrcOver => "source-over"
//      case GlobalCompositeOperation.Multiply => "multiply"
//      case GlobalCompositeOperation.Screen => "screen"
//      case GlobalCompositeOperation.Add => "lighter"
    }

  def miterLimit: Float =
    ctx.miterLimit.toFloat

  def miterLimit_=(value: Float): Unit =
    ctx.miterLimit = value

  def lineCap: LineCap =
    ctx.lineCap match {
      case "butt" => LineCap.Butt
      case "square" => LineCap.Square
      case "round" => LineCap.Round
    }

  def lineCap_=(value: LineCap): Unit =
    ctx.lineCap = value match {
      case LineCap.Butt => "butt"
      case LineCap.Round => "round"
      case LineCap.Square => "square"
    }

  def lineJoin: LineJoin =
    ctx.lineJoin match {
      case "bevel" => LineJoin.Bevel
      case "miter" => LineJoin.Miter
      case "round" => LineJoin.Round
    }

  def lineJoin_=(value: LineJoin): Unit =
    ctx.lineJoin = value match {
      case LineJoin.Bevel => "bevel"
      case LineJoin.Miter => "miter"
      case LineJoin.Round => "round"
    }

  def stroke(path: Path2d): Unit = {
    path.asInstanceOf[JSPath2d].execute(ctx)
    ctx.stroke()
  }

  def fill(path: Path2d): Unit  = {
    path.asInstanceOf[JSPath2d].execute(ctx)
    ctx.fill()
  }

  def clip(path: Path2d): Unit = {
    path.asInstanceOf[JSPath2d].execute(ctx)
    ctx.clip()
  }

  def save(): Unit = {
    stack.push(currentState)
    currentState = currentState.copy()
    ctx.save()
  }

  def restore(): Unit = {
    ctx.restore()
    currentState = stack.pop()
  }

  def fillRect(x: Float, y: Float, width: Float, height: Float): Unit =
    ctx.fillRect(x, y, width, height)

  def strokeRect(x: Float, y: Float, width: Float, height: Float): Unit =
    ctx.strokeRect(x, y, width, height)

  def clearRect(x: Float, y: Float, width: Float, height: Float): Unit =
    ctx.clearRect(x, y, width, height)

  def drawImage(image: Image, dx: Float, dy: Float): Unit =
    ctx.drawImage(image.asInstanceOf[ImageImpl].peer, dx, dy)

  def drawImage(image: Image, dx: Float, dy: Float, dWidth: Float, dHeight: Float): Unit =
    ctx.drawImage(image.asInstanceOf[ImageImpl].peer, dx, dy, dWidth, dHeight)

  def drawImage(image: Image, sx: Float, sy: Float, sWidth: Float, sHeight: Float, dx: Float, dy: Float, dWidth: Float, dHeight: Float): Unit =
    ctx.drawImage(image.asInstanceOf[ImageImpl].peer, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight)

  def translate(tx: Float, ty: Float): Unit =
    ctx.translate(tx, ty)

  def scale(sx: Float, sy: Float): Unit =
    ctx.scale(sx, sy)

  def rotate(angle: Float): Unit =
    ctx.rotate(angle)

  def transform(a: Float, b: Float, c: Float, d: Float, e: Float, f: Float): Unit =
    ctx.transform(a, b, c, d, e, f)

  def setTransform(a: Float, b: Float, c: Float, d: Float, e: Float, f: Float): Unit =
    ctx.setTransform(a, b, c, d, e, f)


  private def offsetText(text: String, x: Float, y: Float): (Float, Float) = {
    val x1 = textAlign match {
      case TextAlign.Left => x
      case TextAlign.Start => x
      case TextAlign.Center => x-font.stringWidth(text)/2
      case TextAlign.Right => x-font.stringWidth(text)
      case TextAlign.End => x-font.stringWidth(text)
    }

    val y1 = textBaseline match {
      case TextBaseline.Alphabetic => y
      case TextBaseline.Middle => y+font.ascent/2-font.descent
      case TextBaseline.Bottom => y-font.descent
      case TextBaseline.Top => y+font.ascent
      case TextBaseline.Hanging => y+font.ascent+font.descent
    }
    (x1, y1)
  }

  def fillText(text: String, x: Float, y: Float): Unit = {
    val (x1, y1) = offsetText(text, x, y)
    ctx.fillText(text, x1, y1)
  }

  def strokeText(text: String, x: Float, y: Float): Unit = {
    val (x1, y1) = offsetText(text, x, y)
    ctx.strokeText(text, x1, y1)
  }

  def font_=(font: Font): Unit = {
    currentState.font = font.asInstanceOf[JSFont]
    ctx.font = font.asInstanceOf[JSFont].toCSS
  }

  def font = currentState.font

  def textAlign =
    currentState.textAlign

  def textAlign_=(align: TextAlign): Unit =
    currentState.textAlign = align

  def textBaseline =
    currentState.textBaseline

  def textBaseline_=(baseline: TextBaseline): Unit =
    currentState.textBaseline = baseline
}
