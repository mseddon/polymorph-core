package polymorph.gfx.impl

import polymorph.gfx.Font
import org.scalajs.dom

class JSFont(name: String, style: Font.Style, size: Float) extends Font(name, style, size) {
  def toCSS: String = {
    val flags = style match {
      case Font.Plain => ""
      case Font.Bold => "bold "
      case Font.Italic => "italic "
      case Font.BoldItalic => "bold italic "
    }
    s"$flags${size}px $name"
  }

  private var _ascent: Float = 0
  private var _descent: Float = 0
  private var _height: Float = 0

  def ascent = _ascent
  def descent = _descent
  def height = _height

  def stringWidth(string: String): Float = {
    PatternImpl.ctx.font = toCSS
    PatternImpl.ctx.measureText(string).width.toFloat
  }
}

object JSFont {
  private val text = dom.document.createElement("span").asInstanceOf[dom.html.Span]
  private val block = dom.document.createElement("div").asInstanceOf[dom.html.Div]
  block.style.display = "inline-block"
  block.style.width = "1px"
  block.style.height = "0px"


  private val outer = dom.document.createElement("iframe").asInstanceOf[dom.html.IFrame]
  dom.document.body.appendChild(outer)
  //outer.src = "about:blank"
  outer.width = "1"
  outer.height = "1"
  outer.style.position = "fixed"
  outer.style.left = "-10px"
  outer.contentWindow.document.open()
  outer.contentWindow.document.write("<html><head><meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' /></head><body>wee</body></html>")
  outer.contentWindow.document.close()
  outer.contentWindow.document.body.style.position = "absolute"
  outer.contentWindow.document.body.style.width = "10000px"
  outer.contentWindow.document.body.style.height = "10000px"


  private val div = dom.document.createElement("div").asInstanceOf[dom.html.Div]

  outer.className = "MEASURE"
  div.style.position = "absolute"
  text.style.whiteSpace = "pre"
  div.appendChild(text)
  div.appendChild(block)




  def apply(name: String, style: Font.Style, size: Float): Font = {
    val font = new JSFont(name, style, size)
    outer.contentWindow.document.body.appendChild(div)
    div.style.lineHeight = size+"px"
    text.style.font = font.toCSS
    block.style.verticalAlign = "baseline"
    text.textContent = "X"
    font._ascent = (block.offsetTop-text.offsetTop).toFloat
    block.style.verticalAlign = "bottom"
    font._height = (block.offsetTop-text.offsetTop).toFloat

    font._descent = font._height-font._ascent
    font
  }
}