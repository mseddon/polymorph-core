package polymorph.event.impl

import java.io.{PrintWriter, StringWriter}

import org.scalajs.dom
import polymorph.event._
import scryetek.vecmath.Vec2


class TouchEventReceiverProxy(canvas: dom.html.Element, receiver: TouchEventReceiver)  {

  private def convertTouchEventModifiers(e: dom.TouchEvent): Int = {
    var mask = 0
    if(e.altKey)   mask |= Event.AltDown
    if(e.ctrlKey)  mask |= Event.CtrlDown
    if(e.metaKey)  mask |= Event.MetaDown
    if(e.shiftKey) mask |= Event.ShiftDown
    mask
  }

  private def makeTouches(touches: dom.TouchList): Seq[Touch] =
    (for(i <- 0 until touches.length) yield touches(i)).map { t =>
      val globalPosition = Vec2(t.pageX.toFloat, t.pageY.toFloat)
      val localPosition = globalPosition - Util.globalPosition(canvas)
      Touch(t.asInstanceOf[scalajs.js.Dynamic].identifier.asInstanceOf[Double].toLong, t.target, localPosition, globalPosition, globalPosition)
    }

  canvas.addEventListener("touchstart", (e: dom.TouchEvent) => {
    TouchEventReceiver.capturedTouches += receiver
    e.preventDefault()
    //      e.stopImmediatePropagation()
    val touches = makeTouches(e.touches)
    val changedTouches = makeTouches(e.changedTouches)
    receiver.touchStart(TouchStart(receiver, System.currentTimeMillis(), convertTouchEventModifiers(e), touches, changedTouches))
  })

  canvas.addEventListener("touchend", (e: dom.TouchEvent) => {
    if(TouchEventReceiver.capturedTouches.contains(receiver)) {
      e.preventDefault()
      //      e.stopImmediatePropagation()
      val touches = makeTouches(e.touches)
      val changedTouches = makeTouches(e.changedTouches)
      receiver.touchEnd(TouchEnd(receiver, System.currentTimeMillis(), convertTouchEventModifiers(e), touches, changedTouches))
    }
    if(e.touches.length == 0)
      TouchEventReceiver.capturedTouches -= receiver
  })

  canvas.addEventListener("touchmove", (e: dom.TouchEvent) => {
    if(TouchEventReceiver.capturedTouches.contains(receiver)) {
      e.preventDefault()
      //      e.stopImmediatePropagation()
      val touches = makeTouches(e.touches)
      val changedTouches = makeTouches(e.changedTouches)
      receiver.touchMove(TouchMove(receiver, System.currentTimeMillis(), convertTouchEventModifiers(e), touches, changedTouches))
    }
  })

  canvas.addEventListener("touchcancel", (e: dom.TouchEvent) => {
    if(TouchEventReceiver.capturedTouches.contains(receiver)) {
      e.preventDefault()
      e.stopImmediatePropagation()
      val touches = makeTouches(e.touches)
      receiver.touchCancel(TouchCancel(receiver, System.currentTimeMillis(), convertTouchEventModifiers(e), touches, touches))
    }
    TouchEventReceiver.capturedTouches -= receiver
  })
  
  def documentScroll =
    Vec2((dom.document.body.scrollLeft max dom.document.documentElement.scrollLeft).toFloat,
      (dom.document.body.scrollTop max dom.document.documentElement.scrollTop).toFloat)

  def viewportPosition(slab: MouseEventReceiver, e: dom.MouseEvent): Vec2 =
    Vec2(e.pageX.toFloat, e.pageY.toFloat) - documentScroll

  def globalPosition(slab: MouseEventReceiver, e: dom.MouseEvent): Vec2 =
    Vec2(e.pageX.toFloat, e.pageY.toFloat)

  def localPosition(slab: MouseEventReceiver, e: dom.MouseEvent): Vec2 =
    globalPosition(slab, e) - slab.globalPosition
}