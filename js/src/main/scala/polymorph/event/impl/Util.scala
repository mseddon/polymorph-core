package polymorph.event.impl

import org.scalajs.dom
import scryetek.vecmath.Vec2

object Util {
  // Literally nothing in the-browser works correctly, *sigh*.
  lazy val elem = {
    val div = dom.document.createElement("div").asInstanceOf[dom.html.Div]
    div.style.position = "absolute"
    div.style.left = "0px"
    div.style.top = "0px"
    div.style.width = "0px"
    div.style.height = "0px"
    dom.document.body.appendChild(div)
    div
  }

  def globalPosition(element: dom.Element): Vec2 = {
    val r0 = elem.getBoundingClientRect()
    val r1 = element.getBoundingClientRect()

    Vec2((r1.left - r0.left).toFloat, (r1.top - r0.top).toFloat)
  }
}
