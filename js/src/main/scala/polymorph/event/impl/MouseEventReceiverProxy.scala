package polymorph.event.impl
import org.scalajs.dom

import polymorph.event._
import scryetek.vecmath.Vec2

private[polymorph] class JSEvent(event: dom.raw.Event) extends RawEvent {
  def consume(): Unit =
    event.preventDefault()
}

class MouseEventReceiverProxy(canvas: dom.html.Element, receiver: MouseEventReceiver) {
  import MouseEventReceiverProxy._
  canvas.addEventListener("mousedown", { e: dom.MouseEvent =>
    e.preventDefault()
    if(MouseEventReceiver.capture == null) {
      MouseEventReceiver.capture = receiver
      val globalPosition = Vec2(e.pageX.toFloat, e.pageY.toFloat)
      val localPosition = globalPosition - Util.globalPosition(canvas)
      val me = MouseDown(receiver, System.currentTimeMillis(), convertMouseModifiers(e), localPosition, localPosition, globalPosition, e.button + 1, new JSEvent(e))
      receiver.mouseDown(me)
    }
  }, true)

  canvas.addEventListener("mousemove", { e: dom.MouseEvent =>
    e.preventDefault()
    if(MouseEventReceiver.capture == null) {
      val globalPosition = Vec2(e.pageX.toFloat, e.pageY.toFloat)
      val localPosition = globalPosition - Util.globalPosition(canvas)
      val me = MouseMove(receiver, System.currentTimeMillis(), convertMouseModifiers(e), localPosition, localPosition, globalPosition, new JSEvent(e))
      receiver.mouseMove(me)
    }
  }, true)

  canvas.addEventListener("mouseenter", { e: dom.MouseEvent =>
    e.preventDefault()
    val globalPosition = Vec2(e.pageX.toFloat, e.pageY.toFloat)
    val localPosition = globalPosition - Util.globalPosition(canvas)
    val me = MouseEnter(receiver, System.currentTimeMillis(), convertMouseModifiers(e), localPosition, localPosition, globalPosition, new JSEvent(e))
    receiver.mouseEnter(me)
  })

  canvas.addEventListener("mouseleave", { e: dom.MouseEvent =>
    val globalPosition = Vec2(e.pageX.toFloat, e.pageY.toFloat)
    val localPosition = globalPosition - Util.globalPosition(canvas)
    val me = MouseExit(receiver, System.currentTimeMillis(), convertMouseModifiers(e), localPosition, localPosition, globalPosition, new JSEvent(e))
    receiver.mouseExit(me)
  })

  canvas.addEventListener("mousewheel", { e: dom.WheelEvent =>
    def deltaConvert(x: Double) = x match {
      case 0 => 0f
      case x if x > 0 => 1f
      case x => -1f
    }
    val globalPosition = Vec2(e.pageX.toFloat, e.pageY.toFloat)
    val localPosition = globalPosition - Util.globalPosition(canvas)
    val me = MouseWheel(receiver, System.currentTimeMillis(), convertMouseModifiers(e), localPosition, localPosition, globalPosition, Vec2(deltaConvert(e.deltaX), deltaConvert(e.deltaY)), new JSEvent(e))
    receiver.mouseWheel(me)
  })

  // disable the context menu
  canvas.addEventListener("contextmenu", { e: dom.Event => e.preventDefault() })

}

object MouseEventReceiverProxy {

  def documentScroll =
    Vec2((dom.document.body.scrollLeft max dom.document.documentElement.scrollLeft).toFloat,
      (dom.document.body.scrollTop max dom.document.documentElement.scrollTop).toFloat)

  def viewportPosition(slab: MouseEventReceiver, e: dom.MouseEvent): Vec2 =
    Vec2(e.pageX.toFloat, e.pageY.toFloat) - documentScroll

  def globalPosition(slab: MouseEventReceiver, e: dom.MouseEvent): Vec2 =
    Vec2(e.pageX.toFloat, e.pageY.toFloat)

  def localPosition(slab: MouseEventReceiver, e: dom.MouseEvent): Vec2 =
    globalPosition(slab, e) - slab.globalPosition

  def convertMouseModifiers(e: dom.raw.MouseEvent, scrub: Boolean = false): Int = {
    var mask = 0
    if(e.altKey) mask |= Event.AltDown
    if(e.ctrlKey) mask |= Event.CtrlDown
    if(e.shiftKey) mask |= Event.ShiftDown
    if(e.metaKey) mask |= Event.MetaDown
    // Yes, we lose button 4 and 5 events here.

    mask |= (e.buttons & 0x07)
    if(scrub)
      // this is a mouse up, forcefully remove button up to work around silly browsers
      mask &= ~(1 << e.button)
    mask
  }
  
  dom.window.addEventListener[dom.MouseEvent]("mousemove", (e: dom.MouseEvent) => {
    if (MouseEventReceiver.capture != null) {
      val me = MouseDrag(MouseEventReceiver.capture, System.currentTimeMillis(), convertMouseModifiers(e), localPosition(MouseEventReceiver.capture, e), viewportPosition(MouseEventReceiver.hover, e), globalPosition(MouseEventReceiver.capture, e), new JSEvent(e))
      MouseEventReceiver.capture.mouseDrag(me)
    }
    if (MouseEventReceiver.capture == null && MouseEventReceiver.hover != null) {
      val me = MouseExit(MouseEventReceiver.hover, System.currentTimeMillis(), convertMouseModifiers(e), localPosition(MouseEventReceiver.hover, e), viewportPosition(MouseEventReceiver.hover, e), globalPosition(MouseEventReceiver.hover, e), new JSEvent(e))
      // hover has been lost
      MouseEventReceiver.hover.mouseExit(me)
      MouseEventReceiver.hover = null
    }
  })

  dom.window.addEventListener("mouseup", (e: dom.MouseEvent) =>
    if(MouseEventReceiver.capture != null) {
      val me = MouseUp(MouseEventReceiver.capture, System.currentTimeMillis(), convertMouseModifiers(e, true), localPosition(MouseEventReceiver.capture, e), viewportPosition(MouseEventReceiver.hover, e), globalPosition(MouseEventReceiver.capture, e), e.button, new JSEvent(e))
      MouseEventReceiver.capture.mouseUp(me)
      if((e.buttons & 0x7 & ~(1 << e.button)) == 0)
        MouseEventReceiver.capture = null
    }
  )

}