package polymorph.event.impl

import java.io.{PrintWriter, StringWriter}

import org.scalajs.dom
import polymorph.event._

import scala.scalajs.js.JSON

object KeyEventReceiverProxy {
  var receiver: KeyEventReceiver = null
  val keyboard = dom.document.createElement("input").asInstanceOf[dom.html.Input]
  keyboard.style.position = "fixed"
  keyboard.style.left = "0px"
  keyboard.style.top = "0px"
  keyboard.style.width = "200px"
  keyboard.style.height = "16px"

  dom.document.body.appendChild(keyboard)

  def keyModifiers(e: dom.raw.KeyboardEvent): Int = {
    var mask = 0
    if(e.altKey)   mask |= Event.AltDown
    if(e.ctrlKey)  mask |= Event.CtrlDown
    if(e.metaKey)  mask |= Event.MetaDown
    if(e.shiftKey) mask |= Event.ShiftDown
    mask
  }

  var lastCaret = 0
  var lastString = ""

  val navigationKeys = {
    import KeyEvent._
    Set(VK_LEFT, VK_RIGHT, VK_UP, VK_DOWN, VK_PAGE_UP, VK_PAGE_DOWN, VK_HOME, VK_END)
  }

  keyboard.addEventListener("keydown", { e: dom.KeyboardEvent =>
    if(receiver != null)
      receiver.keyDown(KeyDown(receiver, System.currentTimeMillis(), keyModifiers(e), e.keyCode))
  })

  keyboard.addEventListener("keyup", { e: dom.KeyboardEvent =>
    // don't break the caret position
    keyboard.selectionEnd = lastCaret
    keyboard.selectionStart = lastCaret
    if(receiver != null)
      receiver.keyUp(KeyUp(receiver, System.currentTimeMillis(), keyModifiers(e), e.keyCode))
  })


  private var _inEvent = false

  def processUpdatedText(): Unit = {
    def firstDifference(str1: String, str2: String): Int = {
      val len = str1.length min str2.length
      for(i <- 0 until len if str1.charAt(i) != str2.charAt(i))
        return i
      len
    }

    def lastDifference(str1: String, str2: String): Int =
      str1.length-firstDifference(str1.reverse, str2.reverse)

    _inEvent = true

    val currentString = keyboard.value
    val currentCaret = keyboard.selectionEnd

    if (receiver != null) {
      val now = System.currentTimeMillis()
      val diff = firstDifference(lastString, currentString)
      // delete up to first difference
      for(i <- currentCaret until lastCaret)
        receiver.keyTyped(KeyTyped(receiver, now, 0, 0, 8))
      // type what we got
      for(i <- diff until currentCaret)
        receiver.keyTyped(KeyTyped(receiver, now, 0, 0, currentString.charAt(i)))
    }
    lastCaret = currentCaret
    lastString = currentString
    _inEvent = false
    if(_targetString != null)
      setContext(_targetString, _targetPosition)
  }

  keyboard.addEventListener("input", { e: dom.Event =>
    processUpdatedText()
  })

  private var _targetString: String = null
  private var _targetPosition: Int = 0

  def setContext(context: String, caret: Int, multiSelect: Boolean = false): Unit = {
    val string = if(multiSelect) " "+context else context
    val position = if(multiSelect) 1+caret else caret
    if(_inEvent) {
      _targetString = string
      _targetPosition = position
    } else {
      keyboard.value = string
      keyboard.selectionStart = position
      keyboard.selectionEnd = position
      lastCaret = position
      lastString = string
      _targetString = null
      keyboard.blur()
      keyboard.focus()
    }
  }
}