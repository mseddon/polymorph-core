package polymorph.impl

import java.nio.ByteBuffer

import org.scalajs.dom

import scala.concurrent.Future
import scala.scalajs.js

object ResourceImpl {
  import scala.concurrent.ExecutionContext.Implicits.global
  def getResource(file: String): Future[ByteBuffer] = Future {
    val data = dom.window.atob(dom.window.asInstanceOf[js.Dynamic].selectDynamic("nxResources").selectDynamic(file).asInstanceOf[String])
    ByteBuffer.wrap(data.getBytes("ISO-8859-1"))
  }
}
