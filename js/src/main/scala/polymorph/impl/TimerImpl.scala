package polymorph.impl

import polymorph.timer.TimerHandle
import scalajs.js.timers
/**
 * Created by matt on 05/10/15.
 */
object TimerImpl {
  case class IntervalHandle(var id: timers.SetIntervalHandle, var delay: timers.SetTimeoutHandle) extends TimerHandle
  case class TimeoutHandle(id: timers.SetTimeoutHandle) extends TimerHandle
  def setTimeout(delay: Double)(handler: () => Unit): TimerHandle =
    TimeoutHandle(timers.setTimeout(delay * 1000.0)(handler()))
  def setInterval(delay: Double, interval: Double)(handler: () => Unit): TimerHandle = {
    val handle = IntervalHandle(null, null)
    handle.delay = timers.setTimeout(delay * 1000) {
      handle.delay = null
      handle.id = timers.setInterval(interval * 1000.0)(handler())
    }
    handle
  }
  def clearTimer(timerHandle: TimerHandle): Unit =
    timerHandle match {
      case IntervalHandle(x, y) =>
        if(x != null) timers.clearInterval(x)
        if(y != null) timers.clearTimeout(y)
      case TimeoutHandle(x) =>
        timers.clearTimeout(x)
    }
}
