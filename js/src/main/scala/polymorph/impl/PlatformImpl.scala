package polymorph.impl

import polymorph.PlatformPeer

class PlatformImpl extends PlatformPeer {
  override def isWeb: Boolean = true
}
