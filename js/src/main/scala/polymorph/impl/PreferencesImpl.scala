package polymorph.impl
import org.scalajs.dom
import polymorph.{PreferencesWriter, PreferencesPeer}

class PreferencesImpl extends PreferencesPeer {
  override def getString(key: String): Option[String] =
    Option(dom.window.localStorage.getItem(key))

  override def getFloat(key: String): Option[Float] =
    Option(dom.window.localStorage.getItem(key)).map(_.toFloat)

  override def getLong(key: String): Option[Long] =
  Option(dom.window.localStorage.getItem(key)).map(_.toLong)

  override def withWriter(body: (PreferencesWriter) => Unit): Unit =
    body(writer)

  override def getBoolean(key: String): Option[Boolean] =
    Option(dom.window.localStorage.getItem(key)).map(_.toBoolean)

  override def contains(key: String): Boolean =
    dom.window.localStorage.getItem(key) != null

  override def getInt(key: String): Option[Int] =
    Option(dom.window.localStorage.getItem(key)).map(_.toInt)

  val writer = new PreferencesWriter {
    override def putString(key: String, value: String): Unit =
      dom.window.localStorage.setItem(key, value)

    override def clear(): Unit =
      dom.window.localStorage.clear()

    override def putFloat(key: String, value: Float): Unit =
      dom.window.localStorage.setItem(key, value.toString)

    override def remove(key: String): Unit =
      dom.window.localStorage.removeItem(key)

    override def putBoolean(key: String, value: Boolean): Unit =
      dom.window.localStorage.setItem(key, value.toString)

    override def putInt(key: String, value: Int): Unit =
      dom.window.localStorage.setItem(key, value.toString)

    override def putLong(key: String, value: Long): Unit =
      dom.window.localStorage.setItem(key, value.toString)
  }
}
