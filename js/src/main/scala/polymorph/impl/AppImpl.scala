package polymorph.impl

class AppImpl(app: polymorph.App) {
  def main(): Unit = {
    import scala.concurrent.ExecutionContext.Implicits.global
    for(_ <- app.init(Array()))
      app.main(Array())
  }
}