package polymorph.impl

import polymorph.{Resource, Preferences, Platform}
import polymorph.gfx._
import polymorph.gfx.impl._
import polymorph.net.Http
import polymorph.net.impl.HttpImpl
import polymorph.ui._
import polymorph.ui.impl.{JSCursorImpl, RootGfxViewImpl}

/**
 * Created by Matt on 23/07/2015.
 */
object Linker {
  def link(): Unit = {
    Image._imageCreate = ImageImpl.apply
    Image._imageFromResource = ImageImpl.fromResource
    Image._imageFromByteBuffer = ImageImpl.fromByteBuffer
    Pattern.init = PatternImpl.createPattern
    LinearGradient.init = GradientImpl.initLinear
    RadialGradient.init = GradientImpl.initRadial
    Path2d.init = JSPath2d.apply
    Font.init = JSFont.apply
    RootGfxView._peer = RootGfxViewImpl.apply
    Platform._peer = new PlatformImpl
    polymorph.Prelude._log = PreludeImpl.log
    polymorph.ui.Screen._dpi = polymorph.ui.impl.ScreenImpl.dpi
    Http.httpClient = new HttpImpl
    Preferences.impl = new PreferencesImpl
    Resource._getResource = ResourceImpl.getResource
    Cursor.cursorImpl = new JSCursorImpl

    polymorph.timer._clearTimer  = TimerImpl.clearTimer
    polymorph.timer._setTimeout  = TimerImpl.setTimeout
    polymorph.timer._setInterval = TimerImpl.setInterval

    polymorph.event.Keyboard._setContext = polymorph.event.impl.KeyEventReceiverProxy.setContext
  }
}