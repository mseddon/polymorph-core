package polymorph.net.impl

import java.nio.ByteBuffer

import org.scalajs.dom.ext.Ajax
import polymorph.net.Http.HttpData
import polymorph.net.{Http, HttpResponse, HttpPeer}

import scala.concurrent.Future
import scala.scalajs.js.typedarray.{TypedArrayBuffer, ArrayBuffer}

class HttpImpl extends HttpPeer {
  import scala.concurrent.ExecutionContext.Implicits.global
  override def apply(
      method: String,
      url: String,
      data: HttpData,
      timeout: Int,
      headers: Map[String, String],
      withCredentials: Boolean): Future[HttpResponse] = {
    val request = data match {
      case null =>
        Ajax(method, url, "", timeout, headers, withCredentials, "arraybuffer")
      case Http.StringData(str) =>
        Ajax(method, url, str, timeout, headers, withCredentials, "arraybuffer")
      case Http.ByteBufferData(buf) =>
        Ajax(method, url, buf, timeout, headers, withCredentials, "arraybuffer")
      case Http.ByteArrayData(bytes) =>
        Ajax(method, url, ByteBuffer.wrap(bytes), timeout, headers, withCredentials, "arraybuffer")
    }
    request.map { result =>
      val headers = (for {
        headerLine <- result.getAllResponseHeaders().split("\r\n")
        colon = headerLine.indexOf(':')
      } yield headerLine.substring(0, colon).toLowerCase -> headerLine.substring(colon+1).trim).toMap

      HttpResponse(result.status, result.statusText, headers, TypedArrayBuffer.wrap(result.response.asInstanceOf[ArrayBuffer]))
    }
  }
}
