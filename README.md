= Polymorph Core

The polymorph core api is a microlayer across four different platforms, the DOM api, apple's IOS sdk, the
android SDK, and the official JVM desktop api.

It provides low-level access to input events, some networking, and low-level graphics of the form found in
`Graphics2D`, `Quartz`, `android.graphics` and the `HTML Canvas` api.

Games developers might also be interested in `polymorph-gl` which adds OpenGL support.

== Overview

There are several
